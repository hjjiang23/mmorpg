package mmo;

import java.util.Queue;
import java.util.concurrent.ExecutionException;

import mmo.server.ScheduledTask;

public abstract class Controller<V extends ScheduledTask> extends Thread {
    public Queue<V> queue;
    
    public void initQueue(Queue<V> queue) {
        this.queue = queue;
    }
    
    @Override
    public void run() {
        if(queue ==  null) {
            System.err.println("Controller initialized incorrectly: backing queue is null");
            return;
        }
        while(isAlive()) {
            try {
                V element = queue.poll();
                if(element != null) {
                    element.get();
                    // sleep(1000);
                }
                else {
                    // sleep(1000);
                    sleep(1);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }
    }
    
    public boolean addFutureEvent(V element) {
        // System.out.println("tasks scheduled: " + (queue.size() + 1));
        return queue.offer(element);
    }
}
