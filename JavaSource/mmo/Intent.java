package mmo;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.Arrays;

/**
 * The constant arguments are used to check for equalTo(Object o) equality.
 * @author Alan Wang
 *
 */
public class Intent {
    public Intent(Object[] constants, Object[] variables, boolean[] constantIndices, int totalBytes) {
        this.constants = constants;
        this.variables = variables;
        this.constantIndices = constantIndices;
        this.totalBytes = totalBytes;
    }
        
    protected Object[] constants;
    protected Object[] variables;
    protected boolean[] constantIndices; // True where the constant variable is added to bytebuffer, only has to go until the last constant index
    protected int totalBytes;

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + Arrays.hashCode(constantIndices);
        result = prime * result + Arrays.hashCode(constants);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Intent other = (Intent) obj;
        if (!Arrays.equals(constantIndices, other.constantIndices))
            return false;
        if (!Arrays.equals(constants, other.constants))
            return false;
        return true;
    }
    
    /**
     * Helper method to write an Object to a ByteBuffer, in order to convert a primitive object to bytes.
     * @param buf - The ByteBuffer used to store the bytes of the object.
     * @param o - The Object to be written
     * @return The number of bytes used to write the object
     */
    private int writeToByteOutput(ByteBuffer buf, Object o) {
        if(o instanceof Byte) {
            buf.put((Byte) o);
            return 1;
        }
        else if(o instanceof Integer) {
            buf.putInt((Integer) o);
            return 4;
        }
        else if(o instanceof Float) {
            buf.putFloat((Float) o);
            return 4;
        }
        else if(o instanceof String) {
            String str = (String) o;
            Util.writeStringToBuffer(buf, str);
            return (((String) o).length() + 1) * 2;
        }
        else if(o instanceof Boolean) {
            if((Boolean)o) {
                buf.put((byte) 1);
            }
            else {
                buf.put((byte) 0);
            }
            return 1;
        }
        else {
            System.err.println("Unknown class in writeToByteOutput: " + o.getClass());
            return 0;
        }
    }    
    protected byte[] convertToPrimitiveByteArray() {
        ByteBuffer buf = ByteBuffer.allocate(4); // TODO: use a different conversion if argument contains Strings?
        buf.mark();
        byte[] bytes = new byte[totalBytes];
        int i = 0; // index for bytes
        int j = 0; // index for constantIndices
        int constantsIndex = 0;
        int variablesIndex = 0;
        int b; // bytes written per cycle
        for(; j < constantIndices.length; ++j) {
            if(constantIndices[j]) {
                // handle Strings as special case (may need to resize ByteBuffer to the size of this String)
                if((constants[constantsIndex]) instanceof String) {
                    String str = (String) constants[constantsIndex];
                    if(str.length() >= 1) {
                        buf = ByteBuffer.allocate(2 * (str.length() + 1));
                        buf.mark();
                    }
                }
                
                b = writeToByteOutput(buf, constants[constantsIndex]);
                ++constantsIndex;
            }
            else {
                // also handle Strings as special case
                if((variables[variablesIndex]) instanceof String) {
                    String str = (String) variables[variablesIndex];
                    if(str.length() >= 1) {
                        buf = ByteBuffer.allocate(2 * (str.length() + 1));
                        buf.mark();
                    }
                }
            
                b = writeToByteOutput(buf, variables[variablesIndex]);
                ++variablesIndex;
            }
            System.arraycopy(buf.array(), 0, bytes, i, b);
            i += b;
            buf.reset();
        }
        
        // Error check
        if(constantsIndex != constants.length) {
            System.err.println("constantIndices does not describe all constants!");
        }
        
        // Write the rest of the variables
        for(; variablesIndex < variables.length; ++variablesIndex) {
            // again handle Strings as special case
            if((variables[variablesIndex]) instanceof String) {
                String str = (String) variables[variablesIndex];
                if(str.length() >= 1) {
                    buf = ByteBuffer.allocate(2 * (str.length() + 1));
                    buf.mark();
                }
            }
            
            b = writeToByteOutput(buf, variables[variablesIndex]);
            System.arraycopy(buf.array(), 0, bytes, i, b);
            i += b;
            buf.reset();
        }
        return bytes;
    }
    
    public void writeContents(OutputStream output) {
        try {            
            output.write(convertToPrimitiveByteArray());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
