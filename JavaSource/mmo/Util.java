package mmo;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.BufferOverflowException;
import java.nio.ByteBuffer;
import java.util.Arrays;

import javax.servlet.AsyncContext;

import mmo.world.Position;

public class Util {
    public static String getStringFromByteBuffer(ByteBuffer buf) {
        StringBuilder sb = new StringBuilder();
        while(buf.remaining() > 1) {
            char c = buf.getChar();
            if(c == 0) {
                break;
            }
            sb.append(c);
        }
        return sb.toString();
    }
    
    public static void writePositionToStream(OutputStream output, Position pos) throws IOException {
        int x = Float.floatToIntBits(pos.getX());
        output.write(new byte[] {(byte)((x & (0xFF << 24)) >> 24),
                                 (byte)((x & (0xFF << 16)) >> 16),
                                 (byte)((x & (0xFF << 8)) >> 8),
                                 (byte)(x & 0xFF)});
        int y = Float.floatToIntBits(pos.getY());
        output.write(new byte[] {(byte)((y & (0xFF << 24)) >> 24),
                                 (byte)((y & (0xFF << 16)) >> 16),
                                 (byte)((y & (0xFF << 8)) >> 8),
                                 (byte)(y & 0xFF)});
    }
    public static void writePositionToBuffer(ByteBuffer buf, Position pos) {
        buf.putFloat(pos.getX());
        buf.putFloat(pos.getY());
    }
    public static void writeStringToBuffer(ByteBuffer buf, String str) {
        int i = 0;
        try {
            for(; i < str.length(); ++i) {
                buf.putChar(str.charAt(i));
            }
            buf.putChar((char)0);
        } catch(BufferOverflowException e) {
            System.err.println("String: " + str + " Index: " + i + " Buffer size: " + buf.capacity());
            System.err.println(Arrays.toString(buf.array()));
            e.printStackTrace();
        }
    }
    
    public static void completeAsyncContext(AsyncContext aCtx) {
        if(!aCtx.getResponse().isCommitted()) {
            try {
                aCtx.getRequest().getInputStream().close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            aCtx.complete();
        }
    }
}
