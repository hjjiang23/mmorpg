package mmo.world;

import java.awt.Point;
import java.awt.Rectangle;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.locks.ReentrantLock;

import mmo.server.WorldContainer;

public class MapGridCell implements WorldContainer {
    public MapGridCell(Point iPos, Rectangle area) {
        this.id = iPos;
        this.area = area;
    }
    
    private Point id;
    private final Rectangle area;
    private int pinCount = 0;
    private Set<Terrain> terrainSet = new HashSet<Terrain>();
    private Set<Unit> unitsSet = new HashSet<Unit>();
    private long lastAccess = 0;
    ReentrantLock addRemoveLock = new ReentrantLock();
    
    public Point getId() {
        return id;
    }
    
    // @Override // WorldInterface
    public boolean addUnit(Unit unit) {
        if(!area.contains(unit.getPos().toPoint())) {
            System.err.println("Unit being added to cell that does not cover its position:" + area.toString() + " " + unit.getPos().toPoint());
            return false;
        }
        addRemoveLock.lock();
        try {
            return unitsSet.add(unit);
        }
        finally {
            addRemoveLock.unlock();
        }
    }
    // @Override // WorldInterface
    public boolean removeUnit(Unit unit) {
        addRemoveLock.lock();
        try {
            return unitsSet.remove(unit);
        }
        finally {
            addRemoveLock.unlock();
        }
    }
    
    public void addTerrain(Terrain ter) {
        ter.incrPinCount();
        terrainSet.add(ter);
    }
    public Collection<Terrain> getTerrains() {
        return terrainSet;
    }
    public void removeTerrain(Terrain ter) {
        if(terrainSet.remove(ter)) {
            ter.decrPinCount();
        }
    }
    public void removeAllTerrains() {
        Iterator<Terrain> i = terrainSet.iterator();
        while(i.hasNext()) {
            i.next().decrPinCount();
            i.remove();
        }
    }
    
    public boolean canMoveTo(Unit unit, Position pos) {
        for(Terrain ter : getTerrains()) {
            if(ter.getArea().contains(pos.toPoint())) {
                return ter.getPassable();
            }
        }
        
        return true;
    }
    
    public int getPinCount() {
        return pinCount;
    }
    public void incrPinCount() {
        ++pinCount;
    }
    /**
     * Decreases the pin count by one.
     * @param access 
     * @param mc
     * @return true if there are no more pins for this cell
     */
    public boolean decrPinCount(long access) {
        --pinCount;
        if(pinCount == 0) {
            // Start WorldCleanupTask to remove this object
            lastAccess = access;
            return true;
        }
        return false;
    }
    
    public Rectangle getArea() {
        return area;
    }

    @Override
    public long getLastAccessTime() {
        return lastAccess;
    }

    public Collection<Unit> getUnits() {
        // TODO: this needs a lock since an iterator will be used on units
        return unitsSet;
    }
}

enum CELL_STATE {
    LOADED,
    INITIALIZED,
    DESTROYED
}
