package mmo.world;

import java.awt.Point;
import java.awt.Rectangle;
import java.awt.geom.Area;
import java.awt.geom.Rectangle2D;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.DelayQueue;

import mmo.Controller;
import mmo.SpatialMap;
import mmo.server.ScheduledTask;
import mmo.server.WorldContainer;
import mmo.world.Creature.WorldRespawnTask;

public class MapController extends Controller<ScheduledTask> implements Updatable, WorldInterface, WorldContainer {
    public MapController(WorldController wc) {
        super();
        this.wc = wc;
        
        for(Creature cre : wc.getCreaturesForArea(new Area(area))) {
            creaturesMap.add(cre);
        }
        for(Terrain ter : wc.getTerrainsForArea(new Area(area))) {
            terrainsMap.add(ter);
        }
        
        mapId = 0;
        setName("MapController " + mapId);
        initQueue(new DelayQueue<ScheduledTask>());
        
        start();
    }
    
    static final int GRIDSIZE;
    static {
        int temp = 200;
        InputStream is = null;
        try {
            is = MapController.class.getClassLoader().getResourceAsStream("resources.properties");
            System.getProperties().load(is);
            temp = Integer.valueOf(System.getProperty("GridSize"));
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if(is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        
        GRIDSIZE = temp;
    }
    
    private final Rectangle area = new Rectangle(-50 * GRIDSIZE, -50 * GRIDSIZE, 100 * GRIDSIZE, 100 * GRIDSIZE);
    
    private WorldController wc;
    private boolean isValid = true;
    private int mapId;
    private SpatialMap<Terrain> terrainsMap = new SpatialMap<Terrain>();
    private SpatialMap<Creature> creaturesMap = new SpatialMap<Creature>();
    private Map<Point, MapGridCell> gridMap = new HashMap<Point, MapGridCell>(); // TODO: replace with SpatialMap
    private Map<ObjectGUID, GameObject> gameObjectsMap = new ConcurrentHashMap<ObjectGUID, GameObject>();
    private Map<ObjectGUID, MapGridCell> goToCellMap = new ConcurrentHashMap<ObjectGUID, MapGridCell>(); // Should be weak references 
    private int actorCount = 0;
    private long lastAccess = 0;

    public boolean update() {
        return false;
    }
    @Override
    public void setUpdateTask(WorldUpdateTask task) {
    }
    public WorldUpdateTask getUpdateTask() {
        return null;
    }
    public void cancelUpdateTask() {
        
    }
    
    public MapGridCell getGridCellForGO(GameObject go) {
        return getGridCellForPosition(go.getPos());
    }
    public MapGridCell getGridCellForPosition(Position pos) {
        Point cellPos = positionToGridPoint(pos);
        return gridMap.get(cellPos);
    }
    private Collection<Point> updateGridCells(Position currentPos) {
        Point cellPos = positionToGridPoint(currentPos);
        
        // Init grid for 3x3 set of cells along this position, prepare removal for cells in the old 3x3 and not in the current set of cells
        Collection<Point> cellsToLoad = getCellPointsAround(cellPos);
        
        for(Point iPoint : cellsToLoad) {
            MapGridCell cell = gridMap.get(iPoint);
            if(cell == null) {
                Rectangle area = new Rectangle(iPoint.x * GRIDSIZE, iPoint.y * GRIDSIZE, GRIDSIZE, GRIDSIZE);
                cell = new MapGridCell(iPoint, area);
                gridMap.put(iPoint, cell);
                initArea(cell);
            }
            cell.incrPinCount();
        }
        return cellsToLoad;
    }
    private void updateGridCells(Collection<Point> addedCells, Collection<Point> removedCells) {
        for(Point iPoint : removedCells) {
            MapGridCell cell = gridMap.get(iPoint);
            if(cell != null) {
                long access = System.currentTimeMillis();
                cell.decrPinCount(access);
                addFutureEvent(new WorldCleanupTask(this, cell, access));
            }
        }
        
        for(Point iPoint : addedCells) {
            MapGridCell cell = gridMap.get(iPoint);
            if(cell == null) {
                Rectangle area = new Rectangle(iPoint.x * GRIDSIZE, iPoint.y * GRIDSIZE, GRIDSIZE, GRIDSIZE);
                cell = new MapGridCell(iPoint, area);
                gridMap.put(iPoint, cell);
                initArea(cell);
            }
            cell.incrPinCount();
        }
    }
    private void getChangedCells(Position currentPos, Position oldPos, Collection<Point> addedCells, Collection<Point> removedCells) {
        Point cellPos = positionToGridPoint(currentPos);
        Point oldCellPos = positionToGridPoint(oldPos);
        
        // Init grid for 3x3 set of cells along this position, prepare removal for cells in the old 3x3 and not in the current set of cells
        addedCells.addAll(getCellPointsAround(cellPos));
        removedCells.addAll(getCellPointsAround(oldCellPos));
        Iterator<Point> i = addedCells.iterator();
        while(i.hasNext()) {
            // If this cell is in the intersection of the two, remove if from both (it remains unchanged)
            if(removedCells.remove(i.next())) {
                i.remove();
            }
        }        
    }
    
    private Collection<Point> getCellPointsAround(Point cellPoint) {
        Set<Point> points = new HashSet<Point>();
        for(int x = cellPoint.x - 1; x <= cellPoint.x + 1; ++x) {
            for(int y = cellPoint.y - 1; y <= cellPoint.y + 1; ++y) {
                points.add(new Point(x, y));
            }
        }
        return points;
    }
    private Collection<Point> getCellsForArea(Area area) {
        Set<Point> points = new HashSet<Point>();
        Rectangle rectBounds = area.getBounds(); 
        Point topLeft = positionToGridPoint(new Position((float)rectBounds.getMinX(), (float)rectBounds.getMinY()));
        Point bottomRight = positionToGridPoint(new Position((float)rectBounds.getMaxX(), (float)rectBounds.getMaxY()));
        for(int x = topLeft.x; x <= bottomRight.x; ++x) {
            for(int y = topLeft.y; y <= bottomRight.y; ++y) {
                points.add(new Point(x, y));
            }
        }
        return points;
    }
    
    private void initArea(MapGridCell cell) {
        Area area = new Area(cell.getArea());
        for(Terrain ter : getTerrainsForArea(area)) {
            cell.addTerrain(ter);
        }
        for(Creature cre : getCreaturesForArea(area)) {
            wc.addCreatureToWorld(cre);
        }
    }
    
    public Area getAreaOfVisionAroundUnit(Unit unit) {
        int visionRange = unit.getVisionRange();
        float x = unit.getPos().getX();
        float y = unit.getPos().getY();
        Area area = new Area(new Rectangle2D.Float(x - visionRange, y - visionRange, 2 * visionRange, 2 * visionRange));
        
        return area;
    }

    /**
     * Use for update notifies and updates
     * @param cls - The class of GameObjects to fetch
     * @param object - The object that is at the center of the query
     * @param includeSelf - Whether object should be included in the result set
     * @return A collection of the instances of cls within range of the object
     */
    @Override
    public <T extends GameObject> Collection<T> getObjectsOfTypeAround(Class<T> cls, GameObject object, boolean includeSelf) {
        return getObjectsOfTypeAround(cls, object, includeSelf, 2 * GRIDSIZE);
    }
    /**
     * 
     * @param cls - The class of GameObjects to fetch
     * @param object - The object that is at the center of the query
     * @param includeSelf - Whether object should be included in the result set
     * @param range - The range of the query, using Manhattan distance
     * @return A collection of the instances of cls within range of the object
     */
    @Override
    @SuppressWarnings("unchecked")
    public <T extends GameObject> Collection<T> getObjectsOfTypeAround(Class<T> cls, GameObject object, boolean includeSelf, float range) {
        final float RANGE_TOLERANCE = 0.001f; 
        Set<T> objects = new HashSet<T>();
        Position pos = object.getPos();
        Area area = new Area(new Rectangle2D.Float(pos.getX() - range - RANGE_TOLERANCE, pos.getY() - range - RANGE_TOLERANCE, 2 * (range + RANGE_TOLERANCE), 2 * (range + RANGE_TOLERANCE)));
        Collection<Point> cellsToCheck = getCellsForArea(area);
        if(Unit.class.isAssignableFrom(cls)) {
            for(Point iPoint : cellsToCheck) {
                MapGridCell cell = gridMap.get(iPoint);
                if(cell != null) {
                    for(Unit o : cell.getUnits()) {
                        if(cls.isInstance(o) && area.contains(o.getPos().toPoint())) {
                            objects.add((T) o);
                        }
                    }
                }
            }
        }
        else {
            for(GameObject o : gameObjectsMap.values()) {
                if(cls.isInstance(o)) {
                    objects.add((T) o);
                }
            }
        }
        if(!includeSelf) {
            objects.remove(object);
        }
        return objects;
    }    
   
    private Collection<Creature> getCreaturesForArea(Area area) {
        return creaturesMap.get(area);
    }
    public Collection<Terrain> getTerrainsForArea(Area area) {
        return terrainsMap.get(area);
    }
    
    public Rectangle getArea() {
        return area;
    }
    
    private Point positionToGridPoint(Position pos) {
        return new Point((int) Math.floor(pos.getX() / GRIDSIZE), ((int) Math.floor(pos.getY() / GRIDSIZE)));
    }


    @Override
    public boolean addUnit(Unit unit) {
        unit.setController(this);
        gameObjectsMap.put(unit.getGUID(), unit);
        
        if(unit.countsAsActor()) {
            incrActorCount();
            updateGridCells(unit.getPos()); // creates grid Cells
            System.out.println("Actor count is " + getActorCount());
        }
        
        final int MAX_RETRIES = 10;
        for(int i = 0; i < MAX_RETRIES; ++i) { // loop until
            MapGridCell cell = getGridCellForGO(unit);
            if(cell != null) {
                if(cell.canMoveTo(unit, unit.getPos())) {
                    cell.addUnit(unit);
                    goToCellMap.put(unit.getGUID(), cell);
                    return true;
                }
            }
            
            unit.setPos(getRandomLocationNear(unit.getPos(), 100.f));
        }
        
        // System.err.println("Failed to add unit");
        
        return false;
    }
    @Override
    public void moveUnit(Unit movingUnit, Position newPos) {
        if(!area.contains(newPos.toPoint())) {
            System.out.println("ERROR: out of bounds");
            return;
        }
        // null only if the unit is in the process of being removed or was never added
        if(goToCellMap.get(movingUnit.getGUID()) == null) {
            return;
        }
        
        MapGridCell cell = getGridCellForPosition(newPos);
        // FIXME: this will only work if the Actors walk through cells
        if(cell != null) { // TODO: implement "soft loading" of GridCells
            boolean yank = false; // true if unit attempted moving to illegal position, and should be pulled back to the closest valid position
            Position oldPos = movingUnit.getPos();
            
            if(!cell.canMoveTo(movingUnit, newPos)) {
                yank = true;
                newPos = oldPos; // TODO: set this to some half way point that matches the client side calculation
                cell = getGridCellForPosition(newPos);
                
                if(movingUnit instanceof Player)
                    System.out.println("Sending yank to " + movingUnit);
            }
            
            movingUnit.setPos(newPos);
            // Transitioned between cells
            if(cell != goToCellMap.get(movingUnit.getGUID())) {
                goToCellMap.get(movingUnit.getGUID()).removeUnit(movingUnit);
                
                cell.addUnit(movingUnit);
                goToCellMap.put(movingUnit.getGUID(), cell);
                
                Collection<Point> addedCellPoints = new HashSet<Point>();
                Collection<Point> removedCellPoints = new HashSet<Point>();
                getChangedCells(newPos, oldPos, addedCellPoints, removedCellPoints);
                
                // May need to load additional cells if this unit is an actor                
                if(movingUnit.countsAsActor()) {
                    updateGridCells(addedCellPoints, removedCellPoints);
                    
                    // Init terrains for Players
                    if(movingUnit instanceof Player) {
                        Player player = (Player) movingUnit;
                        processCellsForMovingPlayer(player, addedCellPoints, removedCellPoints);
                    }
                }
                else {
                    notifyPlayersInCells(movingUnit, addedCellPoints, removedCellPoints);
                }
            }
            
            wc.notifyMove(movingUnit, yank);
        }
    }
    @Override
    public boolean canMoveTo(Unit unit, Position pos) {
        MapGridCell cell = getGridCellForGO(unit);
        if(cell != null)
            return cell.canMoveTo(unit, unit.getPos());
        
        return false;
    }
    
    private void processCellsForMovingPlayer(Player player, Collection<Point> addedCellPoints, Collection<Point> removedCellPoints) {
        for(Point iPoint : addedCellPoints) {
            MapGridCell addedCell = gridMap.get(iPoint);
            if(addedCell != null) {
                for(Terrain ter : addedCell.getTerrains()) {
                    wc.sendInitTerrain(player.getPlayerSession(), ter);
                }
                for(Unit unit : addedCell.getUnits()) {
                    wc.notifyUnitCreated(player.getPlayerSession(), unit);
                    if(unit instanceof Player) {
                        Player other = (Player) unit;
                        wc.notifyUnitCreated(other.getPlayerSession(), player);
                    }
                }
            }
        }
        for(Point iPoint : removedCellPoints) {
            MapGridCell removedCell = gridMap.get(iPoint);
            if(removedCell != null) {
                for(Terrain ter : removedCell.getTerrains()) {
                    // wc.sendDestroyTerrain(player.getPlayerSession(), ter);
                }
                for(Unit unit : removedCell.getUnits()) {
                    wc.notifyUnitDestroyed(player.getPlayerSession(), unit);
                    if(unit instanceof Player) {
                        Player other = (Player) unit;
                        wc.notifyUnitDestroyed(other.getPlayerSession(), player);
                    }
                }
            }
        }
    }
    private void notifyPlayersInCells(Unit movingUnit, Collection<Point> addedCellPoints, Collection<Point> removedCellPoints) {
        for(Point iPoint : addedCellPoints) {
            MapGridCell addedCell = gridMap.get(iPoint);
            if(addedCell != null) {
                for(Unit unit : addedCell.getUnits()) {
                    if(unit instanceof Player) {
                        Player other = (Player) unit;
                        wc.notifyUnitCreated(other.getPlayerSession(), movingUnit);
                    }
                }
            }
        }
        for(Point iPoint : removedCellPoints) {
            MapGridCell removedCell = gridMap.get(iPoint);
            if(removedCell != null) {
                for(Unit unit : removedCell.getUnits()) {
                    if(unit instanceof Player) {
                        Player other = (Player) unit;
                        wc.notifyUnitDestroyed(other.getPlayerSession(), movingUnit);
                    }
                }
            }
        }
    }
    
    public boolean despawn(Unit respawnUnit){
        if(respawnUnit instanceof Creature) {
            respawnUnit.setUpdateTask(((Creature)respawnUnit).createWorldRespawnTask());
            addFutureEvent(respawnUnit.getUpdateTask());
        }
        return wc.removeUnit(respawnUnit);
    }
    
    public boolean respawn(Unit respawnUnit){
        Position resLocation = getRandomLocationNear(new Position(500.f, 500.f), 400);
        // System.out.println(respawnUnit + " respawned @ X:" + newPosX + " Y:" + newPosY);
        respawnUnit.setPos(resLocation);
        respawnUnit.setCurrHP(respawnUnit.getMaxHP());
        return wc.addUnit(respawnUnit);
    }
    public Position getRandomLocationNear(Position pos, float range) {
        float newPosX = (float) (pos.getX() + (range * 2. * (Math.random() - 0.5)));
        float newPosY = (float) (pos.getY() + (range * 2. * (Math.random() - 0.5)));
        Position resLocation = new Position(newPosX, newPosY);
        return resLocation;
    }
    
    public void notifyCurrHPChange(Unit unit) {
        wc.notifyCurrHPChange(unit);
    }
    public void notifyMaxHPChange(Unit unit) {
        wc.notifyMaxHPChange(unit);
    }
    
    @Override
    public boolean removeUnit(Unit unit) {
        if(gameObjectsMap.remove(unit.getGUID()) != null) {
            if(unit.countsAsActor()) {
                decrActorCount();
            }
            
            MapGridCell cell = goToCellMap.remove(unit.getGUID());
            if(cell != null) {
                cell.removeUnit(unit);
            }
            
            return true;
        }
        return false;
    }
    
    @Override
    public Collection<GameObject> getAllObjects() {
        return gameObjectsMap.values();
    }
    
    public int getActorCount() {
        return actorCount;
    }
    private void incrActorCount() {
        ++actorCount;
    }
    private void decrActorCount() {
        --actorCount;
        if(actorCount == 0) {
            // Start WorldCleanupTask to remove this object
            lastAccess = System.currentTimeMillis();
            wc.addFutureEvent(new WorldCleanupTask(wc, this, lastAccess));
        }
    }
    
    @Override
    public boolean cleanup(Object o) {
        if(o instanceof MapGridCell) {
            MapGridCell cell = (MapGridCell) o;
            if(cell.getPinCount() <= 0) {
                synchronized(cell) {
                    Unit[] removedUnits = new Unit[cell.getUnits().size()];
                    for(Unit unit : cell.getUnits().toArray(removedUnits)) {
                        if(unit.countsAsActor()) {
                            System.err.println("Error: cleaning up cell with an actor present");
                        }
                        wc.removeUnit(unit);
                    }
                    cell.removeAllTerrains();
                }
                gridMap.remove(cell.getId());
                return true;
            }
        }
        return false;
    }
    
    public void invalidate() {
        isValid = false;
    }
    
    @Override
    public void finalize() {
        System.out.println("MapController destroyed");
    }

    @Override
    public boolean isValid() {
        return isValid;
    }

    @Override
    public long getLastAccessTime() {
        return lastAccess;
    }
}
