package mmo.world;

import java.util.Collection;

/**
 * Interface implemented by any controller of any size slice of the World
 * @author Alan Wang
 *
 */
public interface WorldInterface {
    /**
     * Returns true if the WorldInterface is loaded and is not in the process of being removed
     * @return
     */
    boolean isValid();
    
    public Collection<GameObject> getAllObjects();
    public <T extends GameObject> Collection<T> getObjectsOfTypeAround(Class<T> cls, GameObject object, boolean includeSelf);
    public <T extends GameObject> Collection<T> getObjectsOfTypeAround(Class<T> cls, GameObject object, boolean includeSelf, float range);
    public boolean addUnit(Unit unit);
    void moveUnit(Unit movingUnit, Position pos);
    boolean canMoveTo(Unit unit, Position pos);
    public boolean removeUnit(Unit unit);
    
    public boolean cleanup(Object o);
}
