package mmo.world;

public interface Template<T>{
    public T createFromTemplate();
}
