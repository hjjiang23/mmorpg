package mmo.world;

import mmo.world.ObjectGUID.GUIDTYPE;

// Inanimate objects that furnish the world
public class WorldObject extends GameObject {
    protected final static GUIDTYPE type = GUIDTYPE.WORLDOBJECT;
    
    public WorldObject(int id) {
        super(id);
    }

    @Override
    protected GUIDTYPE getType() {
        return GUIDTYPE.WORLDOBJECT;
    }
}
