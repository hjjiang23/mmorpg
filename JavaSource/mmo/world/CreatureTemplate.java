package mmo.world;

public class CreatureTemplate implements Template<Creature> {
    public CreatureTemplate() {}
    
    private int id;
    private String name;
    private String AIname;
    private String sprite;
    private int minDmg;
    private int maxDmg;
    private double range;
    private int maxHP;
    private int maxMana;
    private int exp;
    private float mvSpeed;
    
    public String getSprite() {
        return sprite;
    }
    public void setSprite(String sprite) {
        if(sprite == null) {
            sprite = "";
        }
        this.sprite = sprite;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getAIname() {
        return AIname;
    }
    public void setAIname(String AIname) {
        this.AIname = AIname;
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    @Override
    public Creature createFromTemplate() {
        return null;
    }
	public int getMinDmg() {
		return minDmg;
	}
	public void setMinDmg(int minDmg) {
		this.minDmg = minDmg;
	}
	public int getMaxDmg() {
		return maxDmg;
	}
	public void setMaxDmg(int maxDmg) {
		this.maxDmg = maxDmg;
	}
	public double getRange() {
		return range;
	}
	public void setRange(double range) {
		this.range = range;
	}
	public int getMaxHP() {
		return maxHP;
	}
	public void setMaxHP(int maxHP) {
		this.maxHP = maxHP;
	}
	public int getMaxMana() {
		return maxMana;
	}
	public void setMaxMana(int maxMana) {
		this.maxMana = maxMana;
	}
    public int getExp() {
        return exp;
    }
    public void setExp(int exp) {
        this.exp = exp;
    }
    
    public float getMvSpeed() {
        return mvSpeed;
    }
    public void setMvSpeed(float mvSpeed) {
        this.mvSpeed = mvSpeed;
    }
}
