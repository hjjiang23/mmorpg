package mmo.world;

import java.awt.geom.Area;
import java.awt.geom.Path2D;
import java.awt.geom.PathIterator;
import javax.persistence.Transient;

public class Terrain implements Spatial {
    public Terrain() {
        // System.out.println("New terrain created " + toString());
    }
    
    private TerrainTemplate terrainTemplate;
    private int id;
    private String areaText;
    // private String texture;
    // private Boolean passable;

    @Transient private Area area;
    @Transient private String svgPath;
    @Transient private int pinCount;

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    
    public TerrainTemplate getTerrainTemplate() {
        return terrainTemplate;
    }
    public void setTerrainTemplate(TerrainTemplate terrainTemplate) {
        this.terrainTemplate = terrainTemplate;
    }
        
    /**
     * @return svg path from Area
     */
    @Transient
    public String getSVGPath() {
        return svgPath;
    }
    @Transient
    public Area getArea() {
        return area;
    }
        
    private String buildAreaAsSVGPath() {
        if(area == null) {
            return "";
        }
        
        StringBuilder strB = new StringBuilder("M");
        PathIterator pi = area.getPathIterator(null);
        float[] fbuf = new float[6];
        pi.currentSegment(fbuf);
        float[] start = new float[] {fbuf[0], fbuf[1]}; 
        while(!pi.isDone()) {
            strB.append(((Float)fbuf[0]).toString() + " " + ((Float)fbuf[1]).toString());
            pi.next();
            if(!pi.isDone()) {
                strB.append(", ");
                if(pi.currentSegment(fbuf) == 4) {
                    strB.append(((Float)start[0]).toString() + " " + ((Float)start[1]).toString());
                    break;
                }
            }
        }
        return strB.toString();
    }
    
    /********************
     * Persistence Methods
     ********************/    
    
    public String getAreaText() {
        if(areaText == null && area != null) {
            areaText = areaToString(area);
        }
        return areaText;
    }
    public void setAreaText(String areaText) {
        this.areaText = areaText;
        if(areaText.startsWith("POLYGON")) {
            area = wktPolygonToArea(areaText.substring("POLYGON".length()));
            svgPath = buildAreaAsSVGPath();
        }
    }
    
    public Area wktPolygonToArea(String str) {
        str = str.substring(2, str.length() - 2); // get rid of outer-most parenthesis
        String[] groups = str.split("\\), \\(");
        Area base = wktGroupToArea(groups[0]);
        for(int i = 1; i < groups.length; ++i) {
            base.subtract(wktGroupToArea(groups[i]));
        }
        return base;
    }
    
    public Area wktGroupToArea(String str) {
        Path2D.Float base = new Path2D.Float();
        str = str.replace(",", "");
        String[] coords = str.split("\\s");
        base.moveTo(Float.valueOf(coords[0]), Float.valueOf(coords[1]));
        for(int i = 2; i < coords.length; i += 2) {
            base.lineTo(Float.valueOf(coords[i]), Float.valueOf(coords[i + 1]));
        }
        
        PathIterator pi = base.getPathIterator(null);
        float[] fbuf = new float[6];
        while(!pi.isDone()) {
            pi.currentSegment(fbuf);
            pi.next();
        }
        
        return new Area(base);
    }
    
    /**
     * Helper method for building spatial queries and terrain persistence
     * @param area
     * @return WKT of the Area
     */
    public static String areaToString(Area area) {
        StringBuilder strB = new StringBuilder("POLYGON((");
        PathIterator pi = area.getPathIterator(null);
        float[] fbuf = new float[6];
        pi.currentSegment(fbuf);
        float[] start = new float[] {fbuf[0], fbuf[1]}; 
        while(!pi.isDone()) {
            strB.append(((Float)fbuf[0]).toString() + " " + ((Float)fbuf[1]).toString());
            pi.next();
            if(!pi.isDone()) {
                strB.append(", ");
                if(pi.currentSegment(fbuf) == 4) {
                    strB.append(((Float)start[0]).toString() + " " + ((Float)start[1]).toString());
                    break;
                }
            }
        }
        strB.append("))");
        return strB.toString();
    }
    public void incrPinCount() {
        ++pinCount;
    }
    public boolean decrPinCount() {
        --pinCount;
        if(pinCount == 0) {
            return true;
        }
        return false;
    }
    @Transient
    public int getPinCount() {
        return pinCount;
    }
    @Transient
    public Boolean getPassable() {
        return terrainTemplate.getPassability() == 0;
    }
    @Transient
    public String getTexture() {
        return terrainTemplate.getTexture();
    }
}
