package mmo.world;

/**
 * A GameObject interface implemented by GameObjects that cause portions of the world to be loaded when the entity is created.
 * Examples include Players or very important Creatures.
 * @author Alan Wang
 *
 */
public interface Actor {

}
