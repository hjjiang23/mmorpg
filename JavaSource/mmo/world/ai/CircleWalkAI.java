package mmo.world.ai;

import mmo.world.Creature;
import mmo.world.CreatureAI;
import mmo.world.Position;
import mmo.world.WorldInterface;

public class CircleWalkAI extends CreatureAI {

    public CircleWalkAI(Creature cre, WorldInterface area) {
        super(cre, area);
    }
    
    // int angle = (int)(Math.random() * 360);

    @Override
    public boolean update() {
        // System.out.println(creRef.get() + " should be walking");
        WorldInterface area = areaRef.get();
        Creature cre = creRef.get();
        if(area != null && cre != null && !cre.isDead() && area.isValid()) {
            Position pos = cre.getPos();
            
            double angle = cre.getOrientation();
            float dx = 5 * (float) Math.sin(angle);
            float dy = 5 * (float) Math.cos(angle);
            cre.setOrientation((angle + (Math.PI/9.)) % (2 * Math.PI));
            
            area.moveUnit(cre, new Position(pos.getX() + dx, pos.getY() + dy));
            return true;
        }
        else {
            return false;
        }
    }
}
