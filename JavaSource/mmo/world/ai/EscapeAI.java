package mmo.world.ai;

import mmo.world.Creature;
import mmo.world.CreatureAI;
import mmo.world.WorldInterface;

public class EscapeAI extends CreatureAI
{
	public EscapeAI(Creature cre, WorldInterface area)
	{
		super(cre, area);
	}

	@Override
	public boolean update()
	{
		return escapeWalk();
	}
}
