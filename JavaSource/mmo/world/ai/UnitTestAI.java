

package mmo.world.ai;

import mmo.world.Creature;
import mmo.world.CreatureAI;
import mmo.world.WorldInterface;

public class UnitTestAI extends CreatureAI {

    public UnitTestAI(Creature cre, WorldInterface area) {
        super(cre, area);
    }
    
    enum STATE {
        MOVEMENT,
        ATTACK,
        DEATH
    }

    @Override
    public boolean update() {
        // System.out.println(creRef.get() + " should be walking");
        WorldInterface area = areaRef.get();
        Creature cre = creRef.get();
        return true;
    }
}
