package mmo.world.ai;

import java.lang.ref.WeakReference;
import java.util.Collection;

import mmo.world.Creature;
import mmo.world.CreatureAI;
import mmo.world.Position;
import mmo.world.Unit;
import mmo.world.WorldInterface;

public class AggroAI extends CreatureAI
{
	final int threshold = 1000;
	
	public AggroAI(Creature cre, WorldInterface area)
	{
		super(cre, area);

		targetRef = new WeakReference<Unit>(null);
	}

	WeakReference<Unit> targetRef = null;

	@Override
	public boolean update()
	{
		Creature cre = creRef.get();
		WorldInterface area = areaRef.get();
		if (cre == null || area == null || cre.isDead() || !area.isValid())
		{
			return false;
		}

		Unit target = targetRef.get();
		
		
		if (target == null || !target.isValid() || target.isDead())
		{
			// Find next target
			Collection<Unit> potentialTargets = area.getObjectsOfTypeAround(Unit.class, creRef.get(), false, cre.getVisionRange());
			// System.out.println(potentialTargets);
			for (Unit potentialTarget : potentialTargets)
			{
				if (!potentialTarget.isDead() && potentialTarget.getPos().toPoint().distance(cre.getPos().toPoint()) < threshold)
				{
					targetRef = new WeakReference<Unit>(potentialTarget);
					target = potentialTarget;
					// System.out.println("HunterAI: " + this + " now hunting " + target);
					break;
				}
			}

			if (target != null && target.isDead() )
			{
				targetRef = new WeakReference<Unit>(null);
				target = null;
			}
		}

		// Do not use else here
		if (target != null)
		{
			Position targetPos = target.getPos();
			if(targetPos.toPoint().distance(cre.getPos().toPoint()) > threshold)
			{
				target=null;
				return true;
			}
			
			cre.facePosition(targetPos);

			// Use some short circuiting to speed up calculation
			if (!cre.isGOInMeleeRange(target))
			{
				float dx = (float) (5 * Math.sin(cre.getOrientation()));
				float dy = (float) (5 * Math.cos(cre.getOrientation()));

				area.moveUnit(cre, new Position(cre.getPos().getX() + dx, cre.getPos().getY() + dy));
			}
			// Maybe prevent it from moving closer at some range
			else
			{
				cre.attack();
			}
		}
		if(target==null)
		{
			return randomWalk();
		}

		return true;
	}
}
