package mmo.world.ai;

import mmo.world.Creature;
import mmo.world.CreatureAI;
import mmo.world.WorldInterface;

public class CowardAI extends CreatureAI
{
	public CowardAI(Creature cre, WorldInterface area)
	{
		super(cre, area);
	}

	@Override
	//Randomly walk but run away if damaged
	public boolean update()
	{
		Creature cre = creRef.get();
		if (cre.getCurrHP() / (cre.getMaxHP() + 0.0) > 0.5)
		{
			return randomWalk();
		}
		return escapeWalk();
	}
}
