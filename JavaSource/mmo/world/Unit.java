package mmo.world;

import java.util.Collection;
import java.util.Iterator;

import javax.persistence.Transient;

import mmo.Controller;
import mmo.server.ScheduledTask;
import mmo.world.ObjectGUID.GUIDTYPE;

public class Unit extends GameObject implements Updatable {
    public Unit() {
        this(0);
    }
    public Unit(int id) {
        super(id);
        
        name = "";
    }
    
    @Override @Transient
    protected GUIDTYPE getType() {
        return GUIDTYPE.UNIT;
    }
    
    @Transient protected transient String name;
    @Transient private WorldUpdateTask task = null;
    
    int currHP = 0;
    int currMana = 0;
    int maxHP = 0;
    int maxMana = 0;
    protected final double range = 5;
    private UnitGroup group;
    
    public int getCurrHP() {
        return currHP;
    }
    public void setCurrHP(int currHP) {
        this.currHP = currHP;
    }
    @Transient
    public void setCurrHPAndNotify(int currHP) {
        setCurrHP(currHP);
        getController().notifyCurrHPChange(this);
    }
    
    public int getCurrMana() {
        return currMana;
    }
    public void setCurrMana(int currMana) {
        this.currMana = currMana;
    }
    
    public int getMaxHP() {
        return maxHP;
    }
    
    public void setMaxHP(int maxhp){
    	this.maxHP = maxhp;
    }
    
    public int getMaxMana() {
		return maxMana;
	}
	public void setMaxMana(int maxMana) {
		this.maxMana = maxMana;
	}
	// TODO: NYI
    @Transient
    public int getVisionRange() {
        return MapController.GRIDSIZE;
    }
    // TODO: NYI
    @Transient
    public void setVisionRange(int visionRange) {
    }
    
    @Transient
    public void setGroup(UnitGroup group) {
        this.group = group;
    }
    
    /**
     * Returns this unit's group, or null if it doesn't belong to any.
     * @return
     */
    @Transient
    public UnitGroup getGroup() {
        return group;
    }
    
    public String getName() {
        return name;
    }
    public void setName(String name) {
        // System.out.println("Unit setName called with " + name);
        this.name = name;
    }
    
    public boolean update() {
        // May add more criteria later
        return isValid();
    }
    @Override @Transient
    public void setUpdateTask(WorldUpdateTask task) {
        this.task = task;
    }
    @Override @Transient
    public WorldUpdateTask getUpdateTask() {
        return this.task;
    }
    @Override
    public void cancelUpdateTask() {
        if(this.task != null) {
            this.task.cancel(false);
        }
        this.task = null;
    }
    
    public void initialize() {
        super.initialize();
        
        Controller<ScheduledTask>  controller = getController();
        if(controller != null) {
            if(getUpdateTask() != null) {
                // Can have task if task is meant to respawn this unit, for instance
                // cancelUpdateTask(); // Do not want to call cancel on the task
                task = null;
            }
            WorldUpdateTask task = new WorldUpdateTask(this, (int) (Math.random() * WorldUpdateTask.randomDelayFactor));
            setUpdateTask(task);
            controller.addFutureEvent(task);
        }
    }
    
    public double getRange() {
        return range;
    }
    
    public void setRange(double range){
        // this.range = range;
    }
    
    public void attack() { // TODO: make this abstract
        /*int rollDamage = (int)(Math.random() * (getMaxDmg() - getMinDmg())) + getMinDmg();
        Collection<Unit> unitCollection = getController().getObjectsOfTypeAround(Unit.class, this, false, (float)getRange());
        for (Unit each : unitCollection) {
            if(!each.isDead()) {
                each.takeDmg(rollDamage);
            }
        } */
    }
    
    // public abstract int getMinDmg();
    // public abstract int getMaxDmg();
    
    public void retainAllInMeleeRange(Collection<? extends GameObject> uCollection){
        Iterator<? extends GameObject> i = uCollection.iterator();
        while(i.hasNext()) {
            GameObject each = i.next();
            
            if(!isGOInMeleeRange(each)) {
                // System.out.println("Removed unit since it wasn't within range");
                i.remove();
                continue;
            }
            
            if (!isFacingGO(each)) {
                // System.out.println("Removed unit since it wasn't within orientation: curOrientation: " + curOrientation + " angle to unit: " + unitAngle);
                i.remove();
                continue;
            }
        }
    }
    
    public boolean isGOInMeleeRange(GameObject go) {
        double dx = go.getPos().getX() - getPos().getX(); // player X position relative to the creature
        double dy = go.getPos().getY() - getPos().getY(); // player Y position relative to the creature

        // Use some short circuiting to speed up calculation
        if(Math.abs(dx) > getRange() || Math.abs(dy) > getRange() || (dx * dx) + (dy * dy) > getRange() * getRange()) {
            return false;
        }
        return true;
    }
    public boolean isFacingGO(GameObject go) {
        return isFacingPosition(go.getPos());
    }
    public boolean isFacingPosition(Position pos) {
        final double TOLERANCE = Math.PI/2;
        double dx = pos.getX() - getPos().getX(); // player X position relative to the creature
        double dy = pos.getY() - getPos().getY(); // player Y position relative to the creature
        if(dx != 0 && dy != 0) {        
            double unitAngle = Math.atan2(dx, dy);
            double dAngle = unitAngle - getOrientation() % (2 * Math.PI);
            if (Math.abs(dAngle) > TOLERANCE) {
                return false;
            }
        }
        return true;
    }
    public void facePosition(Position pos) {
        double dx = pos.getX() - getPos().getX(); // player X position relative to the creature
        double dy = pos.getY() - getPos().getY(); // player Y position relative to the creature
        if(dx != 0 || dy != 0) {
            double unitAngle = Math.atan2(dx, dy);
            setOrientation(unitAngle);            
        }
    }

    public int takeDmg(int damage, GameObject dmgSrc) {
        if(isDead()) {
            System.err.println("takeDmg called for dead unit " + this);
            return 0;
        }
        
        int resultHP = getCurrHP() - damage;
        if (resultHP <= 0) {
            damage = getCurrHP();
            setCurrHPAndNotify(0);
            die();
    	}
    	else {
    	    setCurrHPAndNotify(resultHP);
    	}
        return damage;
    }
    
    public boolean isDead() {
        return getCurrHP() <= 0;
    }
    
    public void die() {
    	System.out.println(this + " unit died");
    }
    
    @Override
    public void invalidate() {
        super.invalidate();
        cancelUpdateTask();
    }
    
    public boolean countsAsActor() {
        return this instanceof Actor || getGroup() instanceof Actor;
    }
}
