package mmo.world;

import java.awt.geom.Area;

public interface Spatial {
    public Area getArea();
}
