package mmo.world;

public class Item {
    public Item() {}
    
    int id;
    Player player;
    ItemTemplate itemTemplate;

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public Player getPlayer() {
        return player;
    }
    public void setPlayer(Player player) {
        this.player = player;
    }

    public ItemTemplate getItemTemplate() {
        return itemTemplate;
    }
    public void setItemTemplate(ItemTemplate itemTemplate) {
        this.itemTemplate = itemTemplate;
    }
}
