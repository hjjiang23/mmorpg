package mmo.world;

import java.awt.geom.Point2D;

import javax.persistence.Transient;

/**
 * An Embeddable entity class for JPA. Treat this class as final (Do not call setX or setY after isFinal has been set. The only reason these fields are not final is because JPA exclusively uses the default constructor.)
 * @author Alan Wang
 *
 */
public final class Position {
    public Position() {
        isFinal = null;
    }
    
    public Position(float x, float y) {
        this.x = x;
        this.y = y;
        isFinal = true;
    }

    private float x;
    private float y;
    @Transient private Boolean isFinal;
    
    public float getX() {
        return x;
    }
    public void setX(float x) {
        if(isFinal == null) {
            isFinal = false;
        }
        else if(!isFinal) {
            isFinal = true;
        }
        else {
            System.err.println("Position: setX called for final position");
            return;
        }
        this.x = x;
    }

    public float getY() {
        return y;
    }
    public void setY(float y) {
        if(isFinal == null) {
            isFinal = false;
        }
        else if(!isFinal) {
            isFinal = true;
        }
        else {
            System.err.println("Position: setY called for final position");
            return;
        }
        this.y = y;
    }

    @Override
    public int hashCode() {
        if(!isFinal) {
            isFinal = true;
        }
        final int prime = 31;
        int result = 1;
        result = prime * result + Float.floatToIntBits(x);
        result = prime * result + Float.floatToIntBits(y);
        return result;
    }
    @Override
    public boolean equals(Object obj) {
        if(!isFinal) {
            isFinal = true;
        }
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Position other = (Position) obj;
        if (Float.floatToIntBits(x) != Float.floatToIntBits(other.x))
            return false;
        if (Float.floatToIntBits(y) != Float.floatToIntBits(other.y))
            return false;
        return true;
    }
    
    @Override
    public String toString() {
        return "Position [x=" + x + ", y=" + y + "]";
    }
    
    public Point2D.Float toPoint() {
        return new Point2D.Float(x, y);
    }
}

