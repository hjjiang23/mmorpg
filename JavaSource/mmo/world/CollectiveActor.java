package mmo.world;

import java.util.Collection;

/**
 * A group of non-Actor GameObjects that can cause portions of the World to be loaded.
 * For instance, a horde of mooks.
 * @author Alan Wang
 *
 */
public class CollectiveActor extends UnitGroup implements Actor {
}
