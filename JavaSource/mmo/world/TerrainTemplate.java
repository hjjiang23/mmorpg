package mmo.world;

public class TerrainTemplate implements Template<Terrain> {
    int id;
    String texture;
    String name;
    String desc;
    int passability;
    
    public String getTexture() {
        return texture;
    }

    public void setTexture(String texture) {
        this.texture = texture;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public int getPassability() {
        return passability;
    }

    public void setPassability(int passability) {
        this.passability = passability;
    }

    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }

    @Override
    public Terrain createFromTemplate() {
        // TODO Auto-generated method stub
        return null;
    }
}
