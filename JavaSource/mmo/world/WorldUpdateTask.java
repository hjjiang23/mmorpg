package mmo.world;

import java.util.concurrent.ExecutionException;
import mmo.server.ScheduledTask;

public class WorldUpdateTask extends ScheduledTask {
    public WorldUpdateTask(Updatable updatable, int delayOffset) {
        setDelayFromNow(defaultDelay + delayOffset);
        
        this.updatable = updatable;
        
        // System.out.println("WorldUpdateTask initialized");
    }
    public WorldUpdateTask(Updatable updatable) {
        this(updatable, 0);
    }
    public WorldUpdateTask() {}
    
    private static final int defaultDelay = 200;
    protected static final int randomDelayFactor = defaultDelay;
    
    Updatable updatable;

    @Override
    public Boolean get() throws InterruptedException, ExecutionException {
        // System.out.println("WorldUpdateTask getting");
        updatable.update();
        return true;
    }

    // TODO: set timeout for AI tasks?
    /*@Override
    public Boolean get(long arg0, TimeUnit arg1) throws InterruptedException,
            ExecutionException, TimeoutException {
        return null;
    }*/
}
