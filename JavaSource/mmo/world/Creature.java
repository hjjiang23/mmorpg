package mmo.world;

import java.lang.ref.WeakReference;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import javax.persistence.Transient;

import mmo.Controller;
import mmo.server.ScheduledTask;
import mmo.world.ObjectGUID.GUIDTYPE;

public class Creature extends Unit {
    public Creature() {
        this(0);
    }
    public Creature(int id) {
        super(id);
    }
    
    protected CreatureAI ai;
    protected CreatureTemplate creatureTemplate;
    
    @Transient Map<GameObject, Integer> dmgSrcs = new HashMap<GameObject, Integer>();
    
    // @Override
    public int getId() {
        return super.getId();
    }
    // @Override
    public void setId(int i) {
        super.setId(i);
    }

    public CreatureTemplate getCreatureTemplate() {
        return creatureTemplate;
    }
    public void setCreatureTemplate(CreatureTemplate creatureTemplate) {
        this.creatureTemplate = creatureTemplate;
    }
    
    @Override
    public boolean update() {
        boolean updateInFuture = super.update();
        
        if(updateInFuture && ai != null) {
            updateInFuture = ai.update();
        }
        
        if(updateInFuture) {
            Controller<ScheduledTask> controller = getController();
            if(controller != null) {
                setUpdateTask(new WorldUpdateTask(this));
                controller.addFutureEvent(getUpdateTask());
            }
        }
        
        return updateInFuture;
    }
    
    @Transient
    public void setAI(CreatureAI ai) {
        this.ai = ai;
    }
        
    @Override
    @Transient
    protected GUIDTYPE getType() {
        return GUIDTYPE.CREATURE;
    }
    
    @Override
    @Transient
    public String getName() {
        // System.out.println("Creature getName called " + creatureTemplate.getName());
        return creatureTemplate.getName();
    }
    
    @Override
    public void setPos(Position pos) {
        super.setPos(pos);
    }
    @Override
    public Position getPos() {
        return super.getPos();
    }
    
    @Override
    public double getOrientation() {
        return super.getOrientation();
    }
    @Override
    public void setOrientation(double orientation) {
        super.setOrientation(orientation);
    }
    
    public int getCurrHP() {
        return super.getCurrHP();
    }
    public void setCurrHP(int currHP) {
        super.setCurrHP(currHP);
    }
    @Transient
    @Override
    public int getMaxHP() {
        if(creatureTemplate == null) {
            return super.getMaxHP();
        }
        
        return creatureTemplate.getMaxHP();
    }
    public int getCurrMana() {
        return super.getCurrMana();
    }
    public void setCurrMana(int currMana) {
        super.setCurrMana(currMana);
    }
    
    @Transient
    public int getMinDmg(){
        return creatureTemplate.getMinDmg();
    }
    
    @Transient
    public int getMaxDmg() {
        return creatureTemplate.getMaxDmg();
    }
    
    @Transient
    @Override
    public double getRange() {
        return creatureTemplate.getRange();
    }
    
    @Transient
    @Override
    public void setRange(double range) {
         creatureTemplate.setRange(range);
    }
    @Transient
    public int getEXP(){
    	return creatureTemplate.getExp();
    }
    @Transient
    public void setEXP(int eXP){
    	creatureTemplate.setExp(eXP);
    }
    
    // TODO: combine with Creature's attack in Unit.java
    public void attack() {
        int rollDamage = (int)(Math.random() * (getMaxDmg() - getMinDmg())) + getMinDmg();
        Collection<Unit> unitCollection = getController().getObjectsOfTypeAround(Unit.class, this, false, (float)getRange());
        retainAllInMeleeRange(unitCollection);
        // System.out.println(this + " attacking " + unitCollection.size() + " units");
        for (Unit each : unitCollection) {
            if(!each.isDead()) {
                each.takeDmg(rollDamage, this);
            }
        }
    }
    
    @Override
    // TODO: may be necessary to synchronize this
    public int takeDmg(int damage, GameObject dmgSrc) {
        int damageTaken = super.takeDmg(damage, dmgSrc);
        if(damageTaken != 0) {
            if(dmgSrcs.containsKey(dmgSrc)) {
                dmgSrcs.put(dmgSrc, dmgSrcs.get(dmgSrc) + damageTaken);
            }
            else {
                dmgSrcs.put(dmgSrc, damageTaken);
            }
        }
        
        return damageTaken;
    }
    
    @Override
    public void die(){
    	// System.out.println(this + " creature died");
    	cancelUpdateTask(); // Stop the world from using the WorldUpdateTask for this unit
    	
    	setUpdateTask(new WorldDespawnTask(getController()));
        getController().addFutureEvent(getUpdateTask());
        
        // TODO: Split loot amongst Players in dmgSrcs
        for(GameObject go : dmgSrcs.keySet()) {
            if(go instanceof Player) {
                Player player = (Player) go;
                player.addExp(getEXP());
                // Code for distributing loot
                System.out.println("Creature " + this + " killed by " + player);
                System.out.println("Player "+ player + " gained exp: " + getEXP());
            }
        }
    }
    
    public WorldRespawnTask createWorldRespawnTask() {
        return new WorldRespawnTask(getController());
    }
    
    class WorldDespawnTask extends WorldUpdateTask {
        public WorldDespawnTask(MapController mc) {
            setDelayFromNow(5000);
            
            mcRef = new WeakReference<MapController>(mc);
        }
        
        WeakReference<MapController> mcRef;
        
        @Override
        public Boolean get() throws InterruptedException, ExecutionException {
            MapController mc = mcRef.get();
            if(mc == null) {
                return false;
            }
            
            return mc.despawn(Creature.this);
        }
    }
    
    class WorldRespawnTask extends WorldUpdateTask {
        public WorldRespawnTask(MapController mc) {
            setDelayFromNow(2000);
            
            mcRef = new WeakReference<MapController>(mc);
        }
        
        WeakReference<MapController> mcRef;
        
        @Override
        public Boolean get() throws InterruptedException, ExecutionException {
            MapController mc = mcRef.get();
            if(mc == null) {
                return false;
            }
            
            return mc.respawn(Creature.this);
        }
    }
}