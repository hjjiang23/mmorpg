package mmo.world;

import java.awt.geom.Area;
import java.awt.geom.Rectangle2D;
import java.nio.ByteBuffer;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.DelayQueue;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import mmo.Controller;
import mmo.Intent;
import mmo.Util;
import mmo.server.AddRemoveGOIntent;
import mmo.server.NotifyIntent;
import mmo.server.Opcode.OP;
import mmo.server.PlayerSession;
import mmo.server.RequestWrapper;
import mmo.server.ScheduledTask;

@ManagedBean(name="WorldController", eager=true)
@ApplicationScoped
public class WorldController extends Controller<ScheduledTask> implements WorldInterface {
    public WorldController() {
        super();

        setName("WorldController");
        initQueue(new DelayQueue<ScheduledTask>());
        
        aiManager = new AIManager(); // Must happen before creatures loaded
        
        emf = Persistence.createEntityManagerFactory("MMO");
        em = emf.createEntityManager();
        
        System.out.println("WorldController initialized");
        
        start();
    }
    
    EntityManagerFactory emf;
    EntityManager em;
    
    private Map<ObjectGUID, GameObject> gameObjectsMap = new ConcurrentHashMap<ObjectGUID, GameObject>();
    private AIManager aiManager;
    
    private MapController mapController = null;
    private Position playerStartLocation = new Position(200, 200);
    
    public MapController getMapControllerForGO(GameObject go) {
        if(mapController == null) {
            if(go.countsAsActor()){
                mapController =  new MapController(this);
            }
            else {
                System.err.println("WorldController: MapController is null, but MapController cannot be instantiated for non-Actor");
            }
        }
        return mapController;
    }
    @Override
    public boolean cleanup(Object o) {
        if(o instanceof MapController) {
            // MapController mc = (MapController) o;
            // Pretend for now that something more meaningful is happening
            cleanupMapController((MapController) o);
            System.out.println("MapController can now be garbage collected");
        }
        return true;
    }
    public void cleanupMapController(MapController mc) {
        mc.invalidate();
        for(GameObject go : mc.getAllObjects()) {
            if(go instanceof Unit) {
                removeUnit((Unit)go);
            }
            else if (go instanceof WorldObject) {
                
            }
        }
    }
    
    public Collection<Creature> getCreaturesForArea(Area area) {
        // TODO: use spatial query
        TypedQuery<Creature> query = em.createQuery("SELECT cre FROM creature cre WHERE (x BETWEEN :minX AND :maxX) AND (y BETWEEN :minY AND :maxY)", Creature.class);
        Rectangle2D rect = area.getBounds2D();
        query.setParameter("minX", rect.getMinX()).setParameter("maxX", rect.getMaxX()).setParameter("minY", rect.getMinY()).setParameter("maxY", rect.getMaxY());
        return query.getResultList();
        /*Iterator<Creature> i = query.getResultList().iterator();
        while(i.hasNext()) {
            Creature cre = (Creature) i.next();
            addCreatureToWorld(cre);
        }*/
    }
    public Collection<Terrain> getTerrainsForArea(Area area) {
        String geomAsText = Terrain.areaToString(area);
        TypedQuery<Terrain> query = em.createNamedQuery("getTerrainsIntersectingArea", Terrain.class).setParameter("geomAsText", geomAsText);
        return query.getResultList();
        /*long start = System.nanoTime();
        List<Terrain> results = query.getResultList();
        System.out.println("Loaded " + results.size() + " terrains in " + (System.nanoTime() - start) + " " + System.nanoTime());
        return results;*/
    }

    
    /*private void loadAllCreaturesForMap(MapController mc) {        
        for(Object o : em.createQuery("SELECT cre FROM creature cre").getResultList().toArray()) {
            Creature cre = (Creature) o;
            addCreatureToWorld(cre);
        }
    }
    @SuppressWarnings("unchecked")
    private Collection<Terrain> getAllTerrainsForMap() {
        System.err.println("Warning: WorldController.getTerrains() is not meant to be used");
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("MMO");
        EntityManager em = emf.createEntityManager();
        
        return (List<Terrain>) em.createQuery("SELECT ter FROM terrain_view ter").getResultList();
    }*/
    
    public boolean addPlayerToWorld(PlayerSession playerSession) {
        assert(playerSession.getAccount() != null);
        
        int id = playerSession.getAccount().getId(); // TODO: replace with character id
        ObjectGUID guid = new ObjectGUID(ObjectGUID.GUIDTYPE.PLAYER, id);
        
        Player player = (Player) getGameObjectByGUID(guid);
        if(player != null) {
            // Player already logged in
            // Replace playerSession if session on this Player has expired
            PlayerSession oldPlayerSession = player.getPlayerSession();
            if(oldPlayerSession != null && oldPlayerSession.isSessionValid()) {
                System.err.println("Character already ingame");
                return false;
            }
            else {
                System.out.println("Player's old session had expired; now pointing to new session");
                player.setPlayerSession(playerSession); // This is the only reasonable place for this function to be called outside of Player
            }
        }
        else {
            // Player not already logged in
            System.out.println("Adding new player to world");
            //player = new Player(id, playerSession);
            player = instantiatePlayerForSession(playerSession);
            
            // Must init for all other players
            if(!addUnit(player))
                return false;
        }

        // Initialize all units for this player (includes self: this is called whenever a player logs on to this char, even if player was already in world)
        MapController mc = getMapControllerForGO(player);
        for(Unit unit : mc.getObjectsOfTypeAround(Unit.class, player, true)) {
            notifyUnitCreated(playerSession, unit);
        }
        for(Terrain ter : getTerrainsForArea(mc.getAreaOfVisionAroundUnit(player))) {
            sendInitTerrain(playerSession, ter);
        }
        
        playerSession.setOriginalMover(player); // Only place original mover should be set is on login
        playerSession.setMover(player);
        sendSetMover(playerSession, player);

        return true;
    }
    private Player instantiatePlayerForSession(PlayerSession playerSession) {
        if(playerSession.getAccount() == null) {
            System.err.println("Tried to instantiate player from session that is not logged in");
            return null;
        }
        
        int aid = playerSession.getAccount().getId();
        try {
            Player player = (Player) em.createQuery("SELECT c FROM player_character c WHERE c.accountId = :accountId")
                    .setParameter("accountId", aid)
                    .getResultList().get(0);
            player.setPlayerSession(playerSession);
            return player;
        }
        catch(NoResultException e) {
            return null;
        }
    }
    public boolean addCreatureToWorld(Creature cre) {
        MapController mc = getMapControllerForGO(cre);
        if(cre.getCreatureTemplate() == null) {
            System.err.println("Cannot instantiate a Creature with no template");
            return false;
        }
        
        String creAI = cre.getCreatureTemplate().getAIname();
        if(creAI != null) {
            cre.setAI(aiManager.createInstanceFor(creAI, cre, mc));
        }
        else {
            cre.setAI(aiManager.createDefaultInstanceFor(cre, mc));
        }
        return addUnit(cre);
    }
    @Override
    public boolean addUnit(Unit unit) {
        unit.obtainAddRemoveLock();
        try {
            // Return if already in world (assume already initialized)
            if(getGameObjectByGUID(unit.getGUID()) != null) {
                return false;
            }
            
            // For creatures / units created from database, position should be non-null already
            if(unit.pos == null) {
                unit.setPos(playerStartLocation);
            }
            
            if(!getMapControllerForGO(unit).addUnit(unit))
                return false;
            
            // Initialize this player for other players nearby
            notifyUnitCreatedForNearPlayers(unit, false);
            unit.initialize();
            return (gameObjectsMap.put(unit.getGUID(), unit) == null); // return true if unit was not previously in map
        } finally {
            unit.releaseAddRemoveLock();
        }
    }
    @Override
    public void moveUnit(Unit movingUnit, Position pos) {
        if(!isObjectInWorld(movingUnit)) {
            System.err.println("WorldController: move attempt for unit not in world " + movingUnit);
            return;
        }

        getMapControllerForGO(movingUnit).moveUnit(movingUnit, pos);
    }
    @Override
    public boolean canMoveTo(Unit unit, Position pos) {
        if(!isObjectInWorld(unit)) {
            System.err.println("WorldController: canMoveTo called for unit not in world " + unit);
            return false;
        }

        return getMapControllerForGO(unit).canMoveTo(unit, pos);
    }
    
    @Override
    public boolean removeUnit(Unit unit) {
        unit.obtainAddRemoveLock();
        try {
            unit.invalidate();
            if(gameObjectsMap.containsKey(unit.getGUID())) {
                if(!getMapControllerForGO(unit).removeUnit(unit)) {
                    System.err.println("WorldController contains unit, but MapController does not");
                }
                gameObjectsMap.remove(unit.getGUID());
                notifyUnitDestroyedForNearPlayers(unit);

                // Make players persist
                if(unit instanceof Player) {
                    em.merge(unit);
                }

                return true;
            }
            return false;
        } finally {
            unit.releaseAddRemoveLock();
        }
    }
    
    // Purely WorldController methods
    public boolean isObjectInWorld(GameObject go) {
        return gameObjectsMap.containsKey(go.getGUID());
    }
            
    public void handleMove(RequestWrapper requestWrapper) {
        ByteBuffer buf = requestWrapper.getByteBuffer();
        PlayerSession playerSession = requestWrapper.getPlayerSession();
        if(playerSession == null) {
            return;
        }
        
        if(playerSession.getMover() == null) {
            System.err.println("Move received when not yet logged into a character");
            return;
        }
        
        float newX = buf.getFloat();
        float newY = buf.getFloat(); 
        float orientation = buf.getFloat();
                
        Unit movingUnit = playerSession.getMover();

        // Position oldLoc = movingUnit.getPos();
        // float dx = newX - oldLoc.getX();
        // float dy = newY - oldLoc.getY();
        // System.out.println(playerSession + " " + dx + " " + dy);
        
        if(playerSession.getLastMovementPacketID() > requestWrapper.getPacketID()) {
            // Still need to validate
            System.err.println("Rejected packet " + requestWrapper.getPacketID() + ", currently at " + playerSession.getLastMovementPacketID());
            return;
        }
        
        movingUnit.setOrientation(orientation);
        moveUnit(movingUnit, new Position(newX, newY));        
    }
    
    public void handleMeleeAttack(RequestWrapper requestWrapper) {
        ByteBuffer buf = requestWrapper.getByteBuffer();
        PlayerSession playerSession = requestWrapper.getPlayerSession();
        if(playerSession == null) {
            return;
        }
        
        Unit unit = playerSession.getMover();
        if(unit == null) {
            System.err.println("Move received when not yet logged into a character");
            return;
        }
        
        unit.attack();
    }
    
    public void handleChat(RequestWrapper requestWrapper) {
        ByteBuffer buf = requestWrapper.getByteBuffer();
        PlayerSession playerSession = requestWrapper.getPlayerSession();
        if(playerSession == null || playerSession.getOriginalMover() == null) {
            System.err.println("PlayerSession " + playerSession + " null or mover not found");
            return;
        }
        
        String message = Util.getStringFromByteBuffer(buf);
        ByteBuffer sendBuf = ByteBuffer.allocate(5 + 2 * (message.length() + 1)); // 1 + 4 + 2 * (message length + 1)
        sendBuf.put((byte) OP.sCHAT.ordinal());
        sendBuf.putInt(playerSession.getOriginalMover().getGUID().getValue());
        Util.writeStringToBuffer(sendBuf, message);
        
        System.err.println("Sending " + message + " to " + getPlayersAround(playerSession.getMover(), false).size() + " players");
        for(Player player : getPlayersAround(playerSession.getMover(), false)) {
            PlayerSession otherPlayerSession = player.getPlayerSession();
            if(otherPlayerSession == null)
                continue;

            sendBufferedData(otherPlayerSession, sendBuf);
        }
    }
    
    // Helper method for repeatedly sending the same buffered data to all players
    // Write the buffered data, then reset the buffer so it can be used by the next playerSession
    public void sendBufferedData(PlayerSession playerSession, ByteBuffer sendBuf) {
        sendBuf.mark();
        playerSession.writeToOutput(sendBuf);
        sendBuf.reset();
    }
    
    public void notifyMove(Unit movingUnit, boolean includeSelf) {
        NotifyIntent intent = new NotifyIntent(new Object[] {(byte)OP.sMOVE.ordinal(), movingUnit.getGUID().getValue()}, // 1 + 4 + 4 + 4 + 4
                                               new Object[] {movingUnit.getPos().getX(), movingUnit.getPos().getY(), (float)movingUnit.getOrientation()},
                                               new boolean[] {true, true}, 17);

        for(Player player : getPlayersAround(movingUnit, includeSelf)) {
            PlayerSession playerSession = player.getPlayerSession();
            if(playerSession == null)
                continue;

            playerSession.addIntent(intent);
        }
    }
    
    public void notifyCurrHPChange(Unit unit) {
        NotifyIntent intent = new NotifyIntent(new Object[] {(byte)OP.sSETCURRHP.ordinal(), unit.getGUID().getValue()}, // 1 + 4 + 4
                new Object[] {unit.getCurrHP()},
                new boolean[] {true, true}, 9);

        for(Player player : getPlayersAround(unit, true)) {
            PlayerSession playerSession = player.getPlayerSession();
            if(playerSession == null)
                continue;
            
            playerSession.addIntent(intent);
        }
    }
    public void notifyMaxHPChange(Unit unit) {
        NotifyIntent intent = new NotifyIntent(new Object[] {(byte)OP.sSETMAXHP.ordinal(), unit.getGUID().getValue()}, // 1 + 4 + 4
                new Object[] {unit.getMaxHP()},
                new boolean[] {true, true}, 9);

        for(Player player : getPlayersAround(unit, true)) {
            PlayerSession playerSession = player.getPlayerSession();
            if(playerSession == null)
                continue;
            
            playerSession.addIntent(intent);
        }
    }
    
    /**
     * Notify all nearby Players of a creature that has just been added to the world
     * @param unit
     * @param includeSelf
     */
    public void notifyUnitCreatedForNearPlayers(Unit unit, boolean includeSelf) {
        AddRemoveGOIntent intent = createIntentForUnit(unit);

        for(Player player : getPlayersAround(unit, includeSelf)) {
            PlayerSession playerSession = player.getPlayerSession();
            if(playerSession == null)
                continue;
            
            playerSession.addIntent(intent);
        }
    }
    // TODO: called for creature AIs 
    public void notifyUnitCreated(PlayerSession playerSession, Unit unit) {
        playerSession.addIntent(createIntentForUnit(unit));
    }
    
    private AddRemoveGOIntent createIntentForUnit(Unit unit) {
        AddRemoveGOIntent intent;
        if(unit instanceof Creature) {
            Creature cre = (Creature) unit;
            intent = new AddRemoveGOIntent(new Object[] {unit.getGUID().getValue()}, // 1 + 4 + 4 + 4 + 4 + 4 + 2 * (name.length + 1)
                    new Object[] {(byte)OP.sINITUNIT.ordinal(), unit.getPos().getX(), unit.getPos().getY(), (float)unit.getOrientation(), unit.getName(), unit.getCurrHP(), cre.getCreatureTemplate().getId()},
                    new boolean[] {false, true}, 25 + 2 * (unit.getName().length() + 1));
        }
        else {
            intent = new AddRemoveGOIntent(new Object[] {unit.getGUID().getValue()}, // 1 + 4 + 4 + 4 + 4 + 4 + 2 * (name.length + 1)
                new Object[] {(byte)OP.sINITUNIT.ordinal(), unit.getPos().getX(), unit.getPos().getY(), (float)unit.getOrientation(), unit.getName(), unit.getCurrHP()},
                new boolean[] {false, true}, 21 + 2 * (unit.getName().length() + 1));
        }
        return intent;
    }
    
    public void sendInitTerrain(PlayerSession playerSession, Terrain ter) {
        Intent intent = new Intent(new Object[] {(byte)OP.sINITTERRAIN.ordinal(), ter.getId()}, // 1 + 4 + 2 strings' lengths + 1
                new Object[] {ter.getSVGPath(), ter.getTexture(), ter.getPassable()},
                new boolean[] {true, true}, 6 + 2 * (ter.getSVGPath().length() + ter.getTexture().length() + 2));
        
        playerSession.addIntent(intent);
    }
    public void sendDestroyTerrain(PlayerSession playerSession, Terrain ter) {
        Intent intent = new Intent(new Object[] {(byte)OP.sDESTROYTERRAIN.ordinal(), ter.getId()},
                new Object[] {},
                new boolean[] {true, true}, 5);
        
        playerSession.addIntent(intent);
    }
    
    public void handleReqCreTemplate(RequestWrapper requestWrapper) {
        ByteBuffer buf = requestWrapper.getByteBuffer();
        int tid = buf.getInt();
        sendInitCreTemplate(requestWrapper.getPlayerSession(), tid);
    }
    public void sendInitCreTemplate(PlayerSession playerSession, int tid) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("MMO");
        EntityManager em = emf.createEntityManager();

        CreatureTemplate creTemplate = (CreatureTemplate) em.createQuery("SELECT creTemplate FROM creature_template creTemplate WHERE creTemplate.id = :id")
                .setParameter("id", tid)
                .getSingleResult();
        
        if(creTemplate != null) {
            Intent intent = new Intent(new Object[] {(byte)OP.sINITCRETEMPLATE.ordinal(), tid}, // 1 + 4 + 4 + 4 + 2*(length of sprite)
                    new Object[] {creTemplate.getSprite(), creTemplate.getMaxHP(), creTemplate.getMaxMana()},
                    new boolean[] {true, true}, 13 + 2 * (creTemplate.getSprite().length() + 1));
            
            playerSession.addIntent(intent);
        }
    }
    public void notifyUnitDestroyedForNearPlayers(Unit unit) {
        AddRemoveGOIntent intent = new AddRemoveGOIntent(new Object[] {unit.getGUID().getValue()}, // 1 + 4
                new Object[] {(byte)OP.sDESTROYUNIT.ordinal()},
                new boolean[] {false, true}, 5);

        for(Player player : getPlayersAround(unit, false)) {
            PlayerSession playerSession = player.getPlayerSession();
            if(playerSession == null)
                continue;
            
            playerSession.addIntent(intent);
        }
    }
    // TODO: call for creature AIs
    public void notifyUnitDestroyed(PlayerSession playerSession, Unit unit) {
        AddRemoveGOIntent intent = new AddRemoveGOIntent(new Object[] {unit.getGUID().getValue()}, // 1 + 4
                new Object[] {(byte)OP.sDESTROYUNIT.ordinal()},
                new boolean[] {false, true}, 5);
        
        playerSession.addIntent(intent);
    }
    
    public void sendSetMover(PlayerSession playerSession, Unit unit) {
        ByteBuffer buf = ByteBuffer.allocate(5); // 1 + 4
        buf.put((byte) OP.sSETMOVER.ordinal());
        buf.putInt(unit.getGUID().getValue());
        playerSession.writeToOutput(buf);
    }
    
    // Use for updates and notifies
    public Collection<Player> getPlayersAround(GameObject object, boolean includeSelf) {
        return getObjectsOfTypeAround(Player.class, object, includeSelf);
    }
    public Collection<Player> getPlayersAround(GameObject object, boolean includeSelf, int range) {
        return getObjectsOfTypeAround(Player.class, object, includeSelf, range);
    }
    // Use for updates and notifies
    public Collection<Unit> getUnitsAround(GameObject object, boolean includeSelf) {
        return getObjectsOfTypeAround(Unit.class, object, includeSelf);
    }
    public Collection<Unit> getUnitsAround(GameObject object, boolean includeSelf, int range) {
        return getObjectsOfTypeAround(Unit.class, object, includeSelf, range);
    }
    
    private GameObject getGameObjectByGUID(ObjectGUID guid) {
        return gameObjectsMap.get(guid);
        
    }
    
    protected AIManager getAIManager() {
        return aiManager;
    }
    
    @Override
    public <T extends GameObject> Collection<T> getObjectsOfTypeAround(Class<T> cls, GameObject object, boolean includeSelf) {
        MapController mc = getMapControllerForGO(object);
        if(mc != null) {
            return getMapControllerForGO(object).getObjectsOfTypeAround(cls, object, includeSelf);
        }
        return new HashSet<T>();
    }
    @Override
    public <T extends GameObject> Collection<T> getObjectsOfTypeAround(Class<T> cls, GameObject object, boolean includeSelf, float range) {
        MapController mc = getMapControllerForGO(object);
        if(mc != null) {
            return getMapControllerForGO(object).getObjectsOfTypeAround(cls, object, includeSelf, range);
        }
        return new HashSet<T>();
    }
        
    public void finalize() {
        em.close();
        emf.close();
    }
    @Override
    public Collection<GameObject> getAllObjects() {
        return gameObjectsMap.values();
    }
    @Override
    public boolean isValid() {
        return true;
    }
}
