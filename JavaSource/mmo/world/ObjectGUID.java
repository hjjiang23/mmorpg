package mmo.world;

public class ObjectGUID {
    public ObjectGUID(GUIDTYPE type, int entryNum) {
        // entryNum must not exceed 0x0FFFFFFF
        if(entryNum >= 0x10000000) {
            System.err.println("Too many entries of type " + type);
        }
        
        this.value = (type.ordinal() << 28) | entryNum;
    }
    
    private int value;
    
    public int getValue() {
        return value;
    }
    
    public int getHeader() {
        return (value & 0xF0000000) >> 28;
    }
    public int getId() {
        return value & 0x0FFFFFFF;
    }
    
    @Override
    public int hashCode() {
        return value;
    }
    
    @Override
    public boolean equals(Object o) {
        if(o == null) {
            return false;
        }
        else if(o instanceof ObjectGUID) {
            return getValue() == ((ObjectGUID)o).getValue();
        }
        return false;
    }
    
    @Override
    public String toString() {
        return ((Integer)getValue()).toString();
    }

    public static enum GUIDTYPE {
        GAMEOBJECT,
        WORLDOBJECT,
        UNIT,
        PLAYER,
        CREATURE,
    }
}
