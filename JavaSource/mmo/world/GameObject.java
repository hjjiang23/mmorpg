package mmo.world;

import java.awt.geom.Area;
import java.awt.geom.Rectangle2D;
import java.lang.ref.WeakReference;
import java.util.concurrent.locks.ReentrantLock;

import javax.persistence.Transient;

import mmo.world.ObjectGUID.GUIDTYPE;

public abstract class GameObject implements Spatial {
    public GameObject() {
        this(0);
    }
    public GameObject(int id) {
        setGUID(id);
    }
    
    protected ReentrantLock addRemoveLock = new ReentrantLock(); // Only one thread may touch an object when it is being added to or removed from world
    protected boolean isValid = false; // true if object is in world and is not being removed
    protected Position pos;
    protected ObjectGUID guid;
    WeakReference<MapController> controllerRef;
    double orientation; // 0 = facing north/top
    
    @Transient Area area = null;
        
    public void obtainAddRemoveLock() {
        // System.out.println("Obtained lock for " + this + "; Remaining: " + (addRemoveLock.getHoldCount() + 1));
        addRemoveLock.lock();
    }
    public void releaseAddRemoveLock() {
        // System.out.println("Released lock for " + this + "; Remaining: " + (addRemoveLock.getHoldCount() - 1));
        addRemoveLock.unlock();
    }
    public boolean isValid() {
        return isValid;
    }
    public void initialize() {
        if(isValid()) {
            System.err.println("Initialize called for already initialized GO");
            return;
        }
        this.isValid = true;
    }
    public void invalidate() {
        this.isValid = false;
    }
    
    protected GUIDTYPE getType() {
        return GUIDTYPE.GAMEOBJECT;
    }

    public ObjectGUID getGUID() {
        return guid;
    }
    public void setGUID(int id) {
        guid = new ObjectGUID(getType(), id);
    }
    
    public int getId() {
        return getGUID().getId();
    }
    public void setId(int i) {
        setGUID(i);
    }
    
    public double getOrientation() {
        return orientation;
    }
    public void setOrientation(double orientation) {
        this.orientation = orientation;
    }
    
    public MapController getController() {
        return controllerRef.get();
    }
    public void setController(MapController controller) {
        this.controllerRef = new WeakReference<MapController>(controller);
    }

    public Position getPos() {
        return pos;
    }
    public void setPos(Position pos) {
        // System.out.println("Set pos called");
        this.pos = pos;
        this.area = new Area(new Rectangle2D.Float(pos.getX() - 10, pos.getY() - 10, 20, 20));
    }
    
    @Override
    public String toString() {
        return super.toString() + " " + (guid != null ? guid.toString() : null);
    }

    @Override
    public int hashCode() {
        return guid != null ? guid.hashCode() : 0;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        else if (obj == null)
            return false;
        else if (getClass() != obj.getClass())
            return false;
        GameObject other = (GameObject) obj;
        if (guid.equals(other.getGUID()))
            return true;
        return false;
    }
    
    @Override
    public Area getArea() {
        return area;
    }
    
    public boolean countsAsActor() {
        return this instanceof Actor;
    }
}
