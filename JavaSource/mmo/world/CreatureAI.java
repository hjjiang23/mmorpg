package mmo.world;

import java.lang.ref.WeakReference;
import java.util.Collection;

public abstract class CreatureAI
{
	public CreatureAI(Creature cre, WorldInterface area)
	{
		creRef = new WeakReference<Creature>(cre);
		areaRef = new WeakReference<WorldInterface>(area);
	}

	protected WeakReference<Creature> creRef; // weak reference because AI should not prevent Creature from being deleted
	protected WeakReference<WorldInterface> areaRef; // weak reference because AI should not prevent Creature from being deleted

	/**
	 * Updates the AI. Return true if the AI is to be scheduled to be called again.
	 */
	public abstract boolean update();

	protected boolean escapeWalk()
	{
		Creature cre = creRef.get();
		WorldInterface area = areaRef.get();
		if (cre == null || area == null || cre.isDead() || !area.isValid())
		{
			return false;
		}

		// Find closest target
		Collection<Unit> potentialTargets = area.getObjectsOfTypeAround(Unit.class, creRef.get(), false, cre.getVisionRange());

		double distClosest = Integer.MAX_VALUE;
		Unit closestTargetRef = null;

		for (Unit potentialTarget : potentialTargets)
		{
			if (!potentialTarget.isDead())
			{
				double dist = cre.getPos().toPoint().distance(potentialTarget.getPos().toPoint());
				if (distClosest > dist)
				{
					distClosest = dist;
					closestTargetRef = potentialTarget;
				}
			}
		}

		// Do not use else here
		if (closestTargetRef != null)
		{
			Position targetPos = closestTargetRef.getPos();
			cre.facePosition(targetPos);
			cre.setOrientation(cre.getOrientation() + Math.PI);//Turn around

			float speed = cre.getCreatureTemplate().getMvSpeed();
			float dx = (float) (speed * Math.sin(cre.getOrientation()));
			float dy = (float) (speed * Math.cos(cre.getOrientation()));

			area.moveUnit(cre, new Position(cre.getPos().getX() + dx, cre.getPos().getY() + dy));
		}

		return true;
	}
	protected boolean randomWalk()
	{
		// System.out.println(creRef.get() + " should be walking");
		WorldInterface area = areaRef.get();
		Creature cre = creRef.get();
		if (area != null && cre != null && !cre.isDead() && area.isValid())
		{
		    float speed = cre.getCreatureTemplate().getMvSpeed();
		    
			Position pos = cre.getPos();
			Position newPos;
			for(int i = 0; i < 3; ++i) {
			    
			    newPos = new Position(pos.getX() + (float) ((Math.random() - 0.5) * speed), pos.getY() + (float) ((Math.random() - 0.5) * speed));
			    if(area.canMoveTo(cre, newPos)) {
			         area.moveUnit(cre, newPos);
			         return true;
			    }
			}
			
			return false;
		}
		else
		{
			return false;
		}
	}
}
