package mmo.world;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.math.*;

import javax.persistence.Transient;
// import java.util.concurrent.ThreadLocalRandom; // Requires Java 1.7

import mmo.server.PlayerSession;
import mmo.world.ObjectGUID.GUIDTYPE;

public class Player extends Unit implements Actor {
    public Player() {
        super();
    }
    
    @Deprecated
    public Player(int id, PlayerSession playerSession) {
        super(id);
        
        setPlayerSession(playerSession);
    }
    
    private WeakReference<PlayerSession> playerSessionRef; // Weak reference, because when session dies, bean needs to be destroyed. Bean's finalize destroys this object
    boolean hasSession = false;
    private int accountId = 0;
    private long exp = 0;
    private int level = 0;
    private int currHP = 0;
    private int currMana = 0;
    private int maxHP = 0;
    private int maxMana = 0;
    private int STR = 0;
    private int DEX = 0;
    private int CON = 0;
    private int WIS = 0;
    private int INT = 0;
    private int CHA = 0;
    List<Item> items;

    @Override
    @Transient
    protected GUIDTYPE getType() {
        return GUIDTYPE.PLAYER;
    }
    
    @Override
    public int getId() {
        return super.getId();
    }
    @Override
    public void setId(int i) {
        super.setId(i);
    }
    
    public int getAccountId() {
        return accountId;
    }
    public void setAccountId(int i) {
        this.accountId = i;
    }
    
    @Override
    public String getName() {
        return super.getName();
    }
    @Override
    public void setName(String name) {
        super.setName(name);
    }
    
    @Transient
    public PlayerSession getPlayerSession() {
        if(playerSessionRef != null) {
            return playerSessionRef.get();
        }
        else {
            return null;
        }
    }
    @Transient
    public WeakReference<PlayerSession> getPlayerSessionRef() {
        return playerSessionRef;
    }
    @Transient
    public void setPlayerSession(PlayerSession playerSession) {
        if(playerSession == null) {
            this.playerSessionRef = null;
        }
        this.playerSessionRef = new WeakReference<PlayerSession>(playerSession);
        hasSession = false;
    }

    // TODO: combine with Creature's attack in Unit.java
    public void attack() {
    	int rollDamage = (int)(Math.random() * (getMaxDmg() - getMinDmg())) + getMinDmg();
    	Collection<Unit> unitCollection = getController().getObjectsOfTypeAround(Unit.class, this, false, (float)getRange());
    	for (Unit each : unitCollection) {
    	    if(!each.isDead()) {
    	        each.takeDmg(rollDamage, this);
    	    }
    	}
    }   

    @Override
    public void die() {
    	System.out.println(this + " player died");
    }

    @Transient
	public int getMinDmg() {
		return getDEX();
	}
    @Transient
	public int getMaxDmg() {
		return getSTR();
	}
    @Transient
	public double getRange() {
		// return range;
		return 30.;
	}
    @Transient
	public void setRange(double range){
		// this.range = range;
	}
    @Override
    public int getCurrHP() {
        return currHP;
    }
    @Override
    public void setCurrHP(int currHP) {
        this.currHP = currHP;
    }
    
    @Transient
    public void setCurrHPAndNotify(int currHP) {
        super.setCurrHPAndNotify(currHP);
    }
    @Transient
    public void setMaxHPAndNotify(int maxHP) {
        super.setCurrHPAndNotify(maxHP);
    }
    @Override
    public int getCurrMana() {
        return currMana;
    }
    @Override
    public void setCurrMana(int currMana) {
        this.currMana = currMana;
    }
    @Override
    public int getMaxHP() {
        return maxHP;
    }
    @Override
    public void setMaxHP(int maxhp){
    	this.maxHP = maxhp;
    }
    @Override
    public int getMaxMana() {
		return maxMana;
	}
    @Override
	public void setMaxMana(int maxMana) {
		this.maxMana = maxMana;
	}
    
	public long getExp() {
		return exp;
	}

	public void setExp(long exp) {
		this.exp = exp;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}
	
	public int getSTR() {
		return STR;
	}

	public void setSTR(int sTR) {
		STR = sTR;
	}

	public int getDEX() {
		return DEX;
	}

	public void setDEX(int dEX) {
		DEX = dEX;
	}

	public int getCON() {
		return CON;
	}

	public void setCON(int cON) {
		CON = cON;
	}

	public int getWIS() {
		return WIS;
	}

	public void setWIS(int wIS) {
		WIS = wIS;
	}

	public int getINT() {
		return INT;
	}

	public void setINT(int iNT) {
		INT = iNT;
	}

	public int getCHA() {
		return CHA;
	}

	public void setCHA(int cHA) {
		CHA = cHA;
	}

	public void addExp(int exp){
		setExp(getExp()+exp);
		if (getExp() >= (10+2^(getLevel()-1))){
			setExp(getExp()-10+2^(getLevel()-1));
			Levelup();
		}
	}
	
	public void Levelup(){
		setLevel(getLevel()+1);
		setMaxHPAndNotify(getMaxHP()+10);
		setCurrHPAndNotify(getMaxHP());
		setCHA((int)(Math.ceil(getCHA()*1.1)));
		setCON((int)(Math.ceil(getCON()*1.1)));
		setSTR((int)(Math.ceil(getSTR()*1.1)));
		setDEX((int)(Math.ceil(getDEX()*1.1)));
		setWIS((int)(Math.ceil(getWIS()*1.1)));
		setINT((int)(Math.ceil(getINT()*1.1)));
		System.out.println("Player: " + this + " Leveled Up: " + getLevel());
		System.out.println("Stats:" + "\n" +
				"Cha: " + getCHA() + "\n" +
				"Con: " + getCON() + "\n" +
				"Str: " + getSTR() + "\n" +
				"Dex: " + getDEX() + "\n" +
				"Wis: " + getWIS() + "\n" +
				"Int: " + getINT() + "\n");
	}
	
	public List<Item> getItems() {
        return items;
    }
    public void setItems(List<Item> items) {
        // this.items = items; // Hibernate hands Player a PersistentBag. Documentation does not state whether it is safe to hold on to this. Shallow clone to be safe?
        
        this.items = new ArrayList<Item>();
        this.items.addAll(items);
    }
	
    // TODO: anti-cheating checks here
    @Override
    public boolean update() {
        return false;
    }
}
