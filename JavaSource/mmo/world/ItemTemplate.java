package mmo.world;

public class ItemTemplate implements Template<Item> {
    public ItemTemplate() {}
    
    private int id;
    private String name;
    private String sprite;
    private int type;
    private int subtype;
    
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getSprite() {
        return sprite;
    }
    public void setSprite(String sprite) {
        this.sprite = sprite;
    }

    public int getType() {
        return type;
    }
    public void setType(int type) {
        this.type = type;
    }

    public int getSubtype() {
        return subtype;
    }
    public void setSubtype(int subtype) {
        this.subtype = subtype;
    }
    
    @Override
    public Item createFromTemplate() {
        // TODO Auto-generated method stub
        return null;
    }
}
