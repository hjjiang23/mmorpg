package mmo.world;

import java.util.concurrent.ExecutionException;

import mmo.server.ScheduledTask;
import mmo.server.WorldContainer;

public class WorldCleanupTask extends ScheduledTask {

    public WorldCleanupTask(WorldInterface wi, WorldContainer toBeCleanedUp, long lastAccess) {
        super.delayUntil = System.currentTimeMillis() + defaultDelay;
        
        this.wi = wi;
        this.toBeCleanedUp = toBeCleanedUp;
        this.lastAccess = lastAccess;
    }
    
    private WorldInterface wi;
    private WorldContainer toBeCleanedUp;
    private long defaultDelay = 5000;
    private long lastAccess = 0;

    @Override
    public Boolean get() throws InterruptedException, ExecutionException {
        if(wi.isValid() && toBeCleanedUp.getLastAccessTime() <= this.lastAccess) {
            return wi.cleanup(toBeCleanedUp);
        }
        return false;
    }
}
