package mmo.world;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

public class AIManager extends ClassLoader {
    public AIManager() {
        // Super using this class' ClassLoader
        super(AIManager.class.getClassLoader());
        
        loadedAIs = new HashMap<String, Class<CreatureAI>>();
        
        System.out.println("AIManager initialized");
        
        Class<CreatureAI> cls = null;
        try {
            cls = loadClass("RandomWalkAI");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            defaultAI = cls;
        }
    }
    
    private final String pkgConst = "mmo.world.ai.";
    private final Map<String, Class<CreatureAI>> loadedAIs;
    private final Class<CreatureAI> defaultAI;
    
    @SuppressWarnings("unchecked")
    public synchronized Class<CreatureAI> loadClass(String className) throws ClassNotFoundException {
        Class<CreatureAI> cls = loadedAIs.get(className);
        if(cls == null) {
            // Warning suppressed here
            cls = (Class<CreatureAI>) getParent().loadClass(pkgConst + className);
            loadedAIs.put(className, cls);
        }
        return cls;
    }
    
    public Class<CreatureAI> getDefaultAI() {
        return defaultAI;
    }
    
    public CreatureAI createDefaultInstanceFor(Creature cre, WorldInterface area) {
        return createInstanceFor(getDefaultAI(), cre, area);
    }
    
    public CreatureAI createInstanceFor(String aiName, Creature cre, WorldInterface area) {
        Class<CreatureAI> aiClass = null;
        try {
            aiClass = loadClass(aiName);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return createInstanceFor(aiClass, cre, area);
    }
    
    public CreatureAI createInstanceFor(Class<CreatureAI> aiClass, Creature cre, WorldInterface area) {
        // Lot of exceptions!
        try {
            return aiClass.getConstructor(Creature.class, WorldInterface.class).newInstance(cre, area);
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        
        return null;
    }
}
