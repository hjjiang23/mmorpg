package mmo.world;

public interface Updatable {
    /**
     * @return true if this object must continue to be updated
     */
    public boolean update();
    
    public void setUpdateTask(WorldUpdateTask task);
    public WorldUpdateTask getUpdateTask();
    public void cancelUpdateTask();
}
