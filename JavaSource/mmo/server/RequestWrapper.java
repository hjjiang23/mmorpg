package mmo.server;

import java.lang.ref.WeakReference;
import java.nio.ByteBuffer;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.servlet.AsyncContext;

@RequestScoped
@ManagedBean(name="requestWrapper")
public class RequestWrapper {
    public RequestWrapper() {
        // System.out.println("RequestWrapper initiated");
    }
    
    private ByteBuffer buf = null;
    private WeakReference<PlayerSession> playerSessionRef = null;
    private AsyncContext aCtx = null;
    private boolean forceDefaultResponse = false; // set to true if packet forces a response
    private Opcode.OP op = null;
    private long packetID;
    
    public void setByteBuffer(ByteBuffer buf) {
        this.buf = buf;
    }
    public ByteBuffer getByteBuffer() {
        return buf;
    }
    
    public void setPlayerSession(PlayerSession playerSession) {
        this.playerSessionRef = new WeakReference<PlayerSession>(playerSession);
    }
    public PlayerSession getPlayerSession() {
        PlayerSession playerSession = playerSessionRef.get(); 
        if(playerSession == null) {
            return null;
        }
        return playerSession.isSessionValid() ? playerSession : null;
    }
    
    public AsyncContext getAsyncContext() {
        return aCtx;
    }
    public void setAsyncContext(AsyncContext aCtx) {
        this.aCtx = aCtx;
    }

    public boolean usesDefaultResponse() {
        return this.forceDefaultResponse;
    }
    public void setForceDefaultResponse(boolean b) {
        // block LifeCycle if necessary
        this.forceDefaultResponse = b;
    }

    public Opcode.OP getOP() {
        return op;
    }
    public void setOP(Opcode.OP op) {
        this.op = op;
    }
    
    @Override
    public void finalize() {
        try {
            super.finalize();
        } catch (Throwable e) {
            e.printStackTrace();
        }
        //System.out.println("RequestWrapper destroyed");
    }

    public void setPacketID(long id) {
        this.packetID = id;
        //System.out.println("Received " + id);
    }
    public long getPacketID() {
        return packetID;
    }
}
