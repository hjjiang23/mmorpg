package mmo.server;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class OpcodeHandler {
    public OpcodeHandler(Object handler, Method method) {
        setHandlerInstance(handler);
        setMethod(method);
    }
    
    Method method = null;
    Object handler = null;
    
    public void setHandlerInstance(Object handler) {
        this.handler = handler;
    }
    public Object getHandlerInstance() {
        return handler;
    }
    
    public void setMethod(Method method) {
        this.method = method;
    }
    public Method getMethod() {
        return method;
    }
    
    public void invoke(RequestWrapper requestWrapper) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
        method.invoke(handler, requestWrapper);
    }
}
