package mmo.server;

import java.util.concurrent.Delayed;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public abstract class ScheduledTask implements ScheduledFuture<Boolean> {
    @Override
    public abstract Boolean get() throws InterruptedException, ExecutionException;

    @Override
    public Boolean get(long arg0, TimeUnit arg1) throws InterruptedException, ExecutionException, TimeoutException {
        throw new UnsupportedOperationException();
    }
    
    protected boolean isDone = false;
    protected boolean isCancelled = false;
    
    // System millisecond resolution time that the task should be run at
    protected long delayUntil = 0;

    @Override
    public long getDelay(TimeUnit timeUnit) {
        return timeUnit.convert(delayUntil - System.currentTimeMillis(), TimeUnit.MILLISECONDS);
    }

    @Override
    public int compareTo(Delayed other) {
        if(other instanceof ScheduledTask) {
            return ((Long)delayUntil).compareTo(((ScheduledTask)other).getDelayUntil());
        }
        // Give a close estimate
        else {
            return ((Long)getDelay(TimeUnit.MILLISECONDS)).compareTo(other.getDelay(TimeUnit.MILLISECONDS));
        }
    }
    
    @Deprecated
    public void setDelayUntil(long delayUntil) {
        this.delayUntil = delayUntil;
    }
    public void setDelayFromNow(long dTime) {
        this.delayUntil = System.currentTimeMillis() + dTime;
    }
    public long getDelayUntil() {
        return delayUntil;
    }

    @Override
    public boolean cancel(boolean mayInterruptIfRunning) {
        if(isCancelled || isDone) {
            return false;
        }
        
        return true;
    }

    @Override
    public boolean isCancelled() {
        return isCancelled;
    }

    @Override
    public boolean isDone() {
        return false;
    }
}
