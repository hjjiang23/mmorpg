package mmo.server;

import java.lang.ref.WeakReference;
import java.util.concurrent.ExecutionException;
import mmo.world.Player;
import mmo.world.WorldController;

public class PlayerLogoutTask extends ScheduledTask {
    public PlayerLogoutTask(WorldController wc, WeakReference<PlayerSession> playerSessionRef, Player player) {
        super.delayUntil = System.currentTimeMillis() + defaultDelay;
        
        this.wc = wc;
        this.playerSessionRef = playerSessionRef;
        this.player = player;
    }
    
    private WorldController wc;
    private WeakReference<PlayerSession> playerSessionRef;
    private Player player;
    private final static int defaultDelay = 4000;

    @Override
    public Boolean get() throws InterruptedException, ExecutionException {
        // Remove from world after time is up and the player does not currently have a valid session
        // Time is counted from the last valid session destroyed + defaultDelay
        if(player.getPlayerSessionRef() == null || player.getPlayerSessionRef() == this.playerSessionRef) {
            wc.removeUnit(player);
            System.out.println("PlayerLogoutTask removed " + player + " from world");
            return true;
        }
        else {
            System.out.println("PlayerLogoutTask kept " + player + " in world");
        }
        return false;
    }
}
