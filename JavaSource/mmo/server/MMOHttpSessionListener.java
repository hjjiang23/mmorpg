package mmo.server;

import javax.servlet.ServletContext;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import mmo.world.Player;
import mmo.world.WorldController;

@WebListener
public class MMOHttpSessionListener implements HttpSessionListener {
    public MMOHttpSessionListener() {
        super();
        System.out.println("MMOHttpSessionListener initiated");
    }

    @Override
    public void sessionCreated(HttpSessionEvent sessEvent) {
        HttpSession sess = sessEvent.getSession();
        PlayerSession playerSession = new PlayerSession();
        playerSession.setSession(sess);
        sess.setAttribute("playerSession", playerSession);
        
        // asw12: Iteration 1 only
        // Since there is no UI for logging in, just send this login:password combination
        try {
            Integer value = (Integer.valueOf((String)sess.getServletContext().getAttribute("login")) % 8) + 1;
            sess.getServletContext().setAttribute("login", String.valueOf(value));
            value = (Integer.valueOf((String)sess.getServletContext().getAttribute("password")) % 8) + 1;
            sess.getServletContext().setAttribute("password", String.valueOf(value));
        }
        catch(Exception e) {
            sess.getServletContext().setAttribute("login", "1");
            sess.getServletContext().setAttribute("password", "1");
        }
        
        System.out.println("Session initialized " + sess.getId());
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent sessEvent) {        
        HttpSession sess = sessEvent.getSession();
        PlayerSession playerSession = (PlayerSession) sess.getAttribute("playerSession");
        
        // Additional cleanup needed if Player was in world
        if(playerSession.isInWorld()) {
            playerSession.invalidate();
            Player player = playerSession.getOriginalMover();
            
            ServletContext servCtx = sess.getServletContext();
            ConnectionsController cc = (ConnectionsController) servCtx.getAttribute("ConnectionsController");
            WorldController wc = (WorldController) servCtx.getAttribute("WorldController");
            
            cc.addFutureEvent(new PlayerLogoutTask(wc, player.getPlayerSessionRef(), player));
        }
        
        System.out.println("Session destroyed " + sess.getId());
    }
}
