package mmo.server;

import java.io.IOException;
import java.lang.ref.WeakReference;

import javax.servlet.AsyncEvent;
import javax.servlet.AsyncListener;


public class CometExpirationListener implements AsyncListener {

    public CometExpirationListener(ConnectionsController cc, PlayerSession playerSession) {
        this.cc = cc;
        this.playerSessionRef = new WeakReference<PlayerSession>(playerSession);
    }
    
    private final ConnectionsController cc;
    private final WeakReference<PlayerSession> playerSessionRef;
    
    @Override
    public void onComplete(AsyncEvent aevent) throws IOException {
        //System.out.println("c " + aevent.getAsyncContext() + " " + aevent);
    }

    @Override
    public void onError(AsyncEvent aevent) throws IOException {
        System.out.println("e " + aevent.getAsyncContext() + " " + aevent);
    }

    @Override
    public void onStartAsync(AsyncEvent aevent) throws IOException {}

    @Override
    public void onTimeout(AsyncEvent aevent) throws IOException {
        //System.out.println("t " + aevent.getAsyncContext() + " " + aevent);
        PlayerSession playerSession = playerSessionRef.get();
        if(playerSession != null) {
            cc.sendRequestPackets(aevent.getAsyncContext(), playerSession);
        }
    }
}
