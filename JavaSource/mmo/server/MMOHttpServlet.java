package mmo.server;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.ByteBuffer;
import javax.servlet.AsyncContext;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mmo.server.Opcode.OP;

@WebServlet(urlPatterns={"/pages/ajax.jsp"}, asyncSupported=true)
public class MMOHttpServlet extends HttpServlet {
    private static final long serialVersionUID = 7678993204086924519L;
    
    public MMOHttpServlet() {
        System.out.println("MMOHttpServlet initiated");
    }
    
    // TODO: Put these in a properties file
    final long ASYNC_TIMEOUT = 40000; // Real timeout seems to have a minimum of 10s. Firefox default timeout is ~60s.
    private OpcodeHandler[] opcodeHandlers;
       
    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        
        // asw12: Iteration 1 only
        // Since there is no UI for logging in, just send this login:password combination
        getServletContext().setAttribute("login", "1");
        getServletContext().setAttribute("password", "1");
        
        initOpcodeHandlerMap();
    }
    
    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse res) {
        HttpSession sess = req.getSession(false); // HttpSession should be already created at this point from user visiting Launch page
        if(sess == null) {
            return;
        }
        
        AsyncContext aCtx = req.startAsync(req, res);

        PlayerSession playerSession = (PlayerSession) sess.getAttribute("playerSession");
        // this must occur in the same Thread
        if(playerSession.isSessionNew()) {
            // TODO: Stuff to do when new session is created
            ConnectionsController connCtrl = (ConnectionsController) getHandlerInstance("ConnectionsController");
            playerSession.setCometExpirationListener(new CometExpirationListener(connCtrl, playerSession));
            playerSession.sessionInitialized();
        }
        
        ByteBuffer buf = ByteBuffer.allocate(1 << 10);
        buf.mark();
        
        RequestWrapper requestWrapper = new RequestWrapper();
        req.setAttribute("requestWrapper", requestWrapper);
        requestWrapper.setByteBuffer(buf);
        requestWrapper.setPlayerSession(playerSession);
        requestWrapper.setAsyncContext(aCtx);
        
        try {
            ServletInputStream inputStream = req.getInputStream();
            inputStream.read(buf.array(), 0, 1 << 10);
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        OP op = OP.values()[buf.get()];
        requestWrapper.setOP(op);
        
        requestWrapper.setPacketID(buf.getLong());
        
        addToPacketQueue(requestWrapper);
        processPacket(requestWrapper);
    }
    
    private void initOpcodeHandlerMap() {
        opcodeHandlers = new OpcodeHandler[Opcode.getNumOpcodes()];
        @SuppressWarnings("rawtypes") // unavoidable
        Class[] formalParameters = new Class[] {RequestWrapper.class};

        String handlerClassName = "";
        String handlerMethodName = "";
        try {
            for(Opcode.OP op : Opcode.OP.values()) {
                int i = op.ordinal();
                handlerClassName = Opcode.OPHandlerNames[i][0];
                handlerMethodName = Opcode.OPHandlerNames[i][1];
                Object handlerInstance =  getHandlerInstance(handlerClassName);
                Method handlerMethod = handlerInstance.getClass().getMethod(handlerMethodName, formalParameters);
                
                opcodeHandlers[i] = new OpcodeHandler(handlerInstance, handlerMethod);
            }
        }
        catch(NoSuchMethodException e) {
            System.err.println(handlerClassName +  " :: " + handlerMethodName);
            e.printStackTrace();
        }
    }
    
    // TODO: fix this method so that it does not require handlers to have eager=true
    private Object getHandlerInstance(String name) {
        Object handler = null;
        try {
             handler = getServletContext().getAttribute(name);
        }
        catch(Exception e) {}
        finally {
            if(handler == null) {
                try {
                    Class<?> classDefinition = Class.forName(name, true, getServletContext().getClassLoader());
                    handler = classDefinition.newInstance();
                } catch (InstantiationException e) {
                    System.out.println(e);
                } catch (IllegalAccessException e) {
                    System.out.println(e);
                } catch (ClassNotFoundException e) {
                    System.out.println(e);
                }
            }
            getServletContext().setAttribute(name, handler);
        }
        return handler; 
    }
    
    public void addToPacketQueue(RequestWrapper requestWrapper) {
        final PlayerSession playerSession = requestWrapper.getPlayerSession();
        if(playerSession != null) {
            if(Opcode.allowCometResponse(requestWrapper.getOP())) {
                AsyncContext aCtx = requestWrapper.getAsyncContext();
                aCtx.addListener(playerSession.getCometExpirationListener());
                aCtx.setTimeout(ASYNC_TIMEOUT);
                playerSession.addToCometQueue(requestWrapper);
            }
        }
    }
    
    private void processPacket(RequestWrapper requestWrapper) {
        ByteBuffer buf = requestWrapper.getByteBuffer();
        Opcode.OP opcode = requestWrapper.getOP();
        try {            
            if(opcodeHandlers[opcode.ordinal()] != null) {
                opcodeHandlers[opcode.ordinal()].invoke(requestWrapper);
            }
            else {
                throw new UnsupportedOperationException(opcode + " " + new String(buf.array()));
            }
        }
        catch (IllegalArgumentException e) {
            System.err.println("Exception when processing " + opcode + " " + new String(buf.array()));
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            System.err.println("Exception when processing " + opcode + " " + new String(buf.array()));
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            System.err.println("Exception when processing " + opcode + " " + new String(buf.array()));
            e.printStackTrace();
        }
    }
}
