package mmo.server;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.concurrent.DelayQueue;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.servlet.AsyncContext;
import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

import mmo.Controller;
import mmo.Util;
import mmo.server.Account;
import mmo.server.Opcode.OP;
import mmo.server.PlayerSession;
import mmo.server.RequestWrapper;
import mmo.world.Player;
import mmo.world.WorldController;

// These faces annotations are only here for convenience. Ideally, this should belong in a separate JVM
@ManagedBean(name="ConnectionsController", eager=true)
@ApplicationScoped
public class ConnectionsController extends Controller<ScheduledTask> {    
    public ConnectionsController() {
        super();
        
        setName("ConnectionsController");
        initQueue(new DelayQueue<ScheduledTask>());
        
        emf = Persistence.createEntityManagerFactory("MMO");
        em = emf.createEntityManager();
        
        System.out.println("ConnectionsController initialized");

        start();
    }
    
    EntityManagerFactory emf;
    EntityManager em;
    
    // Unused (until web sockets become more standard)
    public void handleOpenStream(RequestWrapper requestWrapper) {
        AsyncContext aCtx = requestWrapper.getAsyncContext();
        
        System.out.println("Stream request received");
        try {
            aCtx.getRequest().getInputStream().close();
            OutputStream output = aCtx.getResponse().getOutputStream();
            ByteBuffer bb = ByteBuffer.wrap("Hello World".getBytes());
            output.write(bb.array()); // */
            aCtx.complete();    
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void handleGetPublicKey(RequestWrapper requestWrapper) {
    }

    public void sendRequestPackets(AsyncContext aCtx, PlayerSession playerSession) {
        playerSession.removeFromCometQueue(aCtx); // If this is called because a request is about to time out, remove aCtx specifically
        
        HttpServletResponse res = null;
        try{
            res = (HttpServletResponse) aCtx.getResponse();
            //System.out.println("\t " + aCtx + " " + aCtx.getResponse() + " " + aCtx.getRequest().getClass());
        }
        catch(NullPointerException e) {
            e.printStackTrace();
            //System.err.println("\t NPE " + aCtx + " " + aCtx.getResponse() + " " + aCtx.getRequest().getClass());
        }

        try {
            ServletOutputStream output = res.getOutputStream();

            output.write(new byte[] {(byte) OP.sREQUESTPACKETS.ordinal(), 1});
        } catch (IOException e) {
            e.printStackTrace();
        }

        Util.completeAsyncContext(aCtx);
    }
    public void handleRequestPackets(RequestWrapper requestWrapper) {
        // Do nothing: the HttpRequest objects will be put into the comet queue already
        PlayerSession playerSession = requestWrapper.getPlayerSession();
        if(playerSession == null) {
            return;
        }
        
        playerSession.setNumExpectedPackets(playerSession.getNumExpectedPackets() - 1);
    }
    
    public void handleLogin(RequestWrapper requestWrapper) {
        ByteBuffer buf = requestWrapper.getByteBuffer();
        PlayerSession playerSession = requestWrapper.getPlayerSession();
        if(playerSession == null) {
            return;
        }
        
        AsyncContext aCtx = requestWrapper.getAsyncContext();
        
        String accname = Util.getStringFromByteBuffer(buf);
        String SHA1Hash = Util.getStringFromByteBuffer(buf);
        System.out.println("Login: " + accname + " : " + SHA1Hash + " " + playerSession.getLastSession().getId());
        
        // Get the account if it exists
        try {
            Account acc = (Account) em.createQuery("SELECT acc FROM account acc WHERE acc.name = :accname")
                    .setParameter("accname", accname)
                    .getSingleResult();
            System.out.println("EntityManager found your " + acc.getName());
            if(acc.getHash().equals(SHA1Hash)) {
                playerSession.setAccount(acc);
                // TODO: Send login ok, character choices
                // TODO: Move this to after character has been chosen
                // TODO : remove: addToWorld should be called by character choice packet and handled by WorldController
                ServletContext context = aCtx.getRequest().getServletContext();
                
                WorldController wc = (WorldController) context.getAttribute("WorldController");
                if(wc.addPlayerToWorld(playerSession)) {
                    System.out.println("You should be logged in now");
                }
            }
            else {
                // incorrect password
                System.out.println("Incorrect password: received " + SHA1Hash + ", expected " + acc.getHash());
            }
        }
        catch(NoResultException e) {
            System.out.println("Account does not exist");
            e.printStackTrace();
        }
    }
    
    public void handleBrowserTest(RequestWrapper requestWrapper) {
        System.out.println("Received BrowserTest packet");
        ByteBuffer buf = requestWrapper.getByteBuffer();
        AsyncContext aCtx = requestWrapper.getAsyncContext();
        
        // Check the data sent from the client; make sure that binary data is sent correctly (bytes should go from 0 to 255)
        for(int b = 0; b < 256; ++b) {
            if((byte)b != buf.get()) {
                System.err.println("cBROWSERTEST: Encoding problem: " + b);
                break;
            }
        }

        byte[] primitiveByteEncodingTestArray = new byte[256];
        for(int b = 0; b < 256; ++b) {
            primitiveByteEncodingTestArray[b] = (byte)b;
        }

        ServletRequest req = aCtx.getRequest();
        ServletResponse res = aCtx.getResponse();

        try {
            ServletOutputStream output = res.getOutputStream();

            output.write(primitiveByteEncodingTestArray);
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        assert(req.isAsyncStarted());
        Util.completeAsyncContext(req.getAsyncContext());
    }
    
    public void handleLogout(RequestWrapper requestWrapper) {
        PlayerSession playerSession = requestWrapper.getPlayerSession();
        if(playerSession == null) {
            System.out.println("Session logout without playerSession");
            return;
        }

        System.out.println("Session logout " + playerSession.getLastSession().getId());
        AsyncContext aCtx = requestWrapper.getAsyncContext();
        
        Util.completeAsyncContext(aCtx);
        playerSession.getLastSession().invalidate();
    }
    
    /**
     * Will be used after image URL obfuscation is implemented
     * @param requestWrapper
     */
    public void handleRequestImage(RequestWrapper requestWrapper) {
        
    }
    public void sendPreloadImage(PlayerSession playerSession, String imgName) {
        ByteBuffer buf = ByteBuffer.allocate(3 + 2 * imgName.length()); // 1 + 2*length + 2
        buf.put((byte) OP.sPRELOADIMAGE.ordinal());
        Util.writeStringToBuffer(buf, imgName);
        playerSession.writeToOutput(buf);        
    }
    
    public void invalidPacket(RequestWrapper requestWrapper) {

    }
    public void nyiPacket(RequestWrapper requestWrapper) {
        throw new UnsupportedOperationException();
    }
    
    public void finalize() {
        em.close();
        emf.close();
    }
}
