package mmo.server;

import java.lang.ref.WeakReference;
import java.util.concurrent.ExecutionException;

public class PlayerSessionFlushTask extends ScheduledTask {
    public PlayerSessionFlushTask(PlayerSession playerSession) {
        super.delayUntil = System.currentTimeMillis() + defaultDelay;
        
        this.playerSessionRef = new WeakReference<PlayerSession>(playerSession);
    }
    
    private final static int defaultDelay = 100;
    
    WeakReference<PlayerSession> playerSessionRef;
    
    @Override
    public Boolean get() throws InterruptedException, ExecutionException {
        PlayerSession playerSession = playerSessionRef.get();
        if(playerSession != null) {
            playerSession.flush();
            return true;
        }
        
        return false;
    }
}
