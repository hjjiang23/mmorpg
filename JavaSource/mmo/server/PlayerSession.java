package mmo.server;

import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;
import java.lang.ref.WeakReference;
import java.nio.ByteBuffer;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

import javax.servlet.AsyncContext;
import javax.servlet.AsyncListener;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mmo.Intent;
import mmo.Util;
import mmo.server.Opcode.OP;
import mmo.world.Player;
import mmo.world.Unit;

public class PlayerSession implements Serializable {
    private static final long serialVersionUID = -7402644177077152331L;
    
    // TODO: Put these in a properties file
    private static final int COMET_QUEUE_CAPACITY;
    static {
        int temp = 3;
        try {
            System.getProperties().load(PlayerSession.class.getClassLoader().getResourceAsStream("resources.properties"));
            temp = Integer.valueOf(System.getProperty("CometQueueCapacity"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        COMET_QUEUE_CAPACITY = temp;
    }

    public PlayerSession() {
        System.out.println("PlayerSession " + this + " instantiated");
    }
    
    /********************
     * Initialization
     ********************/
    
    transient private boolean isSessionNew = false;
    transient private boolean isSessionValid = false;
    transient private WeakReference<HttpSession> sessRef = null;
    transient private ReentrantLock outputStreamLock = new ReentrantLock();
    transient private ReentrantLock cometQueueLock = new ReentrantLock();
    
    public void setSession(HttpSession sess) {
        if(this.sessRef == null || this.sessRef.get() != sess)
            isSessionNew = true;

        this.sessRef = new WeakReference<HttpSession>(sess);
    }
    public void sessionInitialized() {
        isSessionNew = false;
        isSessionValid = true;
    }
    public boolean isSessionNew() {
        return isSessionNew;
    }
    public boolean isSessionValid() {
        return isSessionValid;
    }
    public HttpSession getLastSession() {
        return sessRef.get();
    }
    public void invalidate() {
        isSessionValid = false;
    }
    
    /********************
     * World Logic Methods
     ********************/
    
    transient private Account acc = null;
    transient private Player originalMover = null; // Although mover can be a unit, make sure only the original Players are removed when the PlayerSession is destroyed!
    transient private Unit mover = null; // Although mover can be a unit, make sure only the original Players are removed when the PlayerSession is destroyed!
    transient private long lastMovementPacketID = 0;
    transient private Set<Intent> intents = Collections.synchronizedSet(new HashSet<Intent>());
    
    public Account getAccount() {
        return acc;
    }
    public void setAccount(Account acc) {
        this.acc = acc;
    }
    
    public Player getOriginalMover() {
        return originalMover;
    }
    public void setOriginalMover(Player originalMover) {
        this.originalMover = originalMover;
    }
    public Unit getMover() {
        return mover;
    }
    public void setMover(Unit mover) {
        this.mover = mover;
    }
    
    protected boolean isLoggedIn() {
        return acc != null;
    }
    protected boolean isInWorld() {
        return originalMover != null;
    }
    
    public long getLastMovementPacketID() {
        return lastMovementPacketID;
    }
    public void setLastMovementPacketID(long lastMovementPacketID) {
        this.lastMovementPacketID = lastMovementPacketID;
    }
    
    public boolean addIntent(Intent intent) {
        if(!isSessionValid()) {
            return false;
        }
        
        // If there are no updates that do not use Intents, then there does not have to be a PlayerSessionFlushTask active
        // getOutputStream() creates a flush task for intents
        if(output == null) {
            createOutputStream();
        }

        intents.remove(intent);
        return intents.add(intent);
    }
    
    /********************
     * Comet Methods
     ********************/
    
    transient private final BlockingQueue<RequestWrapper> cometQueue = new ArrayBlockingQueue<RequestWrapper>(COMET_QUEUE_CAPACITY);
    transient private AsyncListener cometExpirationListener;
    transient private int expectedFillerPackets = 0;
    
    transient private AsyncContext currentOutputCtx = null; 
    transient private OutputStream output = null;
    
    public int getCometQueueSize() {
        return cometQueue.size();
    }
    public void addToCometQueue(RequestWrapper requestWrapper) {
        if(!cometQueue.offer(requestWrapper)) {
            // Burn the oldest request
            RequestWrapper oldRequestWrapper = takeFromCometQueue();
            assert(oldRequestWrapper != null);
            Util.completeAsyncContext(oldRequestWrapper.getAsyncContext());

            cometQueue.offer(requestWrapper);
        }
    }
    private RequestWrapper takeFromCometQueue() {
        cometQueueLock.lock();
        try {
            if (cometQueue.size() > 1 ) {
                return cometQueue.poll(20, TimeUnit.MILLISECONDS);
            }
            else if(COMET_QUEUE_CAPACITY > 1 && cometQueue.size() == 1 && getNumExpectedPackets() == 0) {
                RequestWrapper requestWrapper = cometQueue.poll(10000, TimeUnit.MILLISECONDS);
                if(requestWrapper == null) {
                    return null; // Nothing we can do. Die gracefully, and hope client calls back
                }

                AsyncContext aCtx = requestWrapper.getAsyncContext();
                HttpServletResponse res = (HttpServletResponse) aCtx.getResponse();

                try {
                    ServletOutputStream outputStream = res.getOutputStream();
                    outputStream.write(new byte[] {(byte) OP.sREQUESTPACKETS.ordinal(), (byte) (COMET_QUEUE_CAPACITY - 1)});
                } catch (IOException e) {
                    e.printStackTrace();
                }

                setNumExpectedPackets(getNumExpectedPackets() + COMET_QUEUE_CAPACITY - 1);
                
                // aCtx.complete(); // Not sure if should send immediately or wait
                return requestWrapper;
            }
            
            return cometQueue.poll(10000, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
            return null;
        } finally {
            cometQueueLock.unlock();
        }
    }
    public RequestWrapper removeFromCometQueue(AsyncContext aCtx) {
        Iterator<RequestWrapper> i = cometQueue.iterator(); // concurrent package iterators are weakly consistent
        while(i.hasNext()) {
            RequestWrapper requestWrapper = i.next();
            if(requestWrapper.getAsyncContext() == aCtx) {
                i.remove();
                return requestWrapper;
            }
        }
        
        // Since iterator is only weakly consistent, log when AsyncContext is not found to see how often this happens
        System.err.println("removeFromCometQueue: did not find AsyncContext");
        return null;
    }
    public void setCometExpirationListener(AsyncListener asyncListener) {
        cometExpirationListener = asyncListener;
    }
    public AsyncListener getCometExpirationListener() {
        return cometExpirationListener;
    }
    public int getNumExpectedPackets() {
        return expectedFillerPackets;
    }
    public void setNumExpectedPackets(int expectedFillerPackets) {
        this.expectedFillerPackets = expectedFillerPackets;
    }
    
    private OutputStream createOutputStream() {
        outputStreamLock.lock();
        try {
            if(output == null) {
                // This should be the first; takeFromCometQueue is the most likely of these to throw exception
                RequestWrapper requestWrapper = takeFromCometQueue();
                if(requestWrapper == null)
                    return null;

                this.currentOutputCtx = requestWrapper.getAsyncContext();
                this.output = currentOutputCtx.getResponse().getOutputStream();
                
                ConnectionsController cc = (ConnectionsController) getLastSession().getServletContext().getAttribute("ConnectionsController");
                cc.addFutureEvent(new PlayerSessionFlushTask(this));
            }
        // IllegalStateException should only be thrown if client has closed page on browser
        } catch(IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            outputStreamLock.unlock();
        }
        return output;
    }
    public void flush() {
        if(output == null || !isSessionValid()) {
            System.err.println("Flush called with no stream open");
            return;
        }
        
        outputStreamLock.lock();
        try {
            synchronized(intents) {
                Iterator<Intent> i = intents.iterator(); // Must be in the synchronized block
                while (i.hasNext()) {
                    i.next().writeContents(output);
                    i.remove();
                }
            }

            output.flush(); // implicit
            output.close();
            
            currentOutputCtx.complete();
            
            this.output = null;
            this.currentOutputCtx = null;
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            outputStreamLock.unlock();
        }
    }
    public void writeToOutput(ByteBuffer buf) {
        outputStreamLock.lock();
        try {
            // TODO: this will cause deadlocks. Need to cancel this operation and save contents if this fails.
            while(output == null) {
                createOutputStream();
            }
            output.write(buf.array());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            outputStreamLock.unlock();
        }
    }
    
    /********************
     * Misc. Methods
     ********************/
    
    @Override
    protected void finalize() {
        try {
            super.finalize();
        } catch (Throwable e) {
            e.printStackTrace();
        }
        System.out.println("PlayerSession " + this + " destroyed");
    }
    
    /*@Override
    public String toString() {
        return super.toString()
    }*/
}
