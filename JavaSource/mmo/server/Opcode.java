package mmo.server;

public class Opcode {
    // All opcodes prefixed with 'c' are sent from client to server
    // ""                        's' are sent from server to client
    public static enum OP {
        cGETPUBLICKEY,
        sREQUESTPACKETS,
        cREQUESTPACKETS,
        cOPENSTREAM,
        cLOGIN,
        cCHARCHOICE,
        sCHARCHOICE,
        cLOGOUT,
        sMOVE,
        cMOVE,
        sINITUNIT,
        sDESTROYUNIT,
        cSENDMSG,
        cBROWSERTEST,
        sBROWSERTEST,
        sSETMOVER,
        sPRELOADIMAGE,
        cREQCRETEMPLATE,
        sINITCRETEMPLATE,
        sINITTERRAIN,
        sDESTROYTERRAIN,
        cCHAT,
        sCHAT,
        sSETCURRHP,
        sSETMAXHP,
        cMELEEATTACK,
        sMELEEATTACK
    }
    
    // The class and the method in that class which handle each operation.
    // OPs that are prefixed 's' should not be received by the server, and are sent to invalidPacket in ConnectionsController.
    // Note: Optionally, direct OPs that are not yet implemented, but should be received by the server, to nyiPacket.
    public static String[][] OPHandlerNames = 
    {
        {"ConnectionsController", "handleGetPublicKey"},                // GETPUBLICKEY
        {"ConnectionsController", "invalidPacket"},
        {"ConnectionsController", "handleRequestPackets"},              // REQUESTPACKETS
        {"ConnectionsController", "handleOpenStream"},                  // OPENSTREAM
        {"ConnectionsController", "handleLogin"},                       // LOGIN
        {"ConnectionsController", "nyiPacket"},                         // CHARCHOICE
        {"ConnectionsController", "invalidPacket"},
        {"ConnectionsController", "handleLogout"},                      // LOGOUT
        {"ConnectionsController", "invalidPacket"},
        {"WorldController", "handleMove"},                              // MOVE
        {"ConnectionsController", "invalidPacket"},
        {"ConnectionsController", "invalidPacket"},
        {"ConnectionsController", "nyiPacket"},                         // SENDMSG
        {"ConnectionsController", "handleBrowserTest"},                 // BROWSERTEST
        {"ConnectionsController", "invalidPacket"},
        {"ConnectionsController", "invalidPacket"},
        {"ConnectionsController", "invalidPacket"},
        {"WorldController", "handleReqCreTemplate"},                    // REQCRETEMPLATE
        {"ConnectionsController", "invalidPacket"},
        {"ConnectionsController", "invalidPacket"},
        {"ConnectionsController", "invalidPacket"},
        {"WorldController", "handleChat"},                              // CHAT
        {"ConnectionsController", "invalidPacket"},
        {"ConnectionsController", "invalidPacket"},
        {"ConnectionsController", "invalidPacket"},
        {"WorldController", "handleMeleeAttack"},                       // MELEEATTACK
        {"ConnectionsController", "invalidPacket"},
    };
    
    public static int getNumOpcodes() {
        return Opcode.OP.values().length;
    }
    
    public static boolean allowCometResponse(OP op) {
        switch(op) {
            case cBROWSERTEST:
                return false;
            default:
                return true;
        }
    }
}
