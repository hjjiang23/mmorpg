package mmo.server;

import mmo.Intent;

/**
 * This class is used to wrap a notification that would otherwise be sent many times to the client in a single servlet response,
 * when only the most recent update would be relevant (for example, an update for a unit's attribute, or location). This prevents
 * redundant or obsolete packets from being sent per response.
 * Upon creation, each notification is split into the constant arguments (the type of update and the target), and the variable arguments.
 * The constant arguments are used to check for equalTo(Object o) equality.
 * @author Alan Wang
 *
 */
public class NotifyIntent extends Intent {
    public NotifyIntent(Object[] constants, Object[] variables, boolean[] constantIndices, int totalBytes) {
        super(constants, variables, constantIndices, totalBytes);
    }
}
