package mmo.server;

import java.io.Serializable;


/**
 * The persistent class for the account database table.
 * 
 */
public class Account implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;

	private String name;
	private String hash;

    public Account() {
    }

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
	    System.out.println("Account " + id + " loaded");
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    @Override
    public String toString() {
        return id + " " + getName();
    }
}