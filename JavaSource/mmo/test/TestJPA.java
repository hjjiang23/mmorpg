package mmo.test;

import junit.framework.TestCase;
import java.sql.SQLException;

import org.apache.naming.java.javaURLContextFactory;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NameNotFoundException;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class TestJPA {
    private static InitialContext ic;

    @BeforeClass
    public static void setUpClass() {
        try {
            // Create initial context
            System.setProperty(Context.INITIAL_CONTEXT_FACTORY, "org.apache.naming.java.javaURLContextFactory");
            System.setProperty(Context.URL_PKG_PREFIXES,  "org.apache.naming");
            
            ic = new InitialContext();
            try {
                ic.lookupLink("java:comp/env/jdbc/MMODS");
            }
            catch(NameNotFoundException e) {
                ic.createSubcontext("java:");
                ic.createSubcontext("java:comp");
                ic.createSubcontext("java:comp/env");
                ic.createSubcontext("java:comp/env/jdbc");
               
                // Construct DataSource
                com.mysql.jdbc.jdbc2.optional.MysqlDataSource ds = new com.mysql.jdbc.jdbc2.optional.MysqlDataSource();
                ds.setDatabaseName("mmo");
                ds.setUser("root");
                ds.setPassword("admin");
                ic.bind("java:comp/env/jdbc/MMODS", ds);
            }
        } catch (NamingException ex) {
            ex.printStackTrace();
        }
    }
    
    @Before
    public void setUp() {
    }
        
    public DataSource testGetDatasource() {
        String DS_Context = "java:comp/env/jdbc/MMODS";

        System.out.println("Running TestJPA");
        try {
            DataSource datasource = (DataSource)ic.lookup(DS_Context);
    
            if (datasource != null)
                return datasource;
            else {
                System.err.println("Failed: datasource was null");
                return null;
            }
        } catch (NamingException e) {
            e.printStackTrace();
            return null;
        }
    }
    
    @Test
    public void testConnection() {
        DataSource ds = testGetDatasource();
        try {
            ds.getConnection();
            System.out.println("TestJPA: connection successful");
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("Check in WebContext/META-INF/context.xml and make sure your connection is configured correctly, including the correct username/password combination");
            System.out.println("Example configuration:"+
                    "<Resource name=\"jdbc/MMODS\" auth=\"Container\" type=\"javax.sql.DataSource\"" +
                    "\tmaxActive=\"100\" maxIdle=\"30\" maxWait=\"10000\"" +
                    "\tusername=\"root\" password=\"admin\" driverClassName=\"com.mysql.jdbc.Driver\"" +
                    "\turl=\"jdbc:mysql://localhost:3306/mmo\"/>");
        }
    }
    
    @After
    public void tearDown() {
    }
    
    @AfterClass
    public static void tearDownClass() {
        try {
            ic.close();
        } catch (NamingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
