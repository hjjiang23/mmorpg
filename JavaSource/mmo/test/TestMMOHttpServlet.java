package mmo.test;

import java.awt.geom.Area;
import java.awt.geom.Rectangle2D;
import java.nio.ByteBuffer;
import java.util.Collection;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NameNotFoundException;
import javax.naming.NamingException;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import junit.framework.TestCase;

import mmo.server.MMOHttpServlet;
import mmo.server.MMOHttpSessionListener;
import mmo.server.RequestWrapper;
import mmo.world.CollectiveActor;
import mmo.world.Creature;
import mmo.world.CreatureTemplate;
import mmo.world.GameObject;
import mmo.world.MapController;
import mmo.world.Position;
import mmo.world.Terrain;
import mmo.world.Unit;
import mmo.world.WorldController;


import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class TestMMOHttpServlet {
    private static InitialContext ic;
    static MMOHttpServlet servlet;

    @BeforeClass
    public static void setUpClass() {
        try {
            // Create initial context
            System.setProperty(Context.INITIAL_CONTEXT_FACTORY, "org.apache.naming.java.javaURLContextFactory");
            System.setProperty(Context.URL_PKG_PREFIXES,  "org.apache.naming");            
            ic = new InitialContext();

            try {
                ic.lookupLink("java:comp/env/jdbc/MMODS");
            }
            catch(NameNotFoundException e) {
                ic.createSubcontext("java:");
                ic.createSubcontext("java:comp");
                ic.createSubcontext("java:comp/env");
                ic.createSubcontext("java:comp/env/jdbc");
               
                // Construct DataSource
                com.mysql.jdbc.jdbc2.optional.MysqlDataSource ds = new com.mysql.jdbc.jdbc2.optional.MysqlDataSource();
                ds.setDatabaseName("mmo");
                ds.setUser("root");
                ds.setPassword("admin");
                ic.bind("java:comp/env/jdbc/MMODS", ds);
            }
        } catch (NamingException ex) {
            ex.printStackTrace();
        }
        
        servlet = new MMOHttpServlet();
        
        // TODO: We need method to instantiate ServletContext in ServletsContainer...
    }
    
    @Before
    public void setUp() {
    }
    
    @Test
    public void testDoNothing() {
        
    }

    @After
    public void tearDown() {
    }
    
    @AfterClass
    public static void tearDownClass() {
        try {
            ic.close();
        } catch (NamingException e) {
            e.printStackTrace();
        }
    }
}
