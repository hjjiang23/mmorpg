package mmo.test;

import java.awt.geom.Area;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.Collection;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NameNotFoundException;
import javax.naming.NamingException;

import junit.framework.TestCase;

import mmo.server.Account;
import mmo.server.PlayerSession;
import mmo.server.RequestWrapper;
import mmo.world.CollectiveActor;
import mmo.world.Creature;
import mmo.world.CreatureTemplate;
import mmo.world.GameObject;
import mmo.world.MapController;
import mmo.world.Position;
import mmo.world.Terrain;
import mmo.world.Unit;
import mmo.world.WorldController;

import static org.mockito.Mockito.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class TestWorldController {
    private final int NUM_TEST_CREATURES = 22;
    private static WorldController wc;
    private static InitialContext ic;

    @BeforeClass
    public static void setUpClass() {
        try {
            // Create initial context
            System.setProperty(Context.INITIAL_CONTEXT_FACTORY, "org.apache.naming.java.javaURLContextFactory");
            System.setProperty(Context.URL_PKG_PREFIXES,  "org.apache.naming");            
            ic = new InitialContext();

            try {
                ic.lookupLink("java:comp/env/jdbc/MMODS");
            }
            catch(NameNotFoundException e) {
                ic.createSubcontext("java:");
                ic.createSubcontext("java:comp");
                ic.createSubcontext("java:comp/env");
                ic.createSubcontext("java:comp/env/jdbc");
               
                // Construct DataSource
                com.mysql.jdbc.jdbc2.optional.MysqlDataSource ds = new com.mysql.jdbc.jdbc2.optional.MysqlDataSource();
                ds.setDatabaseName("mmo");
                ds.setUser("root");
                ds.setPassword("admin");
                ic.bind("java:comp/env/jdbc/MMODS", ds);
            }
        } catch (NamingException ex) {
            ex.printStackTrace();
        }
        
        wc = new WorldController();
    }
    
    @Before
    public void setUp() {
    }

    @Test
    public void testGetUnitsForArea() {
        Collection<Creature> creatures = wc.getCreaturesForArea(new Area(new Rectangle2D.Float(0, 0, 1000, 1000)));
        TestCase.assertEquals("testGetUnitsInArea failed", NUM_TEST_CREATURES, creatures.size());
        
        creatures = wc.getCreaturesForArea(new Area(new Rectangle2D.Float(200, 200, 300, 300)));
        TestCase.assertEquals("Bounds should be inclusive", 2, creatures.size());
    }
    
    @Test
    public void testGetTerrainsForArea() {
        Collection<Terrain> terrains = wc.getTerrainsForArea(new Area(new Rectangle2D.Float(0, 0, 1000, 1000)));
        TestCase.assertEquals("Should have retrieved all terrains", 8, terrains.size());
        
        terrains = wc.getTerrainsForArea(new Area(new Rectangle2D.Float(0, 0, 100, 100)));
        TestCase.assertEquals("Bounds should be inclusive", 4, terrains.size());
    }
    
    @Test
    public void testAddRemoveUnit() {
        CreatureTemplate creTemp = new CreatureTemplate();
        creTemp.setId(-1);
        creTemp.setMinDmg(1);
        creTemp.setMaxDmg(1);
        creTemp.setAIname(null);
        creTemp.setName("TestBug");
        
        Creature cre = new Creature();
        cre.setPos(new Position(-5252, 5252));
        cre.setCreatureTemplate(creTemp);
        
        try {
            wc.addCreatureToWorld(cre);
        }
        catch(NullPointerException e) {
            // e.printStackTrace();
            TestCase.assertEquals("mmo.world.WorldController", e.getStackTrace()[0].getClassName());
            TestCase.assertEquals("addUnit", e.getStackTrace()[0].getMethodName());
        }
        
        cre.setGroup(new CollectiveActor());
        wc.addCreatureToWorld(cre);
        
        MapController mc = wc.getMapControllerForGO(cre);
        TestCase.assertNotNull(mc);
        
        TestCase.assertEquals(1, wc.getAllObjects().size());
        wc.removeUnit(cre);
        TestCase.assertEquals(0, wc.getAllObjects().size());
    }
    
    @Test
    public void testLoadFromDatabase() {
        CreatureTemplate creTemp = new CreatureTemplate();
        creTemp.setId(-1);
        creTemp.setMinDmg(1);
        creTemp.setMaxDmg(1);
        creTemp.setAIname(null);
        creTemp.setName("TestBug");
        
        Creature cre = new Creature();
        cre.setPos(new Position(500, 500));
        cre.setCreatureTemplate(creTemp);
        
        cre.setGroup(new CollectiveActor());
        wc.addCreatureToWorld(cre);
        
        MapController mc = wc.getMapControllerForGO(cre);
        TestCase.assertNotNull(mc);
        
        TestCase.assertEquals(NUM_TEST_CREATURES + 1, wc.getAllObjects().size());
        wc.removeUnit(cre);
    }
    
    @Test
    public void testGetObjectsOfTypeAround() {
        CreatureTemplate creTemp = new CreatureTemplate();
        creTemp.setId(-1);
        creTemp.setMinDmg(9999);
        creTemp.setMaxDmg(9999);
        creTemp.setAIname(null);
        creTemp.setName("TestBug");
        
        Creature cre = new Creature();
        cre.setPos(new Position(4096, 4096));
        cre.setOrientation(0.);
        cre.setCreatureTemplate(creTemp);
        
        Creature target = new Creature();
        target.setPos(new Position(4096, 4097));
        target.setCreatureTemplate(creTemp);
        
        cre.setGroup(new CollectiveActor());
        wc.addCreatureToWorld(cre);
        TestCase.assertEquals(true, cre.isValid());
        
        target.setGUID(2);
        wc.addCreatureToWorld(target);
        TestCase.assertEquals(true, target.isValid());
        
        TestCase.assertEquals("WC should find 1 target around cre", 1, wc.getObjectsOfTypeAround(Creature.class, cre, false).size());
        TestCase.assertEquals("WC should find 2 creatures around cre (including self)", 2, wc.getObjectsOfTypeAround(Creature.class, cre, true).size());
        
        // Now do the same test thing for mc
        MapController mc = wc.getMapControllerForGO(cre);
        TestCase.assertEquals("MC should find 1 target around cre", 1, mc.getObjectsOfTypeAround(Creature.class, cre, false).size());
        TestCase.assertEquals("MC should find 2 creatures around cre (including self)", 2, mc.getObjectsOfTypeAround(Creature.class, cre, true).size());
        TestCase.assertSame("Both should refer to the same MapController", mc, cre.getController());
        TestCase.assertEquals("MC should find 1 target around cre", 1, cre.getController().getObjectsOfTypeAround(Unit.class, cre, false, (float)1.00001).size());
    }
    
    @Test
    public void testRetainMeleeRange() {
        CreatureTemplate creTemp = new CreatureTemplate();
        creTemp.setId(-1);
        creTemp.setMinDmg(9999);
        creTemp.setMaxDmg(9999);
        creTemp.setAIname(null);
        creTemp.setRange(1.);
        creTemp.setName("TestBug");
        
        Creature cre = new Creature();
        cre.setPos(new Position(10000, 10000));
        cre.setOrientation(0.);
        cre.setCreatureTemplate(creTemp);
        
        Creature target = new Creature();
        target.setPos(new Position(10000, 10001));
        target.setCreatureTemplate(creTemp);
        
        cre.setGroup(new CollectiveActor());
        wc.addCreatureToWorld(cre);
        TestCase.assertEquals(true, cre.isValid());
        
        target.setGUID(2);
        wc.addCreatureToWorld(target);
        TestCase.assertEquals(true, target.isValid());
        
        Collection<Creature> potentialTargets = wc.getObjectsOfTypeAround(Creature.class, cre, false);
        cre.retainAllInMeleeRange(potentialTargets);
        TestCase.assertEquals("target should be in range of cre", 1, potentialTargets.size());
        
        creTemp.setRange(0.9);
        cre.retainAllInMeleeRange(potentialTargets);
        TestCase.assertEquals("target should not be in range of cre", 0, potentialTargets.size());
    }
    
    @Test
    public void testAttackUnit() {
        CreatureTemplate creTemp = new CreatureTemplate();
        creTemp.setId(-1);
        creTemp.setMinDmg(9999);
        creTemp.setMaxDmg(9999);
        creTemp.setAIname(null);
        creTemp.setRange(1.);
        creTemp.setName("TestBug");
        
        Creature cre = new Creature();
        cre.setPos(new Position(10000, 10000));
        cre.setOrientation(0.);
        cre.setCreatureTemplate(creTemp);
        
        Creature target = new Creature();
        target.setGUID(2);
        target.setPos(new Position(10000.0f, 10000.999f));
        target.setCreatureTemplate(creTemp);
        
        cre.setGroup(new CollectiveActor());
        wc.addCreatureToWorld(cre);
        TestCase.assertEquals(true, cre.isValid());
        
        wc.addCreatureToWorld(target);
        TestCase.assertEquals(true, target.isValid());
        
        Collection<Unit> potentialTargets = wc.getObjectsOfTypeAround(Unit.class, cre, false);
        cre.retainAllInMeleeRange(potentialTargets);
        TestCase.assertEquals("target should be in range of cre", 1, potentialTargets.size());
        
        cre.attack();
        TestCase.assertEquals(true, target.isDead());
    }
    
    // @Test
    // Not to be included in normal test cases
    public void testSanityChecks() {
        Rectangle2D.Float rect = new Rectangle2D.Float(0.0f, 0.0f, 1000.0f, 1000.0f);
        Area area = new Area(rect);
        TestCase.assertEquals("Rect2D does not contain point on border", false, area.contains(new Point2D.Float(1000.0f, 500.0f)));
        TestCase.assertEquals("Rect2D does not contain point on border", false, area.contains(new Point2D.Float(1000.0f, 1000.0f)));
        TestCase.assertEquals("Rect2D should contain point", true, area.contains(new Point2D.Float(999.999f, 999.999f)));
    }

   @Test
   public void testAddPlayerToWorld()
   {
      PlayerSession playerSession = mock(PlayerSession.class);
      Account fakeAccount = new Account();
      fakeAccount.setName("Test1");
      fakeAccount.setId(1);
      when(playerSession.getAccount()).thenReturn(fakeAccount);
      PlayerSession playerSession2 = mock(PlayerSession.class);
      Account fakeAccount2 = new Account();
      fakeAccount.setName("Test2");
      fakeAccount.setId(2);
      when(playerSession2.getAccount()).thenReturn(fakeAccount2);
      
      TestCase.assertTrue(wc.addPlayerToWorld(playerSession));
      // add again to check the expiration code
      TestCase.assertTrue(wc.addPlayerToWorld(playerSession));
     
      when(playerSession.isSessionValid()).thenReturn(true);
      TestCase.assertFalse(wc.addPlayerToWorld(playerSession));
      
      // Tim: this line fails
      //TestCase.assertTrue(wc.addPlayerToWorld(playerSession2));
    }
  
    @Test
    public void testNotifyUnitCreated()
    {
        //create creature, add player should trigger notifyUnitCreated
        CreatureTemplate creTemp = new CreatureTemplate();
        creTemp.setId(-1);
        creTemp.setMinDmg(9999);
        creTemp.setMaxDmg(9999);
        creTemp.setAIname(null);
        creTemp.setRange(1.);
        creTemp.setName("TestBug");
        
        Creature cre = new Creature();
        cre.setPos(new Position(10000, 10000));
        cre.setOrientation(0.);
        cre.setCreatureTemplate(creTemp);

        PlayerSession playerSession = mock(PlayerSession.class);
        Account fakeAccount = new Account();
        fakeAccount.setName("Test1");
        fakeAccount.setId(1);
        when(playerSession.getAccount()).thenReturn(fakeAccount);
        
        TestCase.assertTrue(wc.addPlayerToWorld(playerSession));
        // add again to check the expiration code
        TestCase.assertTrue(wc.addPlayerToWorld(playerSession));
    }

    @After
    public void tearDown() {
        for(GameObject go : wc.getAllObjects()) {
            if(go instanceof Unit) {
                wc.removeUnit((Unit) go);
            }
        }
    }
    
    @AfterClass
    public static void tearDownClass() {
        wc = null;
        try {
            ic.close();
        } catch (NamingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
