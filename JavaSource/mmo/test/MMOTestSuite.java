package mmo.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({ TestJPA.class, TestWorldController.class, TestMMOHttpServlet.class })
public class MMOTestSuite {
}
