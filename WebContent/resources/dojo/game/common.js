dojo.provide("game.common");

var debugMode = true;

function l() {
    if(window.console && console.log && console.log.apply != undefined) {
        console.log.apply(console, arguments);
    }
}
function lc(msg, color) {
    // Color these messages color (append %c, remove the color from arguments)
    if(window.console && console.log && console.log.apply != undefined) {
        arguments[0] = "%c" + arguments[0];
        [].splice.call(arguments, 1, 1, color);

        console.log.apply(console, arguments);
    }
}
function d() {
    // Color these messages blue
    if(window.console && console.debug && console.debug.apply != undefined) {
        arguments[0] = "%c" + arguments[0];
        [].splice.call(arguments, 1, 0, 'color:blue');

        console.debug.apply(console, arguments);
    }
}
function e() {
    if(window.console && console.error && console.error.apply != undefined) {
        console.error.apply(console, arguments);
    }
}
function a() {
    alert.apply(this, arguments);
        
    window.focus();
}

function getDebugMode() {
    return debugMode;
}
function setDebugMode(newMode) {
    debugMode = newMode;
}

// Opera workaround for Object.keys (from https://developer.mozilla.org/en/JavaScript/Reference/Global_Objects/Object/keys)
if(!Object.keys) {
    Object.keys = function(o) {
        if (o !== Object(o))
            throw new TypeError('Object.keys called on non-object');
        var ret=[], p=0;
        for(p in o)
            if(Object.prototype.hasOwnProperty.call(o,p))
                ret.push(p);
        return ret;
    };
}

