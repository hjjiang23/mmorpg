dojo.provide("game.ui.ui");

dojo.require("game.common");
dojo.require("game.ui.MMOFloatingPane");
dojo.require("game.ui.MMOScrollPane");
dojo.require("game.ui.TargetPane");
dojo.require("game.util.util");
dojo.require("game.controls");
dojo.require("game.net.client");
dojo.require("game.net.chat");
dojo.require("game.graphics.graphics");

dojo.provide("dojox.layout.ContentPane");
dojo.require("dojox.layout.TableContainer");
dojo.require("dijit.Menu");
dojo.require("dijit.MenuBar");
dojo.require("dijit.TitlePane");

// dojo.require("dojox.layout.FloatingPane");


/*var currentState = 0;
var validStates = {
        SPLASH : 0,
        WORLD : 1
    };*/

var invWindow;
var targetListWindow;

function initUI() {
    displayMenuBar();
    initGfx();
    bindChat();
}

function bindChat()
{
	 $('#inputBox').focus(function(){
		 showChat();
	 });
	 
	 $('#inputBox').blur(function(){
		 hideChat();
	 });
}

function displayMenuBar() {
    var menu = new dijit.Menu({
            label: "Hello World"
        });
    menu.addChild(new dijit.MenuItem({
            label: "Menu",
            onMouseDown: function(event) { l("Menu button pressed"); return false; }
        }));
    /*menu.addChild(new dijit.MenuItem({
            label: "Top Menu Button 2",
            onMouseDown: function(event) { l("Menu button pressed"); return false; }
        }));*/
    menu._onBlur = function() {
            menu.inherited('_onBlur',arguments);
            dijit.popup.close(menu);
        };
    
    menu.placeAt("menubar");
    menu.startup();
}

function openContextMenu() {
    var menu = new dijit.Menu({
            label: "Context Menu",
            showTitle: true
        });
    menu.addChild(new dijit.MenuItem({
            label: "Menu",
            onClick: function(event) {
                    l("Context menu button pressed");
                    dijit.popup.close(menu);
                }
        }));
    menu._onBlur = function() {
            menu.inherited('_onBlur',arguments);
            dijit.popup.close(menu);
        };
    
    var clientX = getMouseLoc().x;
    var clientY = getMouseLoc().y;
    dijit.popup.open({ popup: menu, x: clientX, y: clientY } );
    
    menu.focus(); // need this so that _onBlur is called
}

function toggleModalWindow() {
    if(!toggleModalWindow.menuWindow) {
        toggleModalWindow.menuWindow = new dijit.Dialog({
                title: "My Dialog",
                content: "test content",
                style: "width: 300px"
            });
            
        dojo.connect(toggleModalWindow.menuWindow, "ondragstart",   dojo, "stopEvent");
        dojo.connect(toggleModalWindow.menuWindow, "onselectstart", dojo, "stopEvent");
    }
    menuWindow = toggleModalWindow.menuWindow;
    
    if(!menuWindow._isShown()) {
        menuWindow.show();
    }
    else {
        menuWindow.hide();
    }
}

function toggleInventoryWindow() {
    if(!invWindow) {
        invWindow = new MMOFloatingPane({ 
            id: "Inventory",
            resizable: true,
            dockable: false,
            isFixedDock: false,
            title: "Inventory",
            content: "Content goes here",
            style: "position:absolute; top:70%;left:70%; width:10%; height:10%; background-color:#ff000000; visibility:hidden;"
        }, dojo.byId("inventory"));
        
        // dojo.connect(invWindow, "ondragstart",   dojo, "stopEvent");
        // dojo.connect(invWindow, "onselectstart", dojo, "stopEvent");
        
        invWindow.startup();
        
        d(invWindow + " should be showing");
    }
    
    if(!invWindow._isShown()) {
        invWindow.show();
    }
    else {
        invWindow.hide();
    }
}

function toggleTargetsList() {
    if(!targetListWindow) {
        targetListWindow = new MMOFloatingPane({ 
                id: "TargetList",
                resizable: true,
                dockable: false,
                isFixedDock: false,
                title: "Targets",
                style: "position:absolute; top:30%; left:90%; width:120px; height:400px; background-color:#ff000000"
            }, dojo.byId("targets"));
        
        var scroll = new dojox.layout.ScrollPane({
                orientation: "vertical",
                style: "width:100%; height:100px; background-color:#ff000000"
            });
        
        var targetTable = new dojox.layout.TableContainer({
            cols: 1,
            customClass: "labelsAndValues",
            showLabels: false,
            style: "padding-left:10px; padding-right:10px; padding-top:200px; padding-bottom:200px"
        });
        
        d(gameObjectsMap);
        Object.keys(gameObjectsMap).forEach(function(target) {
                var targetPane = new TargetPane({
                        target: target,
                        content: target,
                        // content: target + "<br /><progress value=\"70\" max=\"100\">70%</progress>",
                        style: "border:1px solid black; height: 80px"
                    });
            
                targetTable.addChild(targetPane);
                
                dojo.connect(targetPane, "onClick", targetPane, function() {
                        if(gameObjectsMap[this.target]) {
                            toggleTarget(gameObjectsMap[this.target]);
                        }
                    });
                dojo.connect(targetPane, "onMouseEnter", targetPane, function() {
                        if(gameObjectsMap[this.target]) {
                            highlightOn(gameObjectsMap[this.target]);
                        }
                    });
                dojo.connect(targetPane, "onMouseLeave", targetPane, function() {
                        if(gameObjectsMap[this.target]) {
                            highlightOff(gameObjectsMap[this.target]);
                        }
                    });
            });        
        
        scroll.set('content', targetTable);        
        
        targetListWindow.startup();
        targetListWindow.set('content', scroll);
    }
    
    if(!targetListWindow._isShown()) {
        targetListWindow.show();
    }
    else {
        targetListWindow.hide();
    }
}

var targets = {};
function toggleTarget(unit) {
    if(isTargeted(unit)) {
        targetOff(unit);
    }
    else {
        targetOn(unit);
    }
}
function isTargeted(unit) {
    return !!targets[unit.getGUID()];
}
function targetOff(unit) {
    if(isTargeted(unit)) {
        targets[unit.getGUID()].removeShape();
        delete targets[unit.getGUID()];
    }
}
function targetOn(unit) {
    targets[unit.getGUID()] = unit.getShape().createImage({
            x: -16,
            y: -16,
            width: 32,
            height: 32,
            src: imgServletURL + "target.svg"
            //src: imgServletURL + "target.png"
        });
    targets[unit.getGUID()].moveToFront();
}

var highlights = {};
function toggleHighlight(unit) {
    if(isHighlighted(unit)) {
        highlightOff(unit);
    }
    else {
        highlightOn(unit);
    }
}
function isHighlighted(unit) {
    return !!highlights[unit.getGUID()];
}
function highlightOff(unit) {
    if(isHighlighted(unit)) {
        highlights[unit.getGUID()].removeShape();
        delete highlights[unit.getGUID()];
    }
}
function highlightOn(unit) {
    highlights[unit.getGUID()] = unit.getShape().createCircle({
            cx: 0, 
            cy: 0,
            r: 16
        })
        .setFill([100, 100, 100, 0.1])
        .setStroke({color: "black", width: 1});
    // highlights[unit.getGUID()].moveToBack();
    highlights[unit.getGUID()].moveToFront();
}
