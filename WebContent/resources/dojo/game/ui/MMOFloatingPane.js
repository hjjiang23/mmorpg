dojo.provide("game.ui.MMOFloatingPane");

dojo.require("dojox.layout.FloatingPane");


dojo.declare("MMOFloatingPane", dojox.layout.FloatingPane, {
        duration: 0,
        resize: function(/* Object */dim){
            // summary: Size the FloatingPane and place accordingly
            dim = dim || this._naturalState;
            this._currentState = dim;
    
            // From the ResizeHandle we only get width and height information
            var dns = this.domNode.style;
            if("t" in dim){ dns.top = dim.t + "px"; }
            if("l" in dim){ dns.left = dim.l + "px"; }
            dns.width = dim.w + "px";
            dns.height = dim.h + "px";
    
            // Now resize canvas
            var mbCanvas = { l: 0, t: 0, w: dim.w, h: (dim.h - this.focusNode.offsetHeight) };
            dojo.marginBox(this.canvas, mbCanvas);
    
            // If the single child can resize, forward resize event to it so it can
            // fit itself properly into the content area
            this._checkIfSingleChild();
            if(this._singleChild && this._singleChild.resize){
                this._singleChild.resize(mbCanvas);
            }
        },
        // Combination of FloatingPane startup and _ContentPaneResizeMixin's startup functions, without onresize listener
        startup: function(){
            if(this._started){ return; }
                        
            var parent = dijit._Contained.prototype.getParent.call(this);
            this._childOfLayoutWidget = parent && parent.isLayoutContainer;

            // I need to call resize() on my child/children (when I become visible), unless
            // I'm the child of a layout widget in which case my parent will call resize() on me and I'll do it then.
            this._needLayout = !this._childOfLayoutWidget;

            this._startChildren();

            if(this._isShown()){
                this._onShow();
            }

            // asw12: this is problematic:
            /*if(!this._childOfLayoutWidget){
                // If my parent isn't a layout container, since my style *may be* width=height=100%
                // or something similar (either set directly or via a CSS class),
                // monitor when my size changes so that I can re-layout.
                // For browsers where I can't directly monitor when my size changes,
                // monitor when the viewport changes size, which *may* indicate a size change for me.
                this.connect(dojo.isIE ? this.domNode : dojo.global, 'onresize', function(){
                    // Using function(){} closure to ensure no arguments to resize.
                    this._needLayout = !this._childOfLayoutWidget;
                    this.resize();
                });
            }*/

            if(this.resizable){
                if(dojo.isIE){
                    this.canvas.style.overflow = "auto";
                }else{
                    this.containerNode.style.overflow = "auto";
                }
                
                this._resizeHandle = new dojox.layout.ResizeHandle({
                    targetId: this.id,
                    resizeAxis: this.resizeAxis
                },this.resizeHandle);

            }

            if(this.dockable){
                // FIXME: argh.
                var tmpName = this.dockTo;

                if(this.dockTo){
                    this.dockTo = dijit.byId(this.dockTo);
                }else{
                    this.dockTo = dijit.byId('dojoxGlobalFloatingDock');
                }

                if(!this.dockTo){
                    var tmpId, tmpNode;
                    // we need to make our dock node, and position it against
                    // .dojoxDockDefault .. this is a lot. either dockto="node"
                    // and fail if node doesn't exist or make the global one
                    // once, and use it on empty OR invalid dockTo="" node?
                    if(tmpName){
                        tmpId = tmpName;
                        tmpNode = dojo.byId(tmpName);
                    }else{
                        tmpNode = dojo.create('div', null, dojo.body());
                        dojo.addClass(tmpNode,"dojoxFloatingDockDefault");
                        tmpId = 'dojoxGlobalFloatingDock';
                    }
                    this.dockTo = new dojox.layout.Dock({ id: tmpId, autoPosition: "south" }, tmpNode);
                    this.dockTo.startup();
                }
                
                if((this.domNode.style.display == "none")||(this.domNode.style.visibility == "hidden")){
                    // If the FP is created dockable and non-visible, start up docked.
                    this.minimize();
                }
            }
            this.connect(this.focusNode,"onmousedown","bringToTop");
            this.connect(this.domNode,  "onmousedown","bringToTop");

            // Initial resize to give child the opportunity to lay itself out
            this.resize(dojo.coords(this.domNode));
            
            this._started = true;
        },
        show: function(/* Function? */callback){
            // summary: Show the FloatingPane
            dojo.fadeIn({node:this.domNode, duration:this.duration,
                beforeBegin: dojo.hitch(this,function(){
                    this.domNode.style.display = "";
                    this.domNode.style.visibility = "visible";
                    if (this.dockTo && this.dockable) { this.dockTo._positionDock(null); }
                    if (typeof callback == "function") { callback(); }
                    this._isDocked = false;
                    if (this._dockNode) {
                        this._dockNode.destroy();
                        this._dockNode = null;
                    }
                })
            }).play();
            // this.resize(dojo.coords(this.domNode));
            this._onShow(); // lazy load trigger
        },
        close: function(){
            // Never destroy
            // summary: Close and destroy this widget
            /*if(!this.closable){ return; }
            dojo.unsubscribe(this._listener);
            this.hide(dojo.hitch(this,function(){
                this.destroyRecursive();
            }));*/
            this.hide();
        }
    });
