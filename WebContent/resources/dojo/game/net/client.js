dojo.provide("game.net.client");

dojo.require("game.common");
dojo.require("game.graphics.graphics");
dojo.require("game.world.worldobject");
dojo.require("game.world.objectguid");
dojo.require("game.world.template");
dojo.require("game.world.terrain");
dojo.require("game.world.unit");
dojo.require("game.world.creature");
dojo.require("game.world.player");
dojo.require("game.net.chat");
dojo.require("game.net.opcode");
dojo.require("game.util.bytebuffer");

dojo.require("dojo.io.iframe");
dojo.require("dojox.encoding.digests.SHA1");

var useCookies = false;
var ajaxServletURL = "ajax.jsp"
		+ (useCookies ? "" : ";jsessionid=" + JSESSIONID);
var imgServletURL = "../resources/images/";

var moveScale = 20; // TODO: get this from mover units

var gameObjectsMap = {}; // maps guids to gameobjects
var creTemplatesMap = {}; // maps template ids to templates
var terrainMap = {}; // maps template ids to templates

var playerGUID = null;
var moverGUID = null;
var centerCameraOnMover = false;

var packetID = 0;

function isCameraCentered() {
	return centerCameraOnMover;
}
function setCameraCentered(b) {
	centerCameraOnMover = b;
	centerCameraOnSelf();
}

function addByteBufHeader(buf, op) {
	buf.putByte(op);
	buf.putLongerInt(packetID++);
}

function sendMove(matrix, noScale) {
	// Prevent this scaling factor if click moving and adjacent to target loc
	if (!noScale) {
		for ( var x in matrix) {
			matrix[x] *= moveScale;
		}
	}

	var loc = getMoverLocation();
	var targetX = loc.x + matrix.dx;
	var targetY = loc.y + matrix.dy;

	var screenX = targetX + getEnclosingGroup().matrix.dx;
	var screenY = targetY + getEnclosingGroup().matrix.dy;

	var terrainPassable = true;

	// getSurface().rawNode.getIntersectionList(hitBox, getSurface().rawNode);
	// // ideal method, NYI in webkit/FF

	if (getDebugMode()) {
		// asw12: client side collision detection code
		/*
		 * var hitBox = getSurface().rawNode.createSVGRect(); hitBox.x = screenX -
		 * 0.5; hitBox.y = screenY - 0.5; hitBox.width = 1; hitBox.height = 1;
		 * 
		 * d("--------"); d(terrainMap[1].getShape().getNode());
		 * Object.keys(terrainMap[1].getShape().getNode()).forEach(function(key) {
		 * d(key); }); d("--------");
		 * 
		 * Object.keys(terrainMap).forEach(function(tid) { //
		 * d(terrainMap[tid].getShape().getNode());
		 * d(getSurface().rawNode.checkIntersection(terrainMap[tid].getShape().getNode(),
		 * hitBox)); });
		 */

		if (window.testDestinationShape) {
			getEnclosingGroup().remove(window.testDestinationShape);
		}
		window.testDestinationShape = getEnclosingGroup().createCircle({
			cx : targetX,
			cy : targetY,
			r : 5
		}).setFill([ 255, 0, 0 ]).setStroke({
			color : "black",
			width : 2
		}).moveToBack();
	}

	if (terrainPassable) {
		var buf = new ByteBuffer({
			capacity : 21
		}); // 1 + 8 + 4 + 4 + 4
		addByteBufHeader(buf, OP.cMOVE);
		buf.putFloat(targetX);
		buf.putFloat(targetY);
		buf.putFloat(Math.atan2(matrix.dx, matrix.dy) % (Math.PI * 2));
		sendPacket(buf);

		animateSelf(targetX, targetY);
	} else {
		e("Not passable");
	}
}

function animateSelf(targetX, targetY) {
	var mover = getMover();
	mover.animateTo(targetX, targetY);

	if (isCameraCentered()) {
		var loc = getCenteredCameraCoords(mover.getLocation());
		dojox.gfx.fx.animateTransform(
				{
					shape : getEnclosingGroup(),
					easing : dojo.fx.easing.linear,
					transform : [
							{
								name : 'translate',
								start : [ getEnclosingGroup().matrix.dx,
										getEnclosingGroup().matrix.dy ],
								end : [ loc.x, loc.y ]
							}, ],
					duration : moveFireRate,
				}).play();
	}
}
function getMover()
{
	return gameObjectsMap[moverGUID];
}
function getMoverLocation() {
	var mover = null;
	if (!moverGUID) {
		return null;
	} else {
		mover = getMover();
		if (mover == undefined) {
			e("Unit does not exist in gameObjectsMap");
			return null;
		}
	}
	return mover.getLocation();
}

function sendRequestPackets(num) {
	if (!num) {
		num = 1;
	} else if (num >= 5) {
		e("sendRequestPackets: "
				+ num
				+ " likely to exceed maxConnectionsPerServer setting of browser!");
	}

	for ( var i = 0; i < num; ++i) {
		var buf = new ByteBuffer({
			capacity : 9
		}); // 1 + 8
		addByteBufHeader(buf, OP.cREQUESTPACKETS);
		sendPacket(buf);
	}
}

function sendLogin(account, password) {
	var hash = dojox.encoding.digests.SHA1(password,
			dojox.encoding.digests.outputTypes.Hex);
	var buf = new ByteBuffer({
		capacity : 13 + 2 * (account.length + hash.length)
	}); // 1 + 8 + string lengths + 2 null terminators
	addByteBufHeader(buf, OP.cLOGIN);
	d(hash);
	buf.putString(account);
	buf.putString(hash);
	sendPacket(buf);
}

function sendLogout() {
	// a("logging out");
	var buf = new ByteBuffer({
		capacity : 9
	});
	addByteBufHeader(buf, OP.cLOGOUT);
	sendBytesAsBinary(buf, function() {
		d("success");
	}, function() {
	}, true);
}

function sendMeleeAttack() {
	var buf = new ByteBuffer({
		capacity : 9
	}); // 1 + 8
	addByteBufHeader(buf, OP.cMELEEATTACK);
	sendPacket(buf);
}

function sendBrowserTest() {
	var buf = new ByteBuffer({
		capacity : 265
	});
	addByteBufHeader(buf, OP.cBROWSERTEST);
	for ( var i = 0; i < 256; ++i) {
		buf.putByte(i);
	}

	var loadCallback = function(data) {
		var success = 1;
		for ( var i = 0; i < 256; ++i) {
			var b = data.array()[i];
			if (b != i) {
				e("BrowserTest: expected " + i + ", received " + b);
				success = 0;
			}
		}

		if (success) {
			dojo.create("div", {
				innerHTML : "Messaging test success"
			}, dojo.body(), "last");
		} else {
			dojo.create("div", {
				innerHTML : "Messaging test failed"
			}, dojo.body(), "last");
		}
	};
	sendPacket(buf, loadCallback);
	d("BrowserTest packet sent");
}

function sendPacket(buf, loadCallback, errorCallback) {
	if (loadCallback == null) {
		loadCallback = handleResponse;
	}
	if (errorCallback == null) {
		errorCallback = function(data) {
			// d(data);
		};
	}

	sendBytesAsBinary(buf, loadCallback, errorCallback);
}
function doBinaryStreamTest() {
	var loadCallback = function(data) {
		d(data);
	};
	var errorCallback = null;

	if (loadCallback == null) {
		loadCallback = handleResponse;
	}
	if (errorCallback == null) {
		errorCallback = function(error) {
			// d(error);
		};
	}

	var buf = new ByteBuffer({
		capacity : 9
	});
	addByteBufHeader(buf, OP.cOPENSTREAM);
	openBinaryStream(buf, loadCallback, errorCallback);
}

function handleResponse(data) {
	// Return if empty
	if (!data.getCapacity())
		return;

	var count = 0; // The number of packets included in this response
	while (data.hasMore()) {
		++count;
		var opcode = data.get();
		// d(Object.keys(OP)[opcode]);
		if (OPHandler[opcode] != undefined) {
			OPHandler[opcode](data);
		} else {
			d("Unhandled opcode: " + opcode);
			return;
		}
	}
	// d(count);
}

function handleRequestPackets(buf) {
	sendRequestPackets(buf.get());
}

/**
 * Initializes a unit via its guid
 * 
 * @param buf -
 *            ByteBuffer that contains the data sent from the server
 */
function handleInitUnit(buf) {
	var guid = buf.getInt();
	var x = buf.getFloat();
	var y = buf.getFloat();
	var orientation = buf.getFloat();
	var name = buf.getString();
	var currHP = buf.getInt();
	var tid = 0;

	var unit;
	if (gameObjectsMap[guid]) {
		gameObjectsMap[guid].removeShape();
	}
	if (isCreatureGUID(guid)) {
	    tid = buf.getInt();
        unit = new Creature({
                GUID : guid,
                tid : tid,
                _currHP : currHP
            });
		if (creTemplatesMap[tid] && creTemplatesMap[tid].isInitialized()) {
			creTemplatesMap[tid].applyTo(unit);
		} else {
			if (!creTemplatesMap[tid]) {
				sendReqCreTemplate(tid);
				creTemplatesMap[tid] = new Template();
			}
			creTemplatesMap[tid].addListener(unit);
		}
	}
	if (isPlayerGUID(guid)) {
	    unit = new Player({
                GUID : guid
            });
		var shape = getEnclosingGroup().createGroup();
		shape.createCircle({
			cx : 0,
			cy : 0,
			r : 10
		}).setFill(randColor(true)).setStroke({
			color : randColor(true),
			width : 2
		});
		shape.createCircle({
			cx : 0,
			cy : 0,
			r : 2
		}).setFill(randColor(true)).setStroke({
			color : randColor(true),
			width : 1
		});
		shape.createCircle({
			cx : 0,
			cy : -10,
			r : 2
		}).setFill(randColor(true)).setStroke({
			color : randColor(true),
			width : 1
		});
		shape.moveToFront();
		unit.setShape(shape);
	}

	if (!unit.getShape()) {
		var shape = getEnclosingGroup().createGroup();
		shape.createCircle({
			cx : 0,
			cy : 0,
			r : 10
		}).setFill(randColor(true)).setStroke({
			color : randColor(true),
			width : 2
		});
		shape.createCircle({
			cx : 0,
			cy : 0,
			r : 2
		}).setFill(randColor(true)).setStroke({
			color : randColor(true),
			width : 1
		});
		shape.moveToFront();
		unit.setShape(shape);
	}
	unit.setName(name);
	unit.setLocation(x, y, orientation);
	unit.setCurrHP(currHP); // HP must be set after location; displaying of HPBar depends on loc info
	gameObjectsMap[guid] = unit;

	// Center camera if this is self
	if (moverGUID == guid) {
		centerCameraOnSelf();
	}

	sendRequestPackets(1);
}
function handleDestroyUnit(buf) {
	d("received destroy unit");
	var guid = buf.getInt();
	var unit = gameObjectsMap[guid];
	if (!unit) {
		return;
	}

	unit.removeShape();
	delete gameObjectsMap[guid];

	sendRequestPackets(1);
}

function handleSetMover(buf) {
	moverGUID = buf.getInt();

	// TODO: separate this into a different type of message?
	// If playerGUID is null, set this to the first moverGUID received. This
	// will always be the handleSetMover received when you first log in.
	if (!playerGUID) {
		playerGUID = moverGUID;
	}

	if (getMover()) {
		centerCameraOnSelf();
	}
	d("handleSetMover: set mover to " + moverGUID);
	
	// Remove login UI here for now
	dojo.destroy("loginContainer");
}

function sendReqCreTemplate(tid) {
	var buf = new ByteBuffer({
		capacity : 13
	}); // 1 + 8 + 4
	addByteBufHeader(buf, OP.cREQCRETEMPLATE);
	buf.putInt(tid);
	sendPacket(buf);
}
function handleInitCreTemplate(buf) {
	var tid = buf.getInt();
	var spriteURL = "unit/" + buf.getString();
	var maxHP = buf.getInt();
    var maxMana = buf.getInt();
	if (!creTemplatesMap[tid]) {
		e("handleInitCreTemplate: server pushing template");
		creTemplatesMap[tid] = new Template();
	}
	creTemplatesMap[tid].initialize(tid, spriteURL, maxHP, maxMana);
}

function handlePreloadImage(buf) {
	d(buf.getString());
}

function centerCameraOnSelf() {
	if (isCameraCentered()) {
		var loc = getCenteredCameraCoords(getMover()
				.getLocation());
		getEnclosingGroup().setTransform({
			dx : loc.x,
			dy : loc.y
		});
	}
}
// Gets the coordinates necessary for the enclosing group in order to have loc
// be the center of the window
function getCenteredCameraCoords(loc) {
	var viewPortCenter = getSurfaceCenter();
	return {
		x : -loc.x + viewPortCenter.x,
		y : -loc.y + viewPortCenter.y
	};
}

/*
 * A notification from the server that a Unit has moved. The server may choose
 * not to send this notification when this client's mover moves, unless the
 * server is correcting the client's position.
 */
function handleMove(buf) {
	// For unit updates, should pull all data out of the buffer first
	var guid = buf.getInt();
	var x = buf.getFloat();
	var y = buf.getFloat();
	var orientation = buf.getFloat();
	// Need to add to cometqueue if it was not my mover that moved (do this
	// first, in case an error is returned later)
	if (guid != moverGUID) {
		sendRequestPackets(1);
	}

	if (gameObjectsMap[guid] == undefined) {
		// Safe to not comment on this error (initUnit can be expected soon)
		// e("handleMove: received move update for unknown unit. Buf pos: " +
		// (buf.pos - 3) + " Contents: " + buf.array());
		return;
	}

	if (!(moverGUID == guid && isCameraCentered())) {
		gameObjectsMap[guid].animateTo(x, y, orientation);
	} else {
		animateSelf(x, y);
	}
}

function handleSetCurrHP(buf) {
	var guid = buf.getInt();
	var currHP = buf.getInt();

	if (gameObjectsMap[guid] == undefined) {
		return;
	}

	gameObjectsMap[guid].setCurrHP(currHP);
	d(guid + " health now " + currHP);
}
function handleSetMaxHP(buf) {
    var guid = buf.getInt();
    var currHP = buf.getInt();

    if (gameObjectsMap[guid] == undefined) {
        return;
    }

    gameObjectsMap[guid].setCurrHP(currHP);
    d(guid + " health now " + currHP);
}

function handleInitTerrain(buf) {
	var tid = buf.getInt();
	var pathStr = buf.getString();
	var textureStr = buf.getString();
	var passable = buf.get();

	terrainMap[tid] = new Terrain({
		ID : tid,
		pathStr : pathStr,
		textureURL : textureStr,
		passable : passable
	});
}
function handleDestroyTerrain(buf) {
	// getEnclosingGroup().remove(terrainMap[tid].getShape());
	// delete terrainMap[tid];
}

function nyiPacket(buf) {
	e("NYI packet received " + buf.array());
}
function invalidPacket(buf) {
	e("Invalid packet received " + buf.array());
}

function handleChat(buf) {
	var guid = buf.getInt();
	var message = buf.getString();
	receiveChatMessage(guid, message);
}

function getNameForGUID(guid) {
	return gameObjectsMap[guid].getName();
}

sendBytesAsBinary = function() {
	if (typeof XMLHttpRequest.prototype.sendAsBinary == "function") {
		return function(byteBuffer, loadCallback, errorCallback, sync) {
			var req = new XMLHttpRequest();
			req.async = !sync;
			req.open("POST", ajaxServletURL, true);
			req.overrideMimeType('text/plain; charset=x-user-defined');
			req.onreadystatechange = function() {
				if (this.readyState == 4) {
					var responseBuf = new ByteBuffer({
						capacity : req.responseText.length
					});
					responseBuf.putByteString(req.responseText);
					responseBuf.reset();
					if (this.status == 200) { // OK
						loadCallback(responseBuf);
					} else {
						errorCallback(responseBuf);
					}
				}
			};
			req.sendAsBinary(byteArrayToStr(byteBuffer.array()));
		};
	} else if (window.BlobBuilder || window.WebKitBlobBuilder) {
		return function(byteBuffer, loadCallback, errorCallback, sync) {
			var byteArray = new Uint8Array(byteBuffer.array().length);
			byteArray.set(byteBuffer.array());
			var req = new XMLHttpRequest();
			req.async = !sync;
			req.open("POST", ajaxServletURL, true);
			req.overrideMimeType('text/plain; charset=x-user-defined');
			req.onreadystatechange = function() {
				if (this.readyState == 4) {
					var responseBuf = new ByteBuffer({
						capacity : req.responseText.length
					});
					responseBuf.putByteString(req.responseText);
					responseBuf.reset();
					if (this.status == 200) { // OK
						loadCallback(responseBuf);
					} else {
						errorCallback(responseBuf);
					}
				}
			};
			req.send(byteArray.buffer);
		};
	} else if (navigator.appName == 'Microsoft Internet Explorer') {
		d("Using IE");
		return function(byteBuffer, loadCallback, errorCallback, sync) {
			var rs = new ActiveXObject("ADODB.RecordSet");
			rs.Fields.Append("a", 205, byteBuffer.array().length);
			rs.open();
			rs.addNew();
			rs("a").appendChunk(byteArrayToStr(byteBuffer.array()));

			var req = new ActiveXObject("Msxml2.XMLHTTP.3.0");
			// req.async = !sync;
			req.open("POST", ajaxServletURL, true);
			req.onreadystatechange = function() {
				if (req.readyState == 4) {
					var responseBuf = new ByteBuffer({
						capacity : req.responseText.length
					});
					responseBuf.putByteArray(req.responseText);
					d("Response buf " + responseBuf.array());
					responseBuf.reset();
					if (req.status == 200) { // OK
						loadCallback(responseBuf);
					} else {
						errorCallback(responseBuf);
					}
				}
				d("Ready state " + req.readyState);
			};
			req.send(rs("a").value);
		};
	} else if (window.opera) {
		d("Opera is not currently supported");
		return function(byteBuffer, loadCallback, errorCallback, sync) {
			var req = new XMLHttpRequest();
			req.async = !sync;
			req.open("POST", ajaxServletURL, true);
			req.setRequestHeader("Content-Type", "charset=x-user-defined");
			req.overrideMimeType("charset=x-user-defined");
			req.onreadystatechange = function() {
				var responseBuf = new ByteBuffer({
					capacity : req.responseText.length
				});
				responseBuf.putByteArray(req.responseBody.toArray());
				responseBuf.reset();
				if (this.readyState == 4) {
					if (this.status == 200) { // OK
						loadCallback(responseBuf);
					} else {
						errorCallback(responseBuf);
					}
				}
			};
			req.send(byteArrayToStr(byteBuffer.array()));
		};
	} else {
		e("Unidentified browser");
	}
}();

openBinaryStream = function() {
	if (typeof XMLHttpRequest.prototype.sendAsBinary == "function") {
		return function(byteBuffer, loadCallback, errorCallback) {
			var req = new XMLHttpRequest();
			req.multipart = true;
			req.open("POST", ajaxServletURL, true);
			// req.setRequestHeader("Content-Type",
			// "multipart/x-mixed-replace");
			// req.setRequestHeader("Content-Type", "text/xml");
			// req.setRequestHeader("Content-Length", "1000");
			req.overrideMimeType('text/plain; charset=x-user-defined');
			req.onreadystatechange = function() {
				if (this.readyState == 4) {
					d("done");
					d(req.responseText);
				}
			};
			req.sendAsBinary(byteArrayToStr(byteBuffer.array()));
		};
	} else {
		return function() {
			e("This operation is not supported in your browser");
		};
	}
}();
