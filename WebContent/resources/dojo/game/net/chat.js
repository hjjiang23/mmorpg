dojo.provide("game.net.chat");

var chatTimeout;
function hideChat()
{
	d("hiding chat");
	$('#inputBox').fadeTo('fast',0.2,function(){
	    chatTimeout = setTimeout(function()
	    {
	    	$('#messageContainer').fadeTo('slow',0);
	    },2000);
	});
	
}
var showing=false;
function showChat()
{
	clearTimeout(chatTimeout);
	chatTimeout = undefined;
	d("showing chat");
	
	
	$('#inputBox').fadeTo('fast',1,function(){
		$('#messageContainer').fadeTo('slow',0.5);
	});
}

function sendMessage()
{
    var message = $('#inputBox').val();
    if(message.length != 0) {
        $('#inputBox').val('');
        sendChat(message);
        displayMessage(playerGUID, message);
    }
}

function sendChat(message)
{
    var buf = new ByteBuffer({capacity: 11 + 2 * (message.length)}); // 1 + 8 + 2 * (string lengths + null terminator)
    addByteBufHeader(buf, OP.cCHAT);
    buf.putString(message);
    sendPacket(buf);
}

function receiveChatMessage(guid, message)
{
    //if the message was not a system command and should be displayed
    if (processMessage(message))
    {
        displayMessage(guid, message);
    }
}



//update the div with the new message
function displayMessage(guid, message)
{
    d("Displaying " + message + " from player " + guid);
    // TODO: add names of person speaking to chat log
    $('#messageBox').append('<p>' + getNameForGUID(guid) + ':' + message + '</p>');
    $('#messageContainer').animate({
        scrollTop: $("#messageContainer").attr("scrollHeight")
    }, 100);
}

function processMessage(message) {
	return true;
}