dojo.provide("game.net.opcode");

dojo.require("game.common");

// All opcodes prefixed with 'c' are sent from client to server
// ""                        's' are sent from server to client 
var OP = {
        cGETPUBLICKEY : 0,
        sREQUESTPACKETS : 1,
        cREQUESTPACKETS : 2,
        cOPENSTREAM : 3,
        cLOGIN : 4,
        cCHARCHOICE : 5,
        sCHARCHOICE : 6,
        cLOGOUT : 7,
        sMOVE : 8,
        cMOVE : 9,
        sINITUNIT : 10,
        sDESTROYUNIT : 11,
        cSENDMSG : 12,
        cBROWSERTEST : 13,
        sBROWSERTEST : 14,
        sSETMOVER : 15,
        sPRELOADIMAGE : 16,
        cREQCRETEMPLATE : 17,
        sINITCRETEMPLATE : 18,
        sINITTERRAIN : 19,
        sDESTROYTERRAIN : 20,
        cCHAT: 21,
        sCHAT: 22,
        sSETCURRHP: 23,
        sSETMAXHP: 24,
        cMELEEATTACK: 25,
        sMELEEATTACK: 26
    };

// The functions that handle each operation. OPs that are prefixed 'c' should not be received by the client, and are sent to invalidPacket. 
var OPHandler = [
        invalidPacket,
        handleRequestPackets,           // REQUESTPACKETS
        invalidPacket,
        invalidPacket,
        invalidPacket,
        invalidPacket,
        nyiPacket,                      // CHARCHOICE
        invalidPacket,
        handleMove,                     // MOVE
        invalidPacket,
        handleInitUnit,                 // INITUNIT
        handleDestroyUnit,              // DESTROYUNIT
        invalidPacket,
        invalidPacket,
        invalidPacket,                  // BROWSERTEST
        handleSetMover,                 // SETMOVER
        handlePreloadImage,             // PRELOADIMAGE
        invalidPacket,
        handleInitCreTemplate,          // INITCRETEMPLATE
        handleInitTerrain,              // INITTERRAIN
        handleDestroyTerrain,           // DESTROYTERRAIN
        invalidPacket,
        handleChat,                     // CHAT
        handleSetCurrHP,                // SETCURRHP
        handleSetMaxHP,                 // SETMAXHP
        invalidPacket,
        nyiPacket,
     ];
