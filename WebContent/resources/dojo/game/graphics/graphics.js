dojo.provide("game.graphics.graphics");

dojo.require("dojo.window");
dojo.require("dojox.gfx");
dojo.require("dojox.gfx.move");
dojo.require("dojox.image");
dojo.require("dojo.colors");

dojo.require("game.common");
dojo.require("game.util.util");


var ready = false;
var context;

var container = null;
var surface = null;
var surface_group = null;
var surface_size = null;
var surface_center = null;
 
var skew_stat_factor = 15;
 
function getRandSkewed(from, to){
    // let skew stats to smaller values
    var seed = 0;
    for(var i = 0; i < skew_stat_factor; ++i){
        seed += Math.random();
    }
    seed = 2 * Math.abs(seed / skew_stat_factor - 0.5);
    return seed * (to - from) + from;
}
 
function randColor(alpha){
    var red   = Math.floor(getRand(0, 50)),
        green = Math.floor(getRand(0, 50)),
        blue  = Math.floor(getRand(0, 50)),
        opacity = alpha ? getRand(0.1, 1) : 1;
    return [red, green, blue, opacity];
}
 
function initGfx(){
    container = dojo.byId("gfx_holder");

    // cancel text selection and text dragging
    dojo.connect(container, "ondragstart",   dojo, "stopEvent");
    dojo.connect(container, "onselectstart", dojo, "stopEvent");
    
    var vs = dojo.window.getBox();
    setSurfaceSize(vs.w - vs.l - 20, vs.h - vs.t - 20);
    surface = dojox.gfx.createSurface(container, surface_size.width, surface_size.height);
    surface_group = surface.createGroup();
    surface_group.setTransform({
            dx: 0,
            dy: 0
        });
}

function getShapeLocation(shape) {
    return { 
            x: (shape.matrix ? shape.matrix.dx : 0) + shape.shape.cx + 
                (shape.shape.r ? shape.shape.r : 0),
            y: (shape.matrix ? shape.matrix.dy : 0) + shape.shape.cy +
                (shape.shape.r ? shape.shape.r : 0)
        };
}

function getSurface() {
    return surface;
}
function getEnclosingGroup() {
    return surface_group;
}
function getSurfaceSize() {
    return surface_size;
}
function getSurfaceCenter() {
    return surface_center;
}

function setSurfaceSize(width, height) {
    surface_size = {
            width: width,
            height: height
        };
    surface_center = {x: width / 2, y: height / 2};
}

function onResize(event) {
    var vs = dojo.window.getBox();
    var oldCenter = getSurfaceCenter();
    setSurfaceSize(vs.w - vs.l - 20, vs.h - vs.t - 20);
    surface.setDimensions(surface_size.width-$('#messageContainer').width(), surface_size.height);
    var newCenter = getSurfaceCenter();
    getEnclosingGroup().applyTransform({
            dx: newCenter.x - oldCenter.x,
            dy: newCenter.y - oldCenter.y
        });
    d("onResize to " + surface_size.width + " " + surface_size.height);
}

