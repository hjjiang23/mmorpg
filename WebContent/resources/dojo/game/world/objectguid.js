dojo.provide("game.world.objectguid");

dojo.require("game.common");


function getHeader(guid) {
    return (guid & 0xF0000000) >> 28;
}
function getId(guid) {
    return guid & 0x0FFFFFFF;
}

function isCreatureGUID(guid) {
    return getHeader(guid) == GUIDTYPE.CREATURE;
}

function isPlayerGUID(guid) {
    return getHeader(guid) == GUIDTYPE.PLAYER;
}


var GUIDTYPE = {
        GAMEOBJECT : 0,
        WORLDOBJECT : 1,
        UNIT : 2,
        PLAYER : 3,
        CREATURE : 4
    };
