dojo.provide("game.world.unit");

dojo.require("game.common");
dojo.require("game.ui.ui");

dojo.require("dojo.fx.easing");
dojo.require("dojox.gfx");
dojo.require("dojox.gfx.fx");
dojo.require("dojox.gfx.matrix");

dojo.declare("Unit", null, {
	GUID : 0, // this belongs in super
	shape : null,
	_pos_x : 0,
	_pos_y : 0,
	_o : 0,
	_name : "",
	_currHP : 10,
	_maxHP : 10,
	hpBar : null,
	hpBarFilled : null,
	_swordShape: null,

	constructor : function(args) {
		dojo.safeMixin(this, args);
		this.hpBar = getEnclosingGroup().createGroup();
		this.hpBar.createRect({
			x : 0,
			y : 0,
			width : 30,
			height : 5
		}).setFill("red").setStroke("red");
		this.hpBar.moveToFront();

		this.hpBarFilled = getEnclosingGroup().createGroup();
		this.hpBarFilled.createRect({
			x : 0,
			y : 0,
			width : 30,
			height : 5
		}).setFill("green").setStroke("green");
		this.hpBarFilled.moveToFront();
		
	
	},
	getGUID : function() {
		return this.GUID;
	},
	setName : function(name) {
		this._name = name;
	},
	getName : function() {
		return this._name;
	},
	setShape : function(shape) {
		this.shape = shape;
		// this.shape.getNode().onclick = function(event) { d("click should be
		// captured"); dojo.stopEvent(event); };
		dojo.connect(this.shape.getNode(), "onmousedown", this,
				function(event) {
					toggleTarget(this);
					dojo.stopEvent(event);
				});
		// dojo.connect(this.shape.getNode(), "onmouseup", null, function(event)
		// { d("mouse up should be captured"); dojo.stopEvent(event); });
	},
	getShape : function() {
		return this.shape;
	},
	removeShape : function() {
		dojo.disconnect(this.shape.getNode(), "onmousedown");
		this.hpBar.removeShape();
		this.hpBarFilled.removeShape();
		this.getShape().removeShape();
	},
	setLocation : function(x, y, o) {
		this._pos_x = x;
		this._pos_y = y;
		if (o) {
			this._o = o;
		}

		if (this.shape != null) {
			this.shape.setTransform([ {
				dx : this._pos_x,
				dy : this._pos_y
			}, dojox.gfx.matrix.rotate(this._o) ]);


            this.hpBar.setTransform([ {
                dx : this._pos_x - 15,
                dy : this._pos_y - 20
            } ]);
            this.hpBarFilled.setTransform([ {
                dx : this._pos_x - 15,
                dy : this._pos_y - 20
            } ]);
		} else {
			e("Unit: shape is null");
		}
	},
	setCurrHP : function(currHP) {
	    var oldPercentage = this.getHPPercentage(); // store percentage before change
		this._currHP = currHP;
		
		// refresh hpBarFilled
		var percentage = this.getHPPercentage();
		dojox.gfx.fx.animateTransform({
		    shape : this.hpBarFilled,
		    easing : dojo.fx.easing.linear,
		    transform : [ {
                    name : 'translate',
                    start : [ this.shape.matrix.dx-15, this.shape.matrix.dy - 20],
                    end : [ this.shape.matrix.dx-15, this.shape.matrix.dy - 20]
                }, {
                    name : 'scale',
                    start : [ oldPercentage, 1 ],
                    end : [ percentage, 1 ]
            } ],
		    duration : moveFireRate,
		}).play();
	},
	getCurrHP : function(currHP) {
		return this._currHP;
	},
	animateAttack : function()
	{
		this._swordShape = getEnclosingGroup().createGroup();
		this._swordShape.createRect({
			x:this.shape.matrix.dx,
			y: this.shape.matrix.dy-1,
			width:30,
			height:2
		}).setFill("gray").setStroke("black");
		this._swordShape.moveToFront();
		

		d(this.getOrientation()-Math.PI/2);
		
		// draw sword bar filled
		dojox.gfx.fx.animateTransform({
			shape : this._swordShape,
			easing : dojo.fx.easing.linear,
			transform : [ {
				name : 'rotateAt',
				start : [ this.getOrientation()+Math.PI*2, this._pos_x,this._pos_y-1 ],
				end : [ this.getOrientation()+ Math.PI, this._pos_x, this._pos_y -1 ]
			}],
			duration : moveFireRate*2,
		}).play();
		var swordShape = this._swordShape;
		setTimeout(function() {swordShape.removeShape();},moveFireRate*2+100);
//		
	},
	animateTo : function(x, y, o) {
		if (o) {
			this._o = o;
		}
		else
		{
			//Calculate Orientation
			this._o=Math.atan((y-this._pos_y)/(x-this._pos_x)) + Math.PI/2.0;
			if((x-this._pos_x)<0)
				this._o += Math.PI;
		}
		
		
		// Draw the character
		dojox.gfx.fx.animateTransform({
			shape : this.shape,
			easing : dojo.fx.easing.linear,
			transform : [ {
				name : "rotateAt",
				start : [ this.getOrientation(), this._pos_x, this._pos_y ],
				end : [ this.getOrientation(), x, y ]
			}, {
				name : 'translate',
				start : [ this.shape.matrix.dx, this.shape.matrix.dy ],
				end : [ x, y ]
			} ],
			duration : moveFireRate,
		}).play();

		// draw hp bar
		dojox.gfx.fx.animateTransform({
			shape : this.hpBar,
			easing : dojo.fx.easing.linear,
			transform : [ {
				name : 'translate',
				start : [ this.shape.matrix.dx-15, this.shape.matrix.dy-20 ],
				end : [ x-15, y-20 ]
			} ],
			duration : moveFireRate,
		}).play();
		
		var percentage = this.getHPPercentage();
		
		// draw hp bar filled
		dojox.gfx.fx.animateTransform({
			shape : this.hpBarFilled,
			easing : dojo.fx.easing.linear,
			transform : [ {
				name : 'translate',
				start : [ this.shape.matrix.dx-15, this.shape.matrix.dy -20],
				end : [ x-15, y-20 ]
			},
			{
				name : 'scale',
				start : [ percentage, 1 ],
				end : [ percentage, 1 ]
			} ],
			duration : moveFireRate,
		}).play();

		this._pos_x = x;
		this._pos_y = y;
	},
	getHPPercentage : function() {
	    return this._currHP / (this.getMaxHP() + 0.0);
	},
	getLocation : function() {
		return {
			x : this._pos_x,
			y : this._pos_y
		};
	},
	getOrientation : function() {
		return this._o;
	},
	getMaxHP : function() {
	    return this._maxHP;
	},
	getMaxMana : function() {
	    return 0;
    }
});
