dojo.provide("game.world.terrain");

dojo.require("game.common");


dojo.declare("Terrain", null, {
        ID: 0,
        shape: null,
        textureURL: "",
        passable: true,
        constructor: function(args) {
            dojo.safeMixin(this, args);
            
            
            this.shape = getEnclosingGroup().createPath({
                    path: args.pathStr
                });
            this.shape.closePath();
            
            if(this.getTexture() != "") {
                d("Loaded " + this.getTexture());
                this.shape.setFill({
                        type: "pattern",
                        width: 32,
                        height: 32,
                        src: imgServletURL + "terrain/" + this.getTexture()
                    });
            }
            else {
                this.shape.setFill([50, 150, 50, 0]);
                this.shape.setStroke({color: "black"});
            }
            this.shape.moveToBack();
            
            // Hack on underlying domNode to make collision detection easier
            this.shape.getNode().terrain = this;
        },
        getId: function() {
            return this.ID;
        },
        getShape: function() {
            return this.shape;
        },
        getTexture: function() {
            return this.textureURL;
        },
        isPassable: function() {
            return this.passable;
        }
    });
