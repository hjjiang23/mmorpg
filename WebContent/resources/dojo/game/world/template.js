dojo.provide("game.world.template");

dojo.require("game.common");


dojo.declare("Template", null, {
        ID: 0,
        listeners: null,
        constructor: function(args) {
            dojo.safeMixin(this, args);
            this.listeners = [];
        },
        getId: function() {
            return this.ID;
        },
        getShape: function() {
            return this.shape;
        },
        getSprite: function() {
            return this.spriteURL;
        },
        addListener: function(gameobject) {
            this.listeners.push(gameobject);
        },
        isInitialized: function() {
            return (this.ID != 0);
        },
        spriteURL: "",
        initialize: function(tid, spriteURL, maxHP, maxMana) {
            if(this.isInitialized()) {
                e("Initialize called on already Template");
                return;
            }
            this.spriteURL = spriteURL;
            this.ID = tid; // indicates that this template is loaded
            this._maxHP = maxHP;
            this._maxMana = maxMana;
            
            if(this.spriteURL != "") {
                d("Shape defined by template " + this.getId());
                var image = dojox.image.preload([imgServletURL + this.spriteURL])[0];
                image.template = this;
                image.onload = function() {
                        d("Loaded " + this.template.getSprite());
                        this.template.applyToAllListeners();
                    };
                image.onerror = function() {
                        e("Failed to load kitties");
                    };
            }
            else {
                d("No shape defined by template " + this.getId());
                this.applyToAllListeners();
            }
        },
        applyTo: function(gameobject) {
            // If this defines a shape, override the old shape
            if(this.spriteURL != "") {
                var initTransform = {
                        dx: 0,
                        dy: 0
                    };
                if(gameobject.getShape()) {
                    var oldShape = gameobject.getShape();
                    initTransform = oldShape.getTransform();
                    oldShape.removeShape();
                }
                var newShape = getEnclosingGroup().createGroup();
                newShape.createImage({
                        x: -10,
                        y: -10,
                        width: 20,
                        height: 20,
                        src: imgServletURL + this.spriteURL
                    }).moveToBack();
                newShape.setTransform(initTransform);
                newShape.moveToFront();
                gameobject.setShape(newShape);
            }
        },
        applyToAllListeners: function() {
            for(var i = 0; i < this.listeners.length; ++i) {
                this.applyTo(this.listeners[i]);
            }
        },
        // TODO: Creature specific (should sub-class this later)
        _maxHP: 1,
        _maxMana: 0,
        getMaxHP : function() {
            return this._maxHP;
        },
        getMaxMana : function() {
            return this._maxMana;
        }
    });
