dojo.provide("game.world.creature");

dojo.require("game.common");
dojo.require("game.world.unit");

dojo.declare("Creature", Unit, {
    tid : 0,
    constructor : function(args) {
        dojo.safeMixin(this, args);
    },
    getTemplateId : function() {
        return this.tid;
    },
    getMaxHP : function() {
        var tid = this.getTemplateId();
        if(creTemplatesMap[tid] && creTemplatesMap[tid].isInitialized()) {
            return creTemplatesMap[tid].getMaxHP();
        }
        else {
            return 1;
        }
    },
    getMaxMana : function() {
        var tid = this.getTemplateId();
        if(creTemplatesMap[tid] && creTemplatesMap[tid].isInitialized()) {
            return creTemplatesMap[tid].getMaxMana();
        }
        else {
            return 0;
        }
    }
});
