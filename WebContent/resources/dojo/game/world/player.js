dojo.provide("game.world.player");

dojo.require("game.common");
dojo.require("game.world.unit");

dojo.declare("Player", Unit, {
	constructor : function(args) {
		dojo.safeMixin(this, args);
	},
	setMaxHP : function(maxHP) {
        var oldPercentage = this.getHPPercentage(); // store percentage before change
        this._maxHP = maxHP;
        
        // refresh hpBarFilled
        var percentage = this.getHPPercentage();
        dojox.gfx.fx.animateTransform({
            shape : this.hpBarFilled,
            easing : dojo.fx.easing.linear,
            transform : [ {
                    name : 'translate',
                    start : [ this.shape.matrix.dx-15, this.shape.matrix.dy - 20],
                    end : [ this.shape.matrix.dx-15, this.shape.matrix.dy - 20]
                }, {
                    name : 'scale',
                    start : [ oldPercentage, 1 ],
                    end : [ percentage, 1 ]
            } ],
            duration : moveFireRate,
        }).play();
    }
});
