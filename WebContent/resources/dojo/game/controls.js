dojo.provide("game.controls");

dojo.require("game.common");
dojo.require("game.net.chat");
dojo.require("game.net.client");
dojo.require("game.net.opcode");
dojo.require("game.util.bytebuffer");

dojo.require("dijit.Dialog");


// constants
var MODIFIERS = {
    NONE: 0,
    ALT: 1,
    CTRL: 1 << 1,
    SHIFT: 1 << 2
};

var isKeyDown = {};
var isButtonDown = {};
var keyboardUpMap = new Array();
var keyboardDownMap = new Array();
var buttonMap = new Array();
var disableBrowserNavKeys = false;
var navKeys = getBrowserNavKeys();
var navButtons = getBrowserMouseButtons();
var mouseLoc = { x: 0, y: 0 };
var isMouseMoving = false;

var moveFireRate = 100;

setDefaultControls();

document.onkeypress = processKeyPress;

function getBrowserNavKeys() {
    var browserNavKeys = {
    //  backspace, tab,    F1,       F3,       F5
        8:true,    9:true, 112:true, 114:true, 116:true,
    //  F6,       F7,       F10,      F11,      F12    
        117:true, 118:true, 121:true, 122:true, 123:true
    // arrow keys
    //    33:true, 34:true, 35:true, 36:true, 37:true, 38:true, 39:true, 40:true
    };
    
    return browserNavKeys;
}
function getBrowserMouseButtons() {
    //                     middle, right
    var mouseNavButtons = {2:true, 3:true};
    
    return mouseNavButtons;
}
function getModifierKeys() {
    var modifierKeys = {
    //  shift,    control,  alt
        16: true, 17: true, 18:true
    };
    
    return modifierKeys;
}

// disable browser keys that navigate away from this page
function disableBrowserKeys() {
    disableBrowserNavKeys = true;
    l("nav keys disabled");
}
function enableBrowserKeys() {
    disableBrowserNavKeys = false;
}
function disabledKeyOp(keyEvent) {
    d("disabled key " + getKeyCodeFromEvent(keyEvent));
    dojo.stopEvent(keyEvent);
    
    // TODO: check if this is still needed with regards to nav keys
    window.status = "";
    window.setTimeout("window.status='';", 2000);

    return false;
}

function setDefaultControls() {
    if(getDebugMode()) {
        disableBrowserKeys();
    }
    
    var defaultKeyMap = {
        37: mvLeft,
        38: mvUp,
        39: mvRight,
        40: mvDown,
        33: mvUpright,
        34: mvDownright,
        35: mvDownleft,
        36: mvUpleft,
        
        13: enterChatMsg,           // Enter (toggles chat window, send what's entered if window is focused)
        32: doMeleeAttack,          // Space
        46: test,                   // t
        67: toggleCameraCentered,   // c
        73: toggleInventoryWindow,  // i
        76: toggleTargetsList,      // l
        192: toggleModalWindow      // Esc
    };
    
    Object.keys(defaultKeyMap).forEach(function(key) {
            mapKey(key, MODIFIERS.NONE, defaultKeyMap[key], false);
        });
        
    var defaultButtonMap = {
        1: mvToward
        //3: mvToward
    };
    
    Object.keys(defaultButtonMap).forEach(function(button) {
            mapButton(button, MODIFIERS.NONE, defaultButtonMap[button]);
        });
}

function processKeyPress(keyEvent) {
    // Firefox sets keyEvent.which to 0 for keys that have an ascii value.
    //d("key press \t %i \t %i \t %i", 'color:green', keyEvent.keyCode,
    // keyEvent.which, getKeyCodeFromEvent(keyEvent));
    
    /*if(keyEvent.keyCode) {
    	var keyCode = getKeyCodeFromEvent(keyEvent);
    	if(navKeys[keyCode]) {
    		disabledKeyOp(keyEvent);
    		return false;
    	}
    }*/
    
    return true;
}
function processKeyDown(keyEvent) {
    var keyCode = getKeyCodeFromEvent(keyEvent);
    
    if(isInputBoxFocused() && keyboardDownMap[getEventIndex(keyCode, keyEvent)] != enterChatMsg) {
    	return false;
    }
    
    if(isKeyDown[keyCode]) {
        return true;
    }
    
    if(disableBrowserNavKeys) {
        if(navKeys[keyCode]) {
            // Browser nav keys must be consumed regardless of the return value
            // of the return value of the call associated with the key press
            //d("disabled key %i down", keyCode);
            disabledKeyOp(keyEvent);
        }
    }
    
    isKeyDown[keyCode] = true;
    
    if(keyboardDownMap[getEventIndex(keyCode, keyEvent)] != undefined) {
        return keyboardDownMap[getEventIndex(keyCode, keyEvent)](keyCode);
    }
    
    l("unmapped key down: " + keyCode + ", modifiers: " + getModifierCodeFromEvent(keyEvent) + ", index: " + createEventIndex(keyCode, getModifierCodeFromEvent(keyEvent)));
    return true;
}
function processKeyUp(keyEvent) {
    var keyCode = getKeyCodeFromEvent(keyEvent);
    
    if(isInputBoxFocused() && keyboardDownMap[getEventIndex(keyCode, keyEvent)] != enterChatMsg) {
    	return false;
    }
    
    if(disableBrowserNavKeys) {
        if(navKeys[keyCode]) {
            // Browser nav keys must be consumed regardless of the return value
            // of the return value of the call associated with the key press
            d("disabled key %i up", keyCode);
            disabledKeyOp(keyEvent);
        }
    }
    
    isKeyDown[keyCode] = false;

    if(keyboardUpMap[getEventIndex(keyCode, keyEvent)] != undefined) {
        return keyboardUpMap[getEventIndex(keyCode, keyEvent)](keyCode);
    }
    
    //l("unmapped key up: " + keyCode + ", modifiers: " + getModifierCodeFromEvent(keyEvent) + ", index: " + createEventIndex(keyCode, getModifierCodeFromEvent(keyEvent)));
    return true;
}

function processMouseDown(mouseButtonEvent) {
    setMouseLocFromEvent(mouseButtonEvent);

    button = getMouseButtonFromEvent(mouseButtonEvent);
    if(isButtonDown[button]) {
        // Prevent firing more than once from one mouse down
        return true;
    }

    isButtonDown[button] = true;
    
    if(buttonMap[getEventIndex(button, mouseButtonEvent)] != undefined) {
        return buttonMap[getEventIndex(button, mouseButtonEvent)](button, { x:mouseButtonEvent.pageX, y:mouseButtonEvent.pageY } );
    }
    
    l("unmapped mouse down: " + button + ", modifiers: " + getModifierCodeFromEvent(mouseButtonEvent) + ", index: " + createEventIndex(button, getModifierCodeFromEvent(mouseButtonEvent)));
    return true;
}
function processMouseUp(mouseButtonEvent) {
    setMouseLocFromEvent(mouseButtonEvent);

    button = getMouseButtonFromEvent(mouseButtonEvent);
    isButtonDown[button] = false;
    return true;
}
function processMouseMv(mouseEvent) {
    setMouseLocFromEvent(mouseEvent);
    
    return true;
}

function setMouseLocFromEvent(mouseEvent) {
    mouseLoc.x = mouseEvent.pageX;
    mouseLoc.y = mouseEvent.pageY;
}
function getMouseLoc() {
    return mouseLoc;
}

// Hashes the modifier with the key
function getEventIndex(code, event) {
    return (getModifierCodeFromEvent(event) << 8) | code;
}
function createEventIndex(code, modifiers) {
    return ((modifiers << 8) | code);
}
function getKeyCodeFromEvent(keyEvent) {
    if(window.event) // IE
    {
        return keyEvent.keyCode;
    }
    else if(keyEvent.which) // Netscape/Firefox/Opera
    {
        return keyEvent.which;
    }
    
    return -1;
}
function getMouseButtonFromEvent(mouseButtonEvent) {
    return mouseButtonEvent.which;
}
function getModifierCodeFromEvent(event){
    return ((event.shiftKey << 2) | (event.ctrlKey << 1) | (event.altKey));
}

function reMapKey(keyCode, modifiers, func, isKeyUpEvent) {
    // save something in a cookie

    mapKey(keyCode, modifiers, func, isKeyUpEvent);
}
function mapKey(keyCode, modifiers, func, isKeyUpEvent) {
    if(isKeyUpEvent) {
        keyboardUpMap[createEventIndex(keyCode, modifiers)] = func;
    }
    else {
        keyboardDownMap[createEventIndex(keyCode, modifiers)] = func;
    }
    
    // l("mapped key event " + createEventIndex(keyCode, modifiers) + " to " + func);
}
function mapButton(button, modifiers, func) {
    buttonMap[createEventIndex(button, modifiers)] = func;
}

function repeatMvAsHeld(msDelay, keyCode) {
    if(keyCode) {
        ctrlFunc.mvmt[ctrlFunc.mvmt.length] = keyCode;
        if(ctrlFunc.mvmt.length > 1) {
            return;
        }
    }
    else {
        keyCode = ctrlFunc.mvmt[ctrlFunc.mvmt.length - 1];
    }
    
    setTimeout(function() {
            // If currently held key is not being held, start removing keys from queue
            keyCode = ctrlFunc.mvmt[ctrlFunc.mvmt.length - 1];
            while(!isKeyDown[keyCode] && ctrlFunc.mvmt.length) { 
                ctrlFunc.mvmt.pop();
                keyCode = ctrlFunc.mvmt[ctrlFunc.mvmt.length - 1];
            }
            
            if(keyCode && isKeyDown[keyCode]) {
                keyboardDownMap[keyCode](0);
            }
        }, msDelay);
}


// Mappable functions
var ctrlFunc = {
    mvmt : [],
    ability : [],
    cast : [],
    channeled : []
};

// Movement functions
function mvUp(keyCode) {
    if(!(ctrlFunc.mvmt.length && keyCode)) {
        sendMove({
                dx: 0,
                dy: -1
            });
    }
    repeatMvAsHeld(moveFireRate, keyCode);
    return true;
}
function mvDown(keyCode) {
    if(!(ctrlFunc.mvmt.length && keyCode)) {
        sendMove({
                dx: 0,
                dy: 1
            });
    }
    repeatMvAsHeld(moveFireRate, keyCode);
    return true;
}
function mvLeft(keyCode) {
    if(!(ctrlFunc.mvmt.length && keyCode)) {
        sendMove({
                dx: -1,
                dy: 0
            });
    }
    repeatMvAsHeld(moveFireRate, keyCode);
    return true;
}   
function mvRight(keyCode) {
    if(!(ctrlFunc.mvmt.length && keyCode)) {
        sendMove({
                dx: 1,
                dy: 0
            });
    }
    repeatMvAsHeld(moveFireRate, keyCode);
    return true;
}

function mvUpright(keyCode) {
    if(!(ctrlFunc.mvmt.length && keyCode)) {
        sendMove({
                dx: 0.7071,
                dy: -0.7071
            });
    }
    repeatMvAsHeld(moveFireRate, keyCode);
    return true;
}
function mvDownright(keyCode) {
    if(!(ctrlFunc.mvmt.length && keyCode)) {
        sendMove({
                dx: 0.7071,
                dy: 0.7071
            });
    }
    repeatMvAsHeld(moveFireRate, keyCode);
    return true;
}
function mvDownleft(keyCode) {
    if(!(ctrlFunc.mvmt.length && keyCode)) {
        sendMove({
                dx: -0.7071,
                dy: 0.7071
            });
    }
    repeatMvAsHeld(moveFireRate, keyCode);
    return true;
}
function mvUpleft(keyCode) {
    if(!(ctrlFunc.mvmt.length && keyCode)) {
        sendMove({
                dx: -0.7071,
                dy: -0.7071
            });
    }
    repeatMvAsHeld(moveFireRate, keyCode);
    return true;
}

function mvToward(button, location) {    
    if(!location) {
        if(isButtonDown[button]) {
            location = getMouseLoc();
        }
        else {
            d("mouse move stop");
            isMouseMoving = false;
            return true;
        }
    }
    else {
        if(isMouseMoving) {
            return true;
        }
        
        d("mouse move start");
        
        isMouseMoving = true;
    }

    if(!(ctrlFunc.mvmt.length)) {
        var moverLocation = getMoverLocation();
        if(moverLocation == null)
            return;

        var matrix;
        if(isCameraCentered()) {
            viewPortCenter = getSurfaceCenter();
            matrix = {
                    dx: location.x - viewPortCenter.x,
                    dy: location.y - viewPortCenter.y
                };
        }
        else {
            var enclTransform = getEnclosingGroup().getTransform();
            matrix = {
                    dx: location.x - moverLocation.x - enclTransform.dx,
                    dy: location.y - moverLocation.y - enclTransform.dy
                };
        }
        
        var normMat = normalizeMat(matrix, moveScale);
        sendMove(normMat, normMat == matrix);
    }

    setTimeout(function() {
            mvToward(button, undefined);
        }, moveFireRate);
}

function toggleCameraCentered() {
    setCameraCentered(!isCameraCentered());
}

// Maybe we will have more input boxes for stuff other than chat later
function isInputBoxFocused() {
    return document.activeElement == dojo.byId("inputBox");
}

function doMeleeAttack() {
	getMover().animateAttack();
    return sendMeleeAttack();
}

function enterChatMsg() {
    node = dojo.byId("inputBox");
    if(document.activeElement == node) {
        node.blur();
        sendMessage();
        hideChat();
    }
    else {
    	showChat();
        node.focus();
    }
}

function test(keyCode) {
    //sendBrowserTest();
    
    var buf = new ByteBuffer({capacity: 17}); // 1 + 8 + 4 + 4
    addByteBufHeader(buf, OP.cMOVE);
    buf.putFloat(0);
    buf.putFloat(0);
    sendPacket(buf);
}
