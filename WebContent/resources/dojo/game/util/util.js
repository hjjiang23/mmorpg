dojo.provide("game.util.util");

dojo.require("game.common");


function getRand(from, to){
	return Math.random() * (to - from) + from;
}

function normalizeArray(array) {
    return array.map(function(x) {
            var divisor = Math.sqrt(array.reduce(function(prev, curr) {
                    return prev + (curr * curr);
                }));
            return x / divisor;
        });
}

function normalizeMat(matrix, maxValue) {
    var divisor = Math.sqrt(matReduce(matrix, function(prev, curr) {
            return prev + (curr * curr);
        }));

    if(divisor <= maxValue) {
        return matrix;
    }

    return matMap(matrix, function(x) {
            return x / divisor;
        });
}

function matMap(matrix, func) {
    var newMat = {};
        
    Object.keys(matrix).forEach(function(key) {
            newMat[key] = func(matrix[key]);
        });

    return newMat;
}

function matReduce(matrix, func) {
    var prev = 0;

    Object.keys(matrix).forEach(function(curr) {
            prev = func(prev, matrix[curr]);
        });

    return prev;
}

//Only works for 32 bit ints!
function fastLogBaseTwo(n) {
    return 32 - numberOfLeadingZeroes(n);
}

function numberOfLeadingZeroes(n) {
    for(var i = 31; i >= 0; --i) {
        if((n & (1 << i)) != 0) {
            return 31 - i;
        }
    }
    return 32;
}

function strToByteArray(str) {
    // TODO: check future releases of IE and Opera for ArrayBuffer/typed array support.
    // see http://blog.n01se.net/?p=248 for performance
    var result = new Array(str.length);
    for(var i = 0; i < str.length; ++i) {
        result[i] = (str.charCodeAt(i));
    }
    return result;
}

// This method should be used exclusively for sending packets
byteArrayToStr = function() { 
    if(navigator.appName != 'Microsoft Internet Explorer') {
        return function (arr) {
            var result = "";
            for(var i = 0; i < arr.length; ++i) {
                result += String.fromCharCode(arr[i]);
            }
            return result;
        };
    }
    // hack for Internet Explorer
    else {
        return function (arr) {
            var result = "";
            var i = 0;
            for(; i < arr.length - 1; i += 2) {
                result += String.fromCharCode((arr[i + 1] << 8) | arr[i]);
            }
            // case where length is odd
            if(i == arr.length - 1) {
                result += String.fromCharCode(arr[i]);
            }
            return result;
        };
    }
}();
