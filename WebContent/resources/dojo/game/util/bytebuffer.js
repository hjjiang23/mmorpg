dojo.provide("game.util.bytebuffer");

dojo.require("game.common");
dojo.require("game.util.util");


dojo.declare("ByteBuffer", null, {
        backingArray : [],
        pos : 0,
        _mark : 0,
        limit : 0,
        capacity : 0,
        constructor: function(args) {
            dojo.safeMixin(this, args);
            this.backingArray = new Array(this.capacity);
        },
        getCapacity : function() {
            return this.capacity;
        },
        putByte : function(b) {
            if(this.pos >= this.capacity) {
                e("ByteBuffer: buffer already at capacity");
                return;
            }
            
            this.backingArray[this.pos++] = b & 0xFF;
        },
        putChar : function(c) {
            this.putByte((c & 0xFF00) >> 8);
            this.putByte(c & 0x00FF);
        },
        putByteArray : function(arr) {
            for(var i = 0; i < arr.length; ++i) {
                this.putByte(arr[i]);
            }
        },
        putInt : function(n) {
            this.putByte((n & 0xFF000000) >>> 24);
            this.putByte((n & 0x00FF0000) >>> 16);
            this.putByte((n & 0x0000FF00) >>> 8);
            this.putByte((n & 0x000000FF));
        },
        // Not truly a Long, because Javascript stores all numbers as doubles, so the range of contiguous integers is 0 - 9007199254740992
        // (0 to 2^53 rather than 0 to 2^64-1)
        putLongerInt : function(n) {
            var upper = (n / Math.pow(2,32)) | 0; // divide and floor to get upper
            var lower = ((n & 0xFFFFFFFF) >>> 0);
            this.putInt(upper);
            this.putInt(lower);
        },
        putString : function(str) {
            for(var i = 0; i < str.length; ++i) {
                var c = str.charCodeAt(i);
                this.putChar(c);
            }
            this.putChar(0);
        },
        putByteString : function(str) {
            for(var i = 0; i < str.length; ++i) {
                this.putByte(str.charCodeAt(i));
            }
        },
        putFloat : function(f) {
            this.putByteArray(floatAsBytes(f));
        },
        mark : function() {
            this._mark = this.pos;
        },
        reset : function() {
            this.pos = this._mark;
        },
        get : function() {
            if(this.pos >= this.capacity) {
                e("ByteBuffer: buffer already at end\n" + this.array());
                return 0;
            }

            var b = this.backingArray[this.pos];
            ++this.pos;
            return b;
        },
        getChar : function() {
            return ((this.get() << 8) | this.get());
        },
        getInt : function() {
            var n = 0;
            n |= this.get() << 24;
            n |= this.get() << 16;
            n |= this.get() << 8;
            n |= this.get();
            return n;
        },
        // Will take in a long (from java for example) and represent it as a double
        getLongerInt : function() {
            return ((this.getInt() >>> 0) * Math.pow(2, 32)) + (this.getInt() >>> 0);
        },
        getFloat : function() {
            var bytes = [this.get(), this.get(), this.get(), this.get()];
            return bytesAsFloat(bytes);
        },
        getString : function() {
            var str = "";
            var c = 0;
            while(true) {
                c =  this.getChar();
                if(c != 0) {
                    str += String.fromCharCode(c);
                }
                else {
                    break;
                }
            }
            return str;
                
        },
        array : function() {
            return this.backingArray;
        },
        hasMore : function() {
            return this.pos < this.capacity; 
        }
    });

// Puts all the contents of Array arr into the ByteBuffer buf
/*function setByteBufferContents(buf, arr) {
}*/

function getByteSize(data) {
    var size = 0;
    
    if(!(data instanceof Array)) {
        if(data instanceof String || typeof data == "string") {
            for(var i = 0; i < data.length; ++i) {
                size += getByteSizeOfChar(data.charCodeAt(i));
            }
        }
        else if(typeof data == "number") {
            return 4;
        }
        else{
            e("getByteSize: called on unsupported type, " + typeof data);
        }
    }
    else {
        for(var i = 0; i < data.length; ++i) {
            size += getByteSize(data[i]);
        }
    }
    
    return size;
}
function getByteSizeOfChar(n) {
    // assume int is 4 bytes; number of bytes for char is 4 minus the number of 0 bytes padding the char code
    return Math.ceil(fastLogBaseTwo(n) / 8);
}

function floatAsBytes(f) {
    if (isNaN(f)) {
        // 0x7F800001
        return [0x7F, 0x80, 0x00, 0x01];
    }

    var sign = (f < 0) << 7;
    var exp = 127;
    var mant;

    if (sign) {
        f = - f;
    }
    if (!f) {
        return [sign, 0x00, 0x00, 0x00];
    }
    if (!isFinite(f)) {
        return [(sign | 0x7F), 0x80, 0x00, 0x00];
    }
    while (f >= 2) {
        ++exp;
        f /= 2; 
    }
    while (f < 1) {
        --exp;
        f *= 2;
    }
    f--;
    mant = f * 0x00800000 + 0.5;
    
    return [sign | ((exp & 0xFE) >>> 1), ((exp & 0x01) << 7) | ((mant & 0x007F0000) >>> 16), (mant & 0x0000FF00) >>> 8, mant & 0x000000FF];
}

function bytesAsFloat(bytes) {
    if(bytes.length != 4) {
        e("Expected four bytes for float");
    }
    
    var sign = bytes[0] & 0x80;
    var exp = ((bytes[0] & 0x7F) << 1) | ((bytes[1] & 0x80) >> 7);
    if(exp == 0xFF) {
        // infinity or NaNs, yo
        return null;
    }
    var mant = ((bytes[1] & 0x7F) << 16) | (bytes[2] << 8) | bytes[3];
    if(!exp) {
        exp = -126;
    }
    else {
        exp -= 127;
        mant |= (1 << 23);
    }
    
    mant *= Math.pow(2, exp - 23);
    return sign ? -mant : mant;
}
