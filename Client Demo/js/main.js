/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
var GAME_WIDTH,GAME_HEIGHT;
var vignette;
var bg;
var lastTime;
var player;
var needToRedraw=true;
var mouseX;
var mouseY;
$(document).ready(function(){
    init();
});


function init()
{
    gCanvasElement=$('#mainScreen');
    ctx = gCanvasElement[0].getContext("2d");

    GAME_WIDTH=$('#mainScreen').attr('width');
    GAME_HEIGHT=$('#mainScreen').attr('height');
    
    
    $(document).keyup(function(e){
        onKeyUp(e.keyCode);
    });

    gCanvasElement.mousedown(function(e){
        if( e.button == 0 ) {
            dragging=true;
            setMouseXY(e);
        }
    });
    gCanvasElement.mouseup(function(e){
        if( e.button == 2 ) {
            handleClick(e,2);
        } 
        else
        {
            dragging=false;
        }
    });
    gCanvasElement.mousemove(function(e){
        setMouseXY(e);
    });

    gCanvasElement.bind("contextmenu",function(e){
        return false;
    }); 

    
    setupBackground();
    
    gameTick();

}
function setupButtons()
{
    $('#newGameButton').mouseup(function (){
        $('#controls').slideUp('slow',function(){
            startNewGame();
        });
    });
}
function login()
{
}
function setMouseXY(e)
{
    if (e.pageX || e.pageY) { 
        mouseX = e.pageX;
        mouseY = e.pageY;
    }
    else { 
        mouseX = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft; 
        mouseY = e.clientY + document.body.scrollTop + document.documentElement.scrollTop; 
    } 
    
    
    mouseX -= gCanvasElement.position().left;
    mouseY -= gCanvasElement.position().top;
//    e.preventDefault();
}
function handleClick(e,button)
{
    setMouseXY(e)
    onClick(parseInt(mouseX),parseInt(mouseY),button);
}

function onClick(x,y,button)
{
    if(inShootZone(x,y))
    {
        switch(button)
        {
        }
    }
}

function onKeyUp(keyCode)
{
    //R is pressed
    if(keyCode==82)
    {
        if(GAME_STATE==GameStates.RUNNING)
        {
            this.pudge.rot();
        }
    }
}

function gameTick()
{
    drawBackground();
    setTimeout(gameTick,50);
}
function setupBackground()
{
    bg = new Image();
    bg.onload=function(){
        gameTick();
    }
    bg.src='img/background.png';
}
function drawBackground()
{
    ctx.drawImage(bg,0,0,GAME_WIDTH,GAME_HEIGHT);
}