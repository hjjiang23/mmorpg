/*
SQLyog Community v9.20 
MySQL - 5.5.15 : Database - mmo
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`mmo` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `mmo`;

/*Table structure for table `account` */

DROP TABLE IF EXISTS `account`;

CREATE TABLE `account` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `hash` char(40) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Data for the table `account` */

insert  into `account`(`id`,`name`,`hash`) values (1,'1','356a192b7913b04c54574d18c28d46e6395428ab'),(2,'2','da4b9237bacccdf19c0760cab7aec4a8359010b0'),(3,'3','77de68daecd823babbb58edb1c8e14d7106e83bb'),(4,'4','1b6453892473a467d07372d45eb05abc2031647a'),(5,'5','ac3478d69a3c81fa62e60f5c3696165a4e5e6ac4'),(6,'6','c1dfd96eea8cc2b62785275bca38ac261256e278'),(7,'7','902ba3cda1883801594b6e1b452790cc53948fda'),(8,'8','fe5dbbcea5ce7e2988b8c69bcfdfde8904aabc1f'),(9,'9','0ade7c2cf97f75d009975f4d720d1fa6c19f4897'),(10,'10','b1d5781111d84f7b3fe45a0852e59758cd7a87e5');

/*Table structure for table `creature` */

DROP TABLE IF EXISTS `creature`;

CREATE TABLE `creature` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `x` float unsigned zerofill NOT NULL,
  `y` float unsigned zerofill NOT NULL,
  `orientation` double unsigned NOT NULL DEFAULT '0',
  `tid` int(11) unsigned NOT NULL DEFAULT '1',
  `curr_hp` int(10) NOT NULL DEFAULT '1',
  `curr_mana` int(10) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

/*Data for the table `creature` */

insert  into `creature`(`id`,`x`,`y`,`orientation`,`tid`,`curr_hp`,`curr_mana`) values (1,000000000200,000000000200,0,2,10,1),(2,000000000300,000000000300,0,3,9,1),(3,00000208.869,00000755.167,0,3,9,1),(4,000000149.23,00000480.648,0,3,9,1),(5,00000955.552,00000335.813,0,3,9,1),(6,00000812.411,00000154.614,0,3,9,1),(7,000000835.84,00000115.356,0,3,9,1),(8,00000569.259,00000800.229,0,3,9,1),(9,00000293.367,000000166.15,0,3,9,1),(10,00000450.648,000000154.79,0,3,9,1),(11,00000922.007,00000445.666,0,3,9,1),(12,00000462.306,00000974.534,0,3,9,1),(13,00000485.751,00000505.155,0,3,9,1),(14,000000168.52,00000827.136,0,3,9,1),(15,00000930.121,00000169.197,0,3,9,1),(16,00000155.622,00000770.518,0,3,9,1),(17,00000685.725,000000117.07,0,3,9,1),(18,00000528.177,00000289.672,0,3,9,1),(19,00000863.831,00000450.141,0,3,9,1),(20,00000659.209,00000945.623,0,3,9,1),(21,000000750.49,000000915.58,0,3,9,1),(22,00000326.432,00000885.418,0,3,9,1),(23,00000635.522,00000348.667,0,4,40,1),(24,00000736.768,00000537.839,0,4,40,1),(25,00000378.894,00000180.951,0,4,40,1),(26,00000668.074,00000697.516,0,4,40,1),(27,00000383.361,00000724.253,0,4,40,1),(28,00000371.186,00000583.167,0,4,40,1);

/*Table structure for table `creature_template` */

DROP TABLE IF EXISTS `creature_template`;

CREATE TABLE `creature_template` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` char(40) DEFAULT NULL,
  `ai_name` char(40) DEFAULT NULL,
  `sprite` char(40) DEFAULT NULL,
  `max_hp` int(10) unsigned NOT NULL DEFAULT '1',
  `max_mana` int(10) unsigned NOT NULL DEFAULT '0',
  `min_dmg` int(10) unsigned NOT NULL DEFAULT '0',
  `max_dmg` int(10) unsigned NOT NULL DEFAULT '0',
  `range` double unsigned NOT NULL DEFAULT '0',
  `mv_speed` double unsigned NOT NULL DEFAULT '1',
  `exp` int(10) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `creature_template` */

insert  into `creature_template`(`id`,`name`,`ai_name`,`sprite`,`max_hp`,`max_mana`,`min_dmg`,`max_dmg`,`range`,`mv_speed`,`exp`) values (1,'','TestAI',NULL,1,0,0,0,0,1,0),(2,'Dwarf','CircleWalkAI','dwarf.png',10,0,1,1,12,1,10),(3,'Cat','HunterAI','cat.png',9,0,1,1,12,1,1),(4,'Chicken','CowardAI','chicken.png',40,0,4,4,12,40,1);

/*Table structure for table `item` */

DROP TABLE IF EXISTS `item`;

CREATE TABLE `item` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `iid` int(10) unsigned DEFAULT NULL,
  `pid` int(10) unsigned DEFAULT NULL,
  `quantity` int(10) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

/*Data for the table `item` */

insert  into `item`(`id`,`iid`,`pid`,`quantity`) values (1,1,1,1),(2,1,2,1),(3,1,3,1),(4,1,4,1),(5,1,5,1),(6,1,6,1),(7,1,7,1),(8,1,8,1),(9,1,9,1),(10,1,1,1),(11,1,1,1);

/*Table structure for table `item_template` */

DROP TABLE IF EXISTS `item_template`;

CREATE TABLE `item_template` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` char(40) NOT NULL,
  `sprite` char(40) DEFAULT NULL,
  `type` int(10) unsigned DEFAULT NULL,
  `subtype` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `item_template` */

insert  into `item_template`(`id`,`name`,`sprite`,`type`,`subtype`) values (1,'Healing Potion',NULL,1,0);

/*Table structure for table `player_character` */

DROP TABLE IF EXISTS `player_character`;

CREATE TABLE `player_character` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `aid` int(11) NOT NULL,
  `name` char(20) NOT NULL,
  `x` float DEFAULT '0',
  `y` float NOT NULL DEFAULT '0',
  `orientation` double NOT NULL DEFAULT '0',
  `exp` bigint(20) unsigned NOT NULL DEFAULT '0',
  `level` int(11) NOT NULL DEFAULT '1',
  `curr_hp` int(11) NOT NULL DEFAULT '10',
  `max_hp` int(11) NOT NULL DEFAULT '10',
  `curr_mana` int(11) NOT NULL DEFAULT '10',
  `max_mana` int(11) NOT NULL DEFAULT '10',
  `str` int(11) unsigned NOT NULL DEFAULT '3',
  `dex` int(11) unsigned NOT NULL DEFAULT '3',
  `con` int(11) unsigned NOT NULL DEFAULT '3',
  `wis` int(11) unsigned NOT NULL DEFAULT '3',
  `int` int(11) unsigned NOT NULL DEFAULT '3',
  `cha` int(11) unsigned NOT NULL DEFAULT '3',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Data for the table `player_character` */

insert  into `player_character`(`id`,`aid`,`name`,`x`,`y`,`orientation`,`exp`,`level`,`curr_hp`,`max_hp`,`curr_mana`,`max_mana`,`str`,`dex`,`con`,`wis`,`int`,`cha`) values (1,1,'Alice',0,0,0,0,1,10,10,10,10,3,3,3,3,3,3),(2,2,'Bob',0,0,0,0,1,10,10,10,10,3,3,3,3,3,3),(3,3,'Carol',0,0,0,0,1,10,10,10,10,3,3,3,3,3,3),(4,4,'Dave',0,0,0,0,1,10,10,10,10,3,3,3,3,3,3),(5,5,'Eve',0,0,0,0,1,10,10,10,10,3,3,3,3,3,3),(6,6,'Mallory',0,0,0,0,1,10,10,10,10,3,3,3,3,3,3),(7,7,'Peggy',0,0,0,0,1,10,10,10,10,3,3,3,3,3,3),(8,8,'Trent',0,0,0,0,1,10,10,10,10,3,3,3,3,3,3),(9,9,'Victor',0,0,0,0,1,10,10,10,10,3,3,3,3,3,3),(10,10,'Walter',0,0,0,0,1,10,10,10,10,3,3,3,3,3,3);

/*Table structure for table `terrain` */

DROP TABLE IF EXISTS `terrain`;

CREATE TABLE `terrain` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tid` int(11) unsigned NOT NULL DEFAULT '1',
  `area` polygon DEFAULT NULL,
  `areatext` char(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

/*Data for the table `terrain` */

insert  into `terrain`(`id`,`tid`,`area`,`areatext`) values (1,5,'\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0Y@\0\0\0\0\0\0\0\0\0\0\0\0\0\0Y@\0\0\0\0\0\0Y@\0\0\0\0\0\0\0\0\0\0\0\0\0\0Y@\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','POLYGON((0 0, 100 0, 100 100, 0 100, 0 0))'),(2,5,'\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0P@\0\0\0\0\0\0\0\0\0\0\0\0\0ā@\0\0\0\0\0\0\0\0\0\0\0\0\0ā@\0\0\0\0\0\0Y@\0\0\0\0\0P@\0\0\0\0\0\0Y@\0\0\0\0\0P@\0\0\0\0\0\0\0\0','POLYGON((1300 0, 1400 0, 1400 100, 1300 100, 1300 0))'),(3,5,'\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0p@\0\0\0\0\0\0Y@\0\0\0\0\0p@\0\0\0\0\0\0Y@\0\0\0\0\0\0@\0\0\0\0\0\0\0\0\0\0\0\0\0\0@\0\0\0\0\0\0\0\0\0\0\0\0\0p@','POLYGON((0 1500, 100 1500, 100 1600, 0 1600, 0 1500))'),(4,5,'\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0P@\0\0\0\0\0p@\0\0\0\0\0ā@\0\0\0\0\0p@\0\0\0\0\0ā@\0\0\0\0\0\0@\0\0\0\0\0P@\0\0\0\0\0\0@\0\0\0\0\0P@\0\0\0\0\0p@','POLYGON((1300 1500, 1400 1500, 1400 1600, 1300 1600, 1300 1500))'),(5,5,'\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0Y@\0\0\0\0\0\0Y@\0\0\0\0\0\0Y@\0\0\0\0\0\0Y@\0\0\0\0\0p@\0\0\0\0\0\0\0\0\0\0\0\0\0p@\0\0\0\0\0\0\0\0\0\0\0\0\0\0Y@','POLYGON((0 100, 100 100, 100 1500, 0 1500, 0 100))'),(6,5,'\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0P@\0\0\0\0\0\0Y@\0\0\0\0\0ā@\0\0\0\0\0\0Y@\0\0\0\0\0ā@\0\0\0\0\0p@\0\0\0\0\0P@\0\0\0\0\0p@\0\0\0\0\0P@\0\0\0\0\0\0Y@','POLYGON((1300 100, 1400 100, 1400 1500, 1300 1500, 1300 100))'),(7,5,'\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0Y@\0\0\0\0\0\0\0\0\0\0\0\0\0P@\0\0\0\0\0\0\0\0\0\0\0\0\0P@\0\0\0\0\0\0Y@\0\0\0\0\0\0Y@\0\0\0\0\0\0Y@\0\0\0\0\0\0Y@\0\0\0\0\0\0\0\0','POLYGON((100 0, 1300 0, 1300 100, 100 100, 100 0))'),(8,5,'\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0Y@\0\0\0\0\0p@\0\0\0\0\0P@\0\0\0\0\0p@\0\0\0\0\0P@\0\0\0\0\0\0@\0\0\0\0\0\0Y@\0\0\0\0\0\0@\0\0\0\0\0\0Y@\0\0\0\0\0p@','POLYGON((100 1500, 1300 1500, 1300 1600, 100 1600, 100 1500))'),(16,4,'\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0Ār@\0\0\0\0\0Ār@\0\0\0\0\0Ār@\0\0\0\0\0\0@\0\0\0\0\0\0y@\0\0\0\0\0\0@\0\0\0\0\0\0y@\0\0\0\0\0Ār@\0\0\0\0\0Ār@\0\0\0\0\0Ār@','POLYGON((300 300, 300 800, 400 800, 400 300, 300 300))'),(17,4,'\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0y@\0\0\0\0\0@@\0\0\0\0\0\0y@\0\0\0\0\0Ā@\0\0\0\0\0Ā@\0\0\0\0\0Ā@\0\0\0\0\0Ā@\0\0\0\0\0@@\0\0\0\0\0\0y@\0\0\0\0\0@@','POLYGON((400 500, 400 600, 600 600, 600 500, 400 500))'),(18,4,'\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0Ā@\0\0\0\0\0@@\0\0\0\0\0Ā@\0\0\0\0\0\0@\0\0\0\0\0ā@\0\0\0\0\0\0@\0\0\0\0\0ā@\0\0\0\0\0@@\0\0\0\0\0Ā@\0\0\0\0\0@@','POLYGON((600 500, 600 800, 700 800, 700 500, 600 500))'),(19,4,'\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0@\0\0\0\0\0Ār@\0\0\0\0\0\0@\0\0\0\0\0\0@\0\0\0\0\0 @\0\0\0\0\0\0@\0\0\0\0\0 @\0\0\0\0\0Ār@\0\0\0\0\0\0@\0\0\0\0\0Ār@','POLYGON((800 300, 800 800, 900 800, 900 300, 800 300))'),(20,4,'\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0 @\0\0\0\0\0ā@\0\0\0\0\0 @\0\0\0\0\0\0@\0\0\0\0\0Ā@\0\0\0\0\0\0@\0\0\0\0\0Ā@\0\0\0\0\0ā@\0\0\0\0\0 @\0\0\0\0\0ā@','POLYGON((900 700, 900 800, 1200 800, 1200 700, 900 700))'),(21,4,'\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0 @\0\0\0\0\0Ār@\0\0\0\0\0 @\0\0\0\0\0\0y@\0\0\0\0\0Ā@\0\0\0\0\0\0y@\0\0\0\0\0Ā@\0\0\0\0\0Ār@\0\0\0\0\0 @\0\0\0\0\0Ār@','POLYGON((900 300, 900 400, 1200 400, 1200 300, 900 300))'),(22,4,'\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\00@\0\0\0\0\0\0y@\0\0\0\0\00@\0\0\0\0\0Ā@\0\0\0\0\0Ā@\0\0\0\0\0Ā@\0\0\0\0\0Ā@\0\0\0\0\0\0y@\0\0\0\0\00@\0\0\0\0\0\0y@','POLYGON((1100 400, 1100 600, 1200 600, 1200 400, 1100 400))'),(23,4,'\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0@@\0\0\0\0\0@@\0\0\0\0\0@@\0\0\0\0\0Ā@\0\0\0\0\00@\0\0\0\0\0Ā@\0\0\0\0\00@\0\0\0\0\0@@\0\0\0\0\0@@\0\0\0\0\0@@','POLYGON((1000 500, 1000 600, 1100 600, 1100 500, 1000 500))'),(24,4,'\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0Ār@\0\0\0\0\0 @\0\0\0\0\0Ār@\0\0\0\0\0ā@\0\0\0\0\0\0y@\0\0\0\0\0ā@\0\0\0\0\0\0y@\0\0\0\0\0 @\0\0\0\0\0Ār@\0\0\0\0\0 @','POLYGON((300 900, 300 1400, 400 1400, 400 900, 300 900))'),(25,4,'\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0@@\0\0\0\0\0 @\0\0\0\0\0@@\0\0\0\0\0ā@\0\0\0\0\0Ā@\0\0\0\0\0ā@\0\0\0\0\0Ā@\0\0\0\0\0 @\0\0\0\0\0@@\0\0\0\0\0 @','POLYGON((500 900, 500 1400, 600 1400, 600 900, 500 900))'),(26,4,'\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0ā@\0\0\0\0\00@\0\0\0\0\0ā@\0\0\0\0\0ā@\0\0\0\0\0\0@\0\0\0\0\0ā@\0\0\0\0\0\0@\0\0\0\0\00@\0\0\0\0\0ā@\0\0\0\0\00@','POLYGON((700 1100, 700 1400, 800 1400, 800 1100, 700 1100))'),(27,4,'\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0 @\0\0\0\0\00@\0\0\0\0\0 @\0\0\0\0\0ā@\0\0\0\0\0@@\0\0\0\0\0ā@\0\0\0\0\0@@\0\0\0\0\00@\0\0\0\0\0 @\0\0\0\0\00@','POLYGON((900 1100, 900 1400, 1000 1400, 1000 1100, 900 1100))'),(28,4,'\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0@\0\0\0\0\00@\0\0\0\0\0\0@\0\0\0\0\0Ā@\0\0\0\0\0 @\0\0\0\0\0Ā@\0\0\0\0\0 @\0\0\0\0\00@\0\0\0\0\0\0@\0\0\0\0\00@','POLYGON((800 1100, 800 1200, 900 1200, 900 1100, 800 1100))'),(29,4,'\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0@\0\0\0\0\0P@\0\0\0\0\0ā@\0\0\0\0\0ā@\0\0\0\0\0 @\0\0\0\0\0ā@\0\0\0\0\0 @\0\0\0\0\0P@\0\0\0\0\0\0@\0\0\0\0\0P@','POLYGON((800 1300, 700 1400, 900 1400, 900 1300, 800 1300))');

/*Table structure for table `terrain_template` */

DROP TABLE IF EXISTS `terrain_template`;

CREATE TABLE `terrain_template` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `texture` char(40) DEFAULT '""',
  `name` char(40) DEFAULT '""',
  `desc` char(255) DEFAULT '""',
  `passability` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `terrain_template` */

insert  into `terrain_template`(`id`,`texture`,`name`,`desc`,`passability`) values (1,'grass.png','grass','Grass',0),(2,'dirt.png','dirt','Dirt',0),(3,'sand.png','sand','Sand',0),(4,'pavedroad.png','paved road','Paved Road',1),(5,'brick.png','brick','Brick Wall',1),(6,'wood.png','wood','Wood',0),(7,'ice.png','ice','Ice',0);

/* Trigger structure for table `terrain` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `terrainTriggerInsert` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'localhost' */ /*!50003 TRIGGER `terrainTriggerInsert` BEFORE INSERT ON `terrain` FOR EACH ROW BEGIN
    SET new.area = GEOMFROMTEXT(new.areatext);
  END */$$


DELIMITER ;

/* Trigger structure for table `terrain` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `terrainTriggerUpdate` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'localhost' */ /*!50003 TRIGGER `terrainTriggerUpdate` BEFORE UPDATE ON `terrain` FOR EACH ROW BEGIN
    SET new.area = GEOMFROMTEXT(new.areatext);
  END */$$


DELIMITER ;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
